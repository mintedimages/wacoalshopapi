<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    var $main_nav = 'home';
    var $sub_nav = '';

    public function index() {
        echo "";
    }
    public function updateMIServer(){
        $this->UpdateDataModel->update();
    }
    public function updateShopServer(){
        //call stock shop
        //record call data to database or save to text file
        
        //call update warehouse content
        
    }
    public function testJson(){
        $data = '{"update_date":"2014-01-13 13:57:08","Coords":[{"Accuracy":"65","Latitude":"53.277720488429026","Longitude":"-9.012038778269686","Timestamp":"Fri Jul 05 2013 11:59:34 GMT+0100 (IST)"},{"Accuracy":"65","Latitude":"53.277720488429026","Longitude":"-9.012038778269686","Timestamp":"Fri Jul 05 2013 11:59:34 GMT+0100 (IST)"},{"Accuracy":"65","Latitude":"53.27770755361785","Longitude":"-9.011979642121824","Timestamp":"Fri Jul 05 2013 12:02:09 GMT+0100 (IST)"},{"Accuracy":"65","Latitude":"53.27769091555766","Longitude":"-9.012051410095722","Timestamp":"Fri Jul 05 2013 12:02:17 GMT+0100 (IST)"},{"Accuracy":"65","Latitude":"53.27769091555766","Longitude":"-9.012051410095722","Timestamp":"Fri Jul 05 2013 12:02:17 GMT+0100 (IST)"}]}';
        $manager = json_decode($data);
        print_r($manager);
        echo '<br />'.$manager->update_date;
        echo '<br />Test Update Date<br />';
        echo '<br />'.$manager->Coords[0]->Accuracy;
        echo '<br />Test Accuracy Obj';
    }
    public function testUpdateFile() {
        $this->load->model(array('DownloadImageModel'));
        $i = 0;
        for ($i = 0; $i < 10; $i++) {
            $downloadUrl = "http://www.google.co.in/intl/en_com/images/srpr/logo1w.png";
            $imageUrl = '/uploads/image_'.$i.'.png';
            $savePath = $this->DownloadImageModel->downloadImageFile($downloadUrl, $imageUrl);
            echo '<img src="'.$savePath.'" alt="" />';
        }
        echo "SUCCESS";
    }

}
