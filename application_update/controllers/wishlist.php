<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wishlist extends CI_Controller {

    public function index() {
        
    }

    public function getWishlist($wishlist_id = 0) {
        $retVal = '{"wishlist":[';
        $this->db->select('tbl_product_item.product_item_id');
        $this->db->select('wishlist_item_amount');
        $this->db->select('product_item_code');
        $this->db->join('tbl_product_item', 'tbl_product_item.product_item_id = tbl_wishlist_item.product_item_id', 'left');
        $this->db->where('tbl_wishlist_item.parent_id', $wishlist_id);
        $this->db->where('tbl_wishlist_item.enable_status', 'show');
        $query = $this->db->get('tbl_wishlist_item');
        $count = 0;
        foreach ($query->result() as $row) {
            for ($i = 0; $i < $row->wishlist_item_amount; $i++) {
                if ($count > 0) {
                    $retVal .= ',';
                }
                $count++;
                $retVal .= '{';
                $retVal .= '"brand_code":"BGW-",';
                $retVal .= '"shop_code":"10120017",';
                $retVal .= '"product_item_code":"' . $row->product_item_code . '",';
                $retVal .= '"max_warehouse":"' . $this->getRemainWarehouseAmount($row->product_item_id) . '"';
                $retVal .= '}';
            }
        }
        $retVal .= ']}';
        header('Content-Type: application/json');
        echo $retVal;
    }
    private function getRemainWarehouseAmount($productItemId){
        $retVal = 0;
        $warehouseId = 2;
        $this->db->where('stock_location_id', $warehouseId);
        $this->db->where('parent_id', $productItemId);
        $this->db->where('enable_status', 'show');
        $query = $this->db->get('tbl_stock');
        if($query->num_rows() > 0){
            $stock = $query->row();
            $retVal = $stock->stock_amount;
            $this->db->select_sum('amount');
            $this->db->where('parent_id', $stock->stock_id);
            $this->db->where('update_date >', $stock->update_date);
            $this->db->where('enable_status', 'show');
            $queryCutStock = $this->db->get('tbl_cut_stock');
            if($queryCutStock->num_rows() > 0){
                $cutStockAmount = $queryCutStock->row()->amount;
                $retVal = $retVal - $cutStockAmount;
            }
        }
        return $retVal;
    }

}
