<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CronJob extends CI_Controller {

    public $shopId = 1;

    public function index() {
        echo date('Y-m-d h:i:s', time());
    }

    public function readJsonStock() {
        $shopStockCode = 'shop_stock';
        $this->load->model('StockService');
        $this->db->where('update_config_code', $shopStockCode);
        $query = $this->db->get('tbl_update_config');
        $updateConfig = $query->row();
        /* START CREATE URL AND LOAD JSON CONTENT TO DATA */
        $url = $updateConfig->update_config_url . $updateConfig->update_config_date;
        $last_update_date = date('Y-m-d h:i:s', time());
        $content = preg_replace('/\s+/', '', file_get_contents($url));
        $shopStockList = json_decode($content);
        /* START UPDATE SHOP STOCK */
        foreach ($shopStockList->GETITEMSHOP as $shopStock) {
            $this->StockService->updateStock($shopStock, $this->shopId);
        }
        /* START UPDATE LAST UPDATE */
        $this->db->set('update_config_date', $last_update_date);
        $this->db->where('update_config_code', $shopStockCode);
        $this->db->update('tbl_update_config');
        echo "Complete";
    }

    
    
    public function readJsonWarehouse() {
        $shopStockCode = 'wh_stock';
        $this->load->model('StockService');
        $this->db->where('update_config_code', $shopStockCode);
        $query = $this->db->get('tbl_update_config');
        $updateConfig = $query->row();
        /* START CREATE URL AND LOAD JSON CONTENT TO DATA */
        $url = $updateConfig->update_config_url;//. $updateConfig->update_config_date;
        $last_update_date = date('Y-m-d h:i:s', time());
        $content = preg_replace('/\s+/', '', file_get_contents($url));
        $shopStockList = json_decode($content);
        /* START UPDATE SHOP STOCK */
        foreach ($shopStockList->GETITEMWHResult as $shopStock) {
            $this->StockService->updateStock($shopStock, 2);
        }
        /* START UPDATE LAST UPDATE */
        $this->db->set('update_config_date', $last_update_date);
        $this->db->where('update_config_code', $shopStockCode);
        $this->db->update('tbl_update_config');
        echo "Complete";
    }
    
    public function readSoldWarehouse() {
        $soldWarehouseCode = 'sold_warehouse';
        $this->load->model('StockService');
        $this->db->where('update_config_code', $soldWarehouseCode);
        $query = $this->db->get('tbl_update_config');
        $updateConfig = $query->row();
        $url = $updateConfig->update_config_url.$updateConfig->update_config_date;
        $last_update_date = date('Y-m-d h:i:s', time());
        $content = preg_replace('/\s+/', '', file_get_contents($url));
        $soldWarehouseList = json_decode($content);
        foreach ($soldWarehouseList->cutStockResult as $cutStock) {
            $this->StockService->addCutStock($cutStock, $this->shopId, $updateConfig->update_config_date);
        }
        $this->db->set('update_config_date', $last_update_date);
        $this->db->where('update_config_code', $soldWarehouseCode);
        $this->db->update('tbl_update_config');
        echo "Complete";
    }

}
