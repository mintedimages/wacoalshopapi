<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ClientTemp extends CI_Controller {

    public function index() {
        
    }

    public function getNewArrival() {
        $this->db->select('tbl_product.*');
        $this->db->select('tbl_product_image.product_image_url');
        $this->db->join('tbl_product_image', 'tbl_product_image.parent_id = tbl_product.product_id', 'left');
        $this->db->where('tbl_product.enable_status', 'show');
        $this->db->order_by('tbl_product.sort_priority', 'asc');
        $query = $this->db->get('tbl_product');
        $count = 0;
        $retVal = '{"products":[';
        foreach ($query->result() as $row) {
            if ($count > 0) {
                $retVal .= ',';
            }
            $count++;
            $retVal .= '{';
            $retVal .= '"product_id":"' . $row->product_id . '",';
            $retVal .= '"product_name":"' . $row->product_name . '",';
            $retVal .= '"product_image_url":"' . $row->product_image_url . '"';
            $retVal .= '}';
        }
        $retVal .= ']}';
        header('Content-Type: application/json');
        echo $retVal;
    }

    public function getBestSeller() {
        
    }

    public function getHotProduct() {
        
    }

    public function getPromotion() {
        
    }

    public function getInnovation($parentId = 0) {
        $this->db->where('recursive_id', $parentId);
        $this->db->where('enable_status', 'show');
        $this->db->order_by('sort_priority', 'asc');
        $query = $this->db->get('tbl_product_innovation');
        $count = 0;
        $retVal = '{"jsonContent":[';
        foreach ($query->result() as $row) {
            if ($count > 0) {
                $retVal .= ',';
            }
            $count++;
            $retVal .= '{';
            $retVal .= '"product_innovation_id":"' . $row->product_innovation_id . '",';
            $retVal .= '"product_innovation_name":"' . $row->product_innovation_name . '",';
            $retVal .= '"product_image_url":"' . $this->getInnovationImage($row->product_innovation_id) . '",';
            $retVal .= '"count_product":"' . $this->countInnovationProduct($row->product_innovation_id) . '"';
            $retVal .= '}';
        }
        $retVal .= ']}';
        header('Content-Type: application/json');
        echo $retVal;
    }

    private function getInnovationImage($innovationId) {
        $retVal = '';
        $arrInnovation = array($innovationId);
        $this->db->where('recursive_id', $innovationId);
        $this->db->where('enable_status', 'show');
        $queryInnovation = $this->db->get('tbl_product_innovation');
        foreach ($queryInnovation->result() as $row) {
            array_push($arrInnovation, $row->product_innovation_name);
        }
        $this->db->select('tbl_product_image.product_image_url');
        $this->db->join('tbl_product', 'tbl_product.product_id = tbl_product_image.parent_id');
        $this->db->where_in('tbl_product.product_innovation_id', $arrInnovation);
        $this->db->where('tbl_product.enable_status', 'show');
        $this->db->where('tbl_product_image.enable_status', 'show');
        $this->db->order_by('tbl_product.sort_priority', 'asc');
        $query = $this->db->get('tbl_product_image', 1);
        if ($query->num_rows() > 0) {
            $retVal = $query->row()->product_image_url;
        }
        return $retVal;
    }

    private function countInnovationProduct($innovationId) {
        $retVal = '';
        $arrInnovation = array($innovationId);
        $this->db->where('recursive_id', $innovationId);
        $this->db->where('enable_status', 'show');
        $queryInnovation = $this->db->get('tbl_product_innovation');
        foreach ($queryInnovation->result() as $row) {
            array_push($arrInnovation, $row->product_innovation_name);
        }
        $this->db->select('tbl_product.product_id');
        $this->db->where_in('tbl_product.product_innovation_id', $arrInnovation);
        $this->db->where('tbl_product.enable_status', 'show');
        $this->db->order_by('tbl_product.sort_priority', 'asc');
        $query = $this->db->get('tbl_product');
        return $query->num_rows();
    }

    public function getPattern($parentId = 0) {
        $this->db->where('recursive_id', $parentId);
        $this->db->where('enable_status', 'show');
        $this->db->order_by('sort_priority', 'asc');
        $query = $this->db->get('tbl_product_pattern');
        $count = 0;
        $retVal = '{"jsonContent":[';
        foreach ($query->result() as $row) {
            if ($count > 0) {
                $retVal .= ',';
            }
            $count++;
            $retVal .= $this->printJsonObj($row->product_pattern_id, $row->product_pattern_name, $this->getPatternImage($row->product_pattern_id), $this->countPatternProduct($row->product_pattern_id));
            /*
              $retVal .= '{';
              $retVal .= '"product_pattern_id":"' . $row->product_pattern_id . '",';
              $retVal .= '"product_pattern_name":"' . $row->product_pattern_name . '",';
              $retVal .= '"product_image_url":"' . $this->getPatternImage($row->product_pattern_id) . '",';
              $retVal .= '"count_product":"' . $this->countPatternProduct($row->product_pattern_id) . '"';
              $retVal .= '}'; */
        }
        $retVal .= ']}';
        header('Content-Type: application/json');
        echo $retVal;
    }

    private function printJsonObj($productGroupId, $productGroupName, $productGroupImage, $productGroupCount) {
        $retVal = '{';
        $retVal .= '"product_group_id":"' . $productGroupId . '",';
        $retVal .= '"product_group_name":"' . $productGroupName . '",';
        $retVal .= '"product_image_url":"' . $productGroupImage . '",';
        $retVal .= '"count_product":"' . $productGroupCount . '"';
        $retVal .= '}';
        return $retVal;
    }

    private function getPatternImage($patternId) {
        $retVal = '';
        $arrPattern = array($patternId);
        $this->db->where('recursive_id', $patternId);
        $this->db->where('enable_status', 'show');
        $queryPattern = $this->db->get('tbl_product_pattern');
        foreach ($queryPattern->result() as $row) {
            array_push($arrPattern, $row->product_pattern_name);
        }
        $this->db->select('tbl_product_image.product_image_url');
        $this->db->join('tbl_product', 'tbl_product.product_id = tbl_product_image.parent_id');
        $this->db->where_in('tbl_product.product_pattern_id', $arrPattern);
        $this->db->where('tbl_product.enable_status', 'show');
        $this->db->where('tbl_product_image.enable_status', 'show');
        $this->db->order_by('tbl_product.sort_priority', 'asc');
        $query = $this->db->get('tbl_product_image', 1);
        if ($query->num_rows() > 0) {
            $retVal = $query->row()->product_image_url;
        }
        return $retVal;
    }

    private function countPatternProduct($patternId) {
        $retVal = '';
        $arrPattern = array($patternId);
        $this->db->where('recursive_id', $patternId);
        $this->db->where('enable_status', 'show');
        $queryPattern = $this->db->get('tbl_product_pattern');
        foreach ($queryPattern->result() as $row) {
            array_push($arrPattern, $row->productPatternName);
        }
        $this->db->select('tbl_product.product_id');
        $this->db->where_in('tbl_product.product_pattern_id', $arrPattern);
        $this->db->where('tbl_product.enable_status', 'show');
        $this->db->order_by('tbl_product.sort_priority', 'asc');
        $query = $this->db->get('tbl_product');
        return $query->num_rows();
    }

    public function getStyle($parentId = 0) {
        
    }

    public function getCollection($parentId = 0) {
        
    }

    public function getProductByInnovation($innovationId) {
        
    }

    public function getProductByPattern($patternId) {
        
    }

    public function getProductByStyle($styleId) {
        
    }

    public function getProductByCollection($collectionId) {
        
    }

    public function getProduct($productId) {
        
    }

    public function getProductImage($productId) {
        
    }

    public function getProductItem($productId) {
        
    }

    public function createWishList($clientCode) {
        $locationCode = 'SH01';
        $arrProductItemId = explode(',', $this->input->post('str_product_item_id'));
        $arrProductItemAmount = explode(',', $this->input->post('str_product_item_amount'));
        $locationId = $this->getLocationId($locationCode);
        $clientId = $this->getClientId($locationId, $clientCode);
        $this->db->set('client_id', $clientId);
        $this->db->set('location_id', $locationId);
        $this->db->set('enable_status', 'show');
        $this->db->insert('tbl_wishlist');
        $this->db->order_by('sort_priority', 'desc');
        $query = $this->db->get('tbl_wishlist', 1);
        $wishlistId = $query->row()->wishlist_id;
        for ($i = 0; $i < count($arrProductItemId); $i++) {
            $this->db->set('parent_id', $wishlistId);
            $this->db->set('product_item_id', $arrProductItemId[$i]);
            $this->db->set('wishlist_item_amount', $arrProductItemAmount[$i]);
            $this->db->set('sort_priority', $i);
            $this->db->set('enable_status', 'show');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', 0);
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', 0);
            $this->db->insert('tbl_wishlist_item');
        }
    }

    private function getLocationId($locationCode) {
        $locationId = 0;
        $this->db->where('location_code', $locationCode);
        $this->db->where('enable_status', 'show');
        $query = $this->db->get('tbl_location', 1);
        if ($query->num_rows() > 0) {
            $locationId = $query->row()->location_id;
        }
        return $locationId;
    }

    private function getClientId($locationId, $clientCode) {
        $clientId = 0;
        $this->db->where('parent_id', $locationId);
        $this->db->where('client_code', $clientCode);
        $this->db->where('enable_status', 'show');
        $query = $this->db->get('tbl_client', 1);
        if ($query->num_rows() > 0) {
            $clientId = $query->row()->client_id;
        }
        return $clientId;
    }

}
