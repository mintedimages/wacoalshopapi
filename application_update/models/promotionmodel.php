<?php

class PromotionModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($promotion) {
        $isNew = $this->checkNew($promotion->promotion_id);
        $serverUrl = $this->SyncDataModel->getMiServerDomain();
        $this->setQuery($promotion);
        if ($isNew) {
            $this->insertQuery($promotion->promotion_id);
        } else {
            $this->updateQuery($promotion->promotion_id);
        }
        $this->DownloadImageModel->keepDownloadImage($serverUrl . $promotion->promotion_image_portrait, $promotion->promotion_image_portrait);
        $this->DownloadImageModel->keepDownloadImage($serverUrl . $promotion->promotion_image_landscape, $promotion->promotion_image_landscape);
    }

    private function checkNew($promotionId) {
        $this->db->where('promotion_reference_id', $promotionId);
        $query = $this->db->get('tbl_promotion');
        return ($query->num_rows() == 0);
    }

    private function setQuery($promotion) {
        $this->db->set('promotion_name', $promotion->promotion_name);
        $this->db->set('location_id', $promotion->location_id);
        $this->db->set('promotion_text', $promotion->promotion_text);
        $this->db->set('promotion_image_portrait', $promotion->promotion_image_portrait);
        $this->db->set('promotion_image_landscape', $promotion->promotion_image_landscape);
        $this->db->set('enable_status', $promotion->enable_status);
        $this->db->set('sort_priority', $promotion->sort_priority);
    }

    private function insertQuery($promotionId) {
        $this->db->set('promotion_reference_id', $promotionId);
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->insert('tbl_promotion');
    }

    private function updateQuery($promotionId) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('promotion_reference_id', $promotionId);
        $this->db->update('tbl_promotion');
    }

}
