<?php

class LocationModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($location) {
        $isNew = $this->checkNew($location->location_code);
        $this->setQuery($location);
        if ($isNew) {
            $this->insertQuery($location->location_code);
        } else {
            $this->updateQuery($location->location_code);
        }
    }

    private function checkNew($locationCode) {
        $this->db->where('location_code', $locationCode);
        $query = $this->db->get('tbl_location');
        return ($query->num_rows() == 0);
    }

    private function setQuery($location) {
        $this->db->set('location_name', $location->location_name);
        $this->db->set('enable_status', $location->enable_status);
        $this->db->set('sort_priority', $location->sort_priority);
    }

    private function insertQuery($locationCode) {
        $this->db->set('location_code', $locationCode);
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->insert('tbl_location');
    }

    private function updateQuery($locationCode) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('location_code', $locationCode);
        $this->db->update('tbl_location');
    }

}
