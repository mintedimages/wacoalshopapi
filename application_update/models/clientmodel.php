<?php

class ClientModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($client) {
        $isNew = $this->checkNew($client->client_id);
        $this->setQuery($client);
        if ($isNew) {
            $this->insertQuery($client->client_id);
        } else {
            $this->updateQuery($client->client);
        }
    }

    private function checkNew($clientId) {
        $this->db->where('client_id', $clientId);
        $query = $this->db->get('tbl_client');
        return ($query->num_rows() == 0);
    }

    private function setQuery($client) {
        $this->db->set('parent_id', $client->parent_id);
        $this->db->set('client_name', $client->client_name);
        $this->db->set('enable_status', $client->enable_status);
        $this->db->set('sort_priority', $client->sort_priority);
    }

    private function insertQuery($clientId) {
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->insert('tbl_client');
    }

    private function updateQuery($clientId) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('client_id', $clientId);
        $this->db->update('tbl_client');
    }

}