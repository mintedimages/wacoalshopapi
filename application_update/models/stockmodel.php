<?php

class StockModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($stock) {
        $isNew = $this->checkNew($stock->parent_id, $stock->stock_location_id);
        $this->setQuery($stock);
        if($isNew){
            $this->insertQuery($stock->parent_id, $stock->stock_location_id);
        }else{
            $this->updateQuery();
        }
    }
    private function checkNew($productItemId, $stockLocationId){
        $this->db->where('parent_id', $productItemId);
        $this->db->where('stock_location_id', $stockLocationId);
        $query = $this->db->get('tbl_stock');
        return ($query->num_rows() == 0);
    }

    private function setQuery($stock) {
        $this->db->set('stock_amount', $stock->stock_amount);
    }

    private function insertQuery($productItemId, $stockLocationId) {
        $this->db->set('parent_id', $productItemId);
        $this->db->set('stock_location_id', $stockLocationId);
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->insert('tbl_stock');
    }

    private function updateQuery($productItemId, $stockLocationId) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('parent_id', $productItemId);
        $this->db->where('stock_location_id', $stockLocationId);
        $this->db->update('tbl_stock');
    }

}
