<?php

class StockService extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function updateStock($shopStock, $shopId) {
        $this->_insertLog($shopStock, $shopId);
        $queryStock = $this->_queryStock($shopStock->ITEMCODE, $shopId);
        if ($queryStock->num_rows() > 0) {
            $stock = $queryStock->row();
            $this->_updateStock($shopStock->QTY, $stock->stock_id);
        } else {
            $productItemId = $this->_getProductItemId($shopStock);
            $this->_insertStock($productItemId, $shopId, $shopStock->QTY);
        }
    }
    
    public function addCutStock($cutStock, $shopId, $requestTime){
        $warehouseId = 2;
        $queryStock = $this->_queryStock($cutStock->ITEMCODE, $warehouseId);
        if($queryStock->num_rows() > 0){
            $stock = $queryStock->row();
            $this->_addCutStock($stock->stock_id, $cutStock->QTY, $shopId, $requestTime);
            echo "<br />ITEM CODE FOUND : ".$cutStock->ITEMCODE;
        }else{
            //keep log error to add cut stock that return wrong code
            echo "<br />ITEM CODE NOT FOUND : ".$cutStock->ITEMCODE;
        }
    }
    /* START PRIVATE FUNCTION */

    private function _getProductItemId($shopStock) {
        $productItem = $this->_getProductItem($shopStock->ITEMCODE);
        $productItemId = 0;
        if ($productItem) {
            $productItemId = $productItem->product_item_id;
        } else {
            $productId = $this->_getProductId($shopStock->ITEMCODE);
            //insert new product item
            $productItemId = $this->_insertProductItem($productId, $shopStock->ITEMCODE, $shopStock->PRICE);
        }
        return $productItemId;
    }

    private function _insertProductItem($productId, $productItemCode, $productItemPrice) {
        $this->db->set('product_id', $productId);
        $this->db->set('product_item_code', $productItemCode);
        $this->db->set('product_item_price', $productItemPrice);
        $this->_setInsertDefault();
        $this->db->insert('tbl_product_item');
        $this->db->order_by('product_item_id', 'DESC');
        return $this->db->get('tbl_product_item', 1)->row()->product_item_id;
    }

    private function _getProductId($productItemCode) {
        $product = $this->_getProductFromItemCode($productItemCode);
        $productId = 0;
        if ($product) {
            $productId = $product->product_id;
        } else {
            $productId = $this->_insertProductFromItemCode($productItemCode);
        }
        return $productId;
    }

    private function _insertLog($shopStock, $shopId) {
        $this->db->set('balanceqty', $shopStock->QTY);
        $this->db->set('itemcode', $shopStock->ITEMCODE);
        $this->db->set('price', $shopStock->PRICE);
        $this->db->set('shop_id', $shopId);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->insert('tbl_log_update_shop_stock');
    }

    private function _queryStock($productItemCode, $shopId) {
        $this->db->select('tbl_stock.*');
        $this->db->join('tbl_stock', 'tbl_product_item.product_item_id = tbl_stock.parent_id', 'left');
        $this->db->where('product_item_code', $productItemCode);
        $this->db->where('stock_location_id', $shopId);
        return $this->db->get('tbl_product_item');
    }

    private function _updateStock($stockQty, $stockId) {
        $this->db->set('stock_amount', $stockQty);
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', 0);
        $this->db->where('stock_id', $stockId);
        $this->db->update('tbl_stock');
    }

    private function _insertStock($productItemId, $shopId, $stockAmount) {
        $this->db->set('stock_amount', $stockAmount);
        $this->db->set('parent_id', $productItemId);
        $this->db->set('stock_location_id', $shopId);
        $this->_setInsertDefault();
        $this->db->insert('tbl_stock');
    }

    private function _getProductItem($itemCode) {
        $this->db->where('product_item_code', $itemCode);
        $queryProductItem = $this->db->get('tbl_product_item');
        if ($queryProductItem->num_rows() > 0) {
            return $queryProductItem->row();
        }
        return false;
    }
    private function _getProductFromItemCode($productItemCode){
        $productCode = substr($productItemCode, 0, 6);
        $this->db->where('product_code', $productCode);
        $queryProduct = $this->db->get('tbl_product');
        if ($queryProduct->num_rows() > 0) {
            return $queryProduct->row();
        }
        return false;
    }

    private function _insertProductFromItemCode($productItemCode) {
        $productCode = substr($productItemCode, 0, 6);
        $this->db->set('product_code', $productCode);
        $this->_setInsertDefault();
        $this->db->insert('tbl_product');
        $this->db->order_by('product_id', 'DESC');
        return $this->db->get('tbl_product', 1)->row()->product_id;
    }

    private function _setInsertDefault() {
        $this->db->set('enable_status', 'show');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', 0);
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', 0);
    }
    private function _addCutStock($stockId, $amount, $shopId, $requestTime){
        $this->db->set('parent_id', $stockId);
        $this->db->set('amount', $amount);
        $this->db->set('shop_id', $shopId);
        $this->db->set('request_time', $requestTime);
        $this->_setInsertDefault();
        $this->db->insert('tbl_cut_stock');
    }
}
