<?php

class UpdateDataModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model(array('DownloadImageModel'));
    }

    function update() {
        $this->updateData();
        $this->updateImage();
    }

    private function updateData() {
        $arrUpdate = array();
        $arrUpdate[] = array('product', $this->ProductModel);
        $arrUpdate[] = array('productItem', $this->ProductItemModel);
        $arrUpdate[] = array('promotion', $this->PromotionModel);
        $arrUpdate[] = array('location', $this->LocationModel);
        $arrUpdate[] = array('client', $this->ClientModel);
        $arrUpdate[] = array('wow', $this->WowModel);
        $arrUpdate[] = array('color', $this->ColorModel);
        $countArrUpdate = count($arrUpdate);
        for ($updateIndex = 0; $updateIndex < $countArrUpdate; $updateIndex++) {
            //get last update of each type
            $lastUpdateDate = $this->getLastUpdateDate($arrUpdate[$updateIndex][0]);
            $updateModel = $arrUpdate[$updateIndex][1];
            //send data to server
            //get response json
            //convert to object
            $strJson = $this->getJsonString('product');
            $objJson = json_decode($strJson);
            //loop and save to database
            $products = $strJson->products;
            $productLength = count($products);
            for ($productIndex = 0; $productIndex < $productLength; $productIndex++) {
                //if it was and image, save it to download image list.
            }
            //if finished all data, change last update date
        }
    }

    private function updateImage() {
        $imageList = $this->DownloadImageModel->getDownloadImageList();
        foreach ($imageList->result() as $image) {
            $this->DownloadImageModel->downloadImageFile($image->download_image_url, $image->image_url);
            $this->DownloadImageModel->finishedDownloadImage($image->download_image_id);
        }
    }

    private function getLastUpdateDate($updateContentCode = '') {
        $this->db->where('update_content_code', $updateContentCode);
        $this->db->where('enable_status', 'show');
        $query = $this->db->get('tbl_update_content', 1);
        if ($query->num_rows() > 0) {
            $updateContent = $query->row();
            return $updateContent->last_update_date;
        }
        return '';
    }

    function setLastUpdateDate($updateContentCode, $date) {
        $this->db->set('last_update_date', $date);
        $this->db->where('update_content_code', $updateContentCode);
        $this->db->where('enable_staus', 'show');
        $this->db->update('tbl_update_content');
    }

}
