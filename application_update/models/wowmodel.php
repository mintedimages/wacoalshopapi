<?php

class WowModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($wow) {
        $isNew = $this->checkNew($wow->wow_id);
        $this->setQuery($wow);
        if ($isNew) {
            $this->insertQuery($wow->wow_id);
        } else {
            $this->updateQuery($wow->wow_id);
        }
    }

    private function checkNew($wowId) {
        $this->db->where('wow_id', $wowId);
        $query = $this->db->get('tbl_wow');
        return ($query->num_rows() == 0);
    }

    private function setQuery($wow) {
        $this->db->set('wow_name', $wow->wow_name);
        $this->db->set('wow_content', $wow->wow_content);
        $this->db->set('enable_status', $wow->enable_status);
        $this->db->set('sort_priority', $wow->sort_priority);
    }

    private function insertQuery($wowId) {
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->insert('tbl_wow');
    }

    private function updateQuery($wowId) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('wow_id', $wowId);
        $this->db->update('tbl_wow');
    }

}