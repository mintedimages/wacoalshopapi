<?php

class ProductItemModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($productItem) {
        $isNew = $this->checkNew($productItem->product_item_name);
        $this->setQuery($productItem);
        if($isNew){
            $this->insertQuery();
        }else{
            $this->updateQuery($productItem->product_item_name);
        }
    }
    private function checkNew($productItemName){
        $this->db->where('product_item_name', $productItemName);
        $query = $this->db->get('tbl_product_item');
        return ($query->num_rows() == 0);
    }

    private function setQuery($productItem) {
        $this->db->set('product_item_name', $productItem->product_item_name);
        $this->db->set('product_item_type_id', $productItem->product_item_type_id);
        $this->db->set('product_color_id', $productItem->product_color_id);
        $this->db->set('bra_size_id', $productItem->bra_size_id);
        $this->db->set('pant_size_id', $productItem->pant_size_id);
    }

    private function insertQuery() {
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->insert('tbl_product_item');
    }

    private function updateQuery($productItemName) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('product_item_name', $productItemName);
        $this->db->update('tbl_product_item');
    }

}
