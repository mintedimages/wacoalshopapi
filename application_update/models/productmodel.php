<?php

class ProductModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getLastUpdate() {
        return '';
    }

    function setLastUpdate() {
        //$this->
    }

    function getUrl() {
        return '';
    }

    function update($product) {
        $isNewProduct = $this->checkNewProduct($product->product_id);
        $this->setProductQuery($product);
        if ($isNewProduct) {
            $this->insertQuery($product->product_id);
        } else {
            $this->updateQuery($product->product_id);
        }
    }
    private function checkNewProduct($productId){
        $this->db->where('product_relate_id', $productId);
        $query = $this->db->get('tbl_product');
        return ($query->num_rows() == 0);
    }

    private function setProductQuery($product) {
        $this->db->set('product_name', $product->product_name);
        $this->db->set('product_innovation_id', $product->product_innovation_id);
        $this->db->set('product_style_id', $product->product_style_id);
        $this->db->set('product_collection_id', $product->product_product_collection_id);
        $this->db->set('product_detail', $product->product_detail);
        $this->db->set('enable_status', $product->enable_status);
        $this->db->set('sort_priority', $product->sort_priority);
    }

    private function insertQuery($productId) {
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->set('product_relate_id', $productId);
        $this->db->insert('tbl_product');
    }

    private function updateQuery($productId) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('product_relate_id', $productId);
        $this->db->update('tbl_product');
    }

}
