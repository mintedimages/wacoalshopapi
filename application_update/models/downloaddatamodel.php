<?php

class DownloadDataModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function updateProduct($product){
        $isNewProduct = false;
        $this->db->set('product_name', $product->product_name);
        $this->db->set('product_innovation_id', $product->product_innovation_id);
        $this->db->set('product_style_id', $product->product_style_id);
        $this->db->set('product_collection_id', $product->product_product_collection_id);
        $this->db->set('product_detail', $product->product_detail);
        if($isNewProduct){
            $this->db->set('create_date', 'NOW()', false);
            $this->db->set('create_by', 0);
            $this->db->set('product_relate_id', $product->product_id);
            $this->db->insert('tbl_product');
        }else{
            $this->db->set('update_date', 'NOW()', false);
            $this->db->set('update_date', 0);
            $this->db->where('product_relate_id', $product->product_id);
            $this->db->update('tbl_product');
        }
    }
}
