<?php

class SyncDataModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getLastUpdate($tableCode) {
        $this->db->where('table_code', $tableCode);
        $query = $this->db->get('tbl_sync_config');
        return '';
    }

    function setLastUpdate($tableCode, $updateDate) {
        $this->db->set();
        $this->db->where();
        $this->db->update();
    }

    function getMiServerDomain() {
        return 'http://wacoalicc.mintedimages.com';
    }
    function getJsonUrl($tableCode){
        return $this->getMiServerDomain().'/admin.php/json/data/'.$tableCode;
    }
}
