<?php

class DownloadImageModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function keepDownloadImage($downloadUrl, $imageUrl) {
        $this->db->set('download_image_url', $downloadUrl);
        $this->db->set('image_url', $imageUrl);
        $this->db->set('enable_status', 'show');
        $this->db->set('create_date', 'NOW()', true);
        $this->db->insert('tbl_download_image');
    }

    function getDownloadImageList() {
        $this->db->where('enable_status', 'show');
        $this->db->orderby('download_image_id', 'ASC');
        return $this->db->get('tbl_download_image');
    }

    function finishedDownloadImage($downloadImageId) {
        $this->db->set('enable_status', 'delete');
        $this->db->set('update_date', 'NOW()', true);
        $this->db->where('download_image_id', $downloadImageId);
        $this->db->update('tbl_download_image');
    }

    function downloadImageFile($downloadUrl, $imageUrl) {
        $content = file_get_contents($downloadUrl);
        $rootPath = $_SERVER['DOCUMENT_ROOT'] . $imageUrl;
        $fp = fopen($rootPath, "w");
        fwrite($fp, $content);
        fclose($fp);
    }

}
