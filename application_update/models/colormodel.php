<?php

class ColorModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($color) {
        $isNew = $this->checkNew($color->color_id);
        $this->setQuery($color);
        if ($isNew) {
            $this->insertQuery();
        } else {
            $this->updateQuery($color->color_id);
        }
    }

    private function checkNew($colorId) {
        $this->db->where('color_id', $colorId);
        $query = $this->db->get('tbl_color');
        return ($query->num_rows() == 0);
    }

    private function setQuery($color) {
        $this->db->set('color_name', $color->color_name);
        $this->db->set('color_image', $color->color_image);
        $this->db->set('enable_status', $color->enable_status);
        $this->db->set('sort_priority', $color->sort_priority);
    }

    private function insertQuery() {
        $this->db->set('create_date', 'NOW()', false);
        $this->db->set('create_by', 0);
        $this->db->insert('tbl_color');
    }

    private function updateQuery($colorId) {
        $this->db->set('update_date', 'NOW()', false);
        $this->db->set('update_date', 0);
        $this->db->where('color_id', $colorId);
        $this->db->update('tbl_color');
    }

}