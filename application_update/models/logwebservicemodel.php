<?php

class LogWebserviceModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function insert($call_url, $json_str){
        $this->db->set('call_url',$call_url);
        $this->db->set('json_str',$json_str);
        $this->db->set('update_status', 'fail');
        $this->db->insert('tbl_log_webservice');
        //get id
        $this->db->order_by('log_webservice_id', 'desc');
        $query = $this->db->get('tbl_log_webservice',1);
        return $query->row()->log_webservice_id;
    }
    function setUpdateSuccess($log_webservice_id){
        $this->db->set('update_status','success');
        $this->db->where('log_webservice_id',$log_webservice_id);
        $this->db->update('tbl_log_webservice');
    }
}