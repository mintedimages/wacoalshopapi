<?php

class Migration_Create_mother extends CI_Migration {

    public function up() {
        /* CREATE TABLE */
        if (!$this->db->table_exists('log_action')) {
            $this->dbforge->add_field(array(
                'action_id' => array(
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'constraint' => '20'
                ),
                'table_name' => array(
                    'type' => 'CHAR',
                    'constraint' => '30'
                ),
                'content_id' => array(
                    'type' => 'BIGINT',
                    'constraint' => '20'
                ),
                'action_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'action_detail' => array(
                    'type' => 'TEXT'
                ),
                'action_query' => array(
                    'type' => 'TEXT'
                ),
                'create_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'create_date' => array(
                    'type' => 'DATETIME'
                ),
                'create_ip' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                )
            ));
            $this->dbforge->add_key('action_id', TRUE);
            $this->dbforge->create_table('log_action');
        }
        if (!$this->db->table_exists('mother_column')) {
            $this->dbforge->add_field(array(
                'column_id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'constraint' => '11'
                ),
                'table_id' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'column_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'column_code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'column_main' => array(
                    'type' => 'ENUM',
                    'constraint' => "'true', 'false'",
                    'default' => "false"
                ),
                'column_field_type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'column_lang' => array(
                    'type' => 'TINYINT',
                    'constraint' => '4'
                ),
                'column_show_list' => array(
                    'type' => 'TINYINT',
                    'constraint' => '4'
                ),
                'column_option' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'column_relation_table' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'sort_priority' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'create_date' => array(
                    'type' => 'DATETIME'
                ),
                'create_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'update_date' => array(
                    'type' => 'DATETIME'
                ),
                'update_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                )
            ));
            $this->dbforge->add_key('column_id', TRUE);
            $this->dbforge->create_table('mother_column');

            $this->dbforge->add_field(array(
                'lang_id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'constraint' => '11'
                ),
                'lang_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'lang_code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '10'
                ),
                'sort_priority' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'create_date' => array(
                    'type' => 'DATETIME'
                ),
                'create_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'update_date' => array(
                    'type' => 'DATETIME'
                ),
                'update_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                )
            ));
            $this->dbforge->add_key('lang_id', TRUE);
            $this->dbforge->create_table('mother_lang');

            $this->dbforge->add_field(array(
                'table_id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'constraint' => '11'
                ),
                'parent_table_id' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'table_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'table_code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'table_type' => array(
                    'type' => 'ENUM',
                    'constraint' => "'static', 'dynamic'"
                ),
                'sort_priority' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'create_date' => array(
                    'type' => 'DATETIME'
                ),
                'create_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'update_date' => array(
                    'type' => 'DATETIME'
                ),
                'update_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                )
            ));
            $this->dbforge->add_key('table_id', TRUE);
            $this->dbforge->create_table('mother_table');

            $this->dbforge->add_field(array(
                'user_id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'constraint' => '11'
                ),
                'user_group_id' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'user_login_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'user_login_password' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'enable_status' => array(
                    'type' => 'ENUM',
                    'constraint' => "'enable', 'disable'",
                    'default' => 'enable'
                ),
                'create_date' => array(
                    'type' => 'DATETIME'
                ),
                'create_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'update_date' => array(
                    'type' => 'DATETIME'
                ),
                'update_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                )
            ));
            $this->dbforge->add_key('user_id', TRUE);
            $this->dbforge->create_table('mother_user');

            $this->dbforge->add_field(array(
                'user_group_id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'constraint' => '11'
                ),
                'user_group_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'user_group_permission' => array(
                    'type' => 'LONGTEXT'
                ),
                'create_date' => array(
                    'type' => 'DATETIME'
                ),
                'create_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'update_date' => array(
                    'type' => 'DATETIME'
                ),
                'update_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                )
            ));
            $this->dbforge->add_key('user_group_id', TRUE);
            $this->dbforge->create_table('mother_user_group');
            /* END CREATE TABLE */

            /* INSERT DEVELOPER */
            $this->db->set('user_group_id', '0');
            $this->db->set('user_login_name', 'developer');
            $this->db->set('user_login_password', md5('023742155'));
            $this->db->set('enable_status', 'enable');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->insert('mother_user');
        }
        if (!$this->db->table_exists('mother_config')) {
            $this->dbforge->add_field(array(
                'config_id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'constraint' => '11'
                ),
                'config_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'config_code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'config_type' => array(
                    'type' => 'ENUM',
                    'constraint' => "'content', 'admin'",
                    'default' => "content"
                ),
                'config_field_type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'config_value' => array(
                    'type' => 'TEXT'
                ),
                'sort_priority' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'create_date' => array(
                    'type' => 'DATETIME'
                ),
                'create_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                ),
                'update_date' => array(
                    'type' => 'DATETIME'
                ),
                'update_by' => array(
                    'type' => 'INT',
                    'constraint' => '11'
                )
            ));
            $this->dbforge->add_key('config_id', TRUE);
            $this->dbforge->create_table('mother_config');
            /* CREATE DEFAULT DATA */
            /* INSERT BACKEND TITLE */
            $this->db->set('config_name', 'Backend Title');
            $this->db->set('config_code', 'site_title');
            $this->db->set('config_type', 'admin');
            $this->db->set('config_field_type', 'text');
            $this->db->set('config_value', 'Backend');
            $this->db->set('sort_priority', '1');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->insert('mother_config');
            /* INSERT BACKEND NAVIGATE COLOR */
            $this->db->set('config_name', 'Navigate Color');
            $this->db->set('config_code', 'navigate_color');
            $this->db->set('config_type', 'admin');
            $this->db->set('config_field_type', 'color');
            $this->db->set('config_value', '#454545');
            $this->db->set('sort_priority', '2');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->insert('mother_config');
            /* INSERT BACKEND NAVIGATE SHADOW */
            $this->db->set('config_name', 'Navigate Shadow Color');
            $this->db->set('config_code', 'navigate_color_shadow');
            $this->db->set('config_type', 'admin');
            $this->db->set('config_field_type', 'color');
            $this->db->set('config_value', '#1f1f1f');
            $this->db->set('sort_priority', '3');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->insert('mother_config');
            /* INSERT BACKEND NAVIGATE COLOR */
            $this->db->set('config_name', 'Navigate Color Active');
            $this->db->set('config_code', 'navigate_color_active');
            $this->db->set('config_type', 'admin');
            $this->db->set('config_field_type', 'color');
            $this->db->set('config_value', '#6b6b6b');
            $this->db->set('sort_priority', '4');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->insert('mother_config');
            /* END CREATE DEFAULT DATA */
        }
    }

    public function down() {
        $this->dbforge->drop_table('log_action');
        $this->dbforge->drop_table('mother_column');
        $this->dbforge->drop_table('mother_config');
        $this->dbforge->drop_table('mother_language');
        $this->dbforge->drop_table('mother_table');
        $this->dbforge->drop_table('mother_user');
        $this->dbforge->drop_table('mother_user_group');
    }

}