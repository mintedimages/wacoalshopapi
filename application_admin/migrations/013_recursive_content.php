<?php

class Migration_Recursive_Content extends CI_Migration {

    //put your code here
    public function up() {
        $fields = array(
            'table_recursive' => array(
                'type' => 'ENUM',
                'constraint' => "'true', 'false'",
                'default' => "false"
            )
        );
        $this->dbforge->add_column('mother_table', $fields);

        //update content table 
        //add recursive_id in content table
        $query = $this->db->get('mother_table');
        foreach ($query->result_array() as $row) {
            $table = 'tbl_' . $row['table_code'];
            $fields = array(
                'recursive_id' => array(
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'default' => 0
                )
            );
            $this->dbforge->add_column($table, $fields);
        }
    }

    public function down() {
        $this->dbforge->drop_column('mother_table', 'table_recursive');

        //drop recursive_id in content table
        $query = $this->db->get('mother_table');
        foreach ($query->result_array() as $row) {
            $table = 'tbl_' . $row['table_code'];
            $this->dbforge->drop_column($table, 'recursive_id');
        }
    }

}