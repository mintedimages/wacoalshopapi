<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 005_config_group
 *
 * @author tomozard
 */
class Migration_Config_Group extends CI_Migration {

    //put your code here
    public function up() {
        $this->dbforge->add_field(array(
            'config_group_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'constraint' => '11'
            ),
            'config_group_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
            'sort_priority' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'create_date' => array(
                'type' => 'DATETIME'
            ),
            'create_by' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'update_date' => array(
                'type' => 'DATETIME'
            ),
            'update_by' => array(
                'type' => 'INT',
                'constraint' => '11'
            )
        ));
        $this->dbforge->add_key('config_group_id', TRUE);
        $this->dbforge->create_table('mother_config_group');

        $this->db->set('config_group_name', 'Main');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_config_group');

        $field_property = array(
            'config_group_id INT NOT NULL DEFAULT 1'
        );
        $this->dbforge->add_column('mother_config', $field_property);
    }

    public function down() {
        $this->dbforge->drop_table('mother_config_group');
        $this->dbforge->drop_column('mother_config', 'config_group_id');
    }

}