<?php

class Migration_Insertdefault_Config extends CI_Migration {

    public function up() {
        //clear config group
        $this->db->truncate('mother_config');
        $this->db->truncate('mother_config_group');

        //insert config group
        $this->db->set('config_group_name', 'Backend Setting');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_config_group');

        $this->db->set('config_group_name', 'Frontend Setting');
        $this->db->set('sort_priority', '2');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_config_group');

        $this->db->set('config_group_name', 'Email Setting');
        $this->db->set('sort_priority', '3');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_config_group');

        $this->db->set('config_group_name', 'Statistics Setting');
        $this->db->set('sort_priority', '4');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_config_group');

        /* BACKEND SETTING */
        /* INSERT BACKEND TITLE */
        $this->db->set('config_name', 'Backend Title');
        $this->db->set('config_code', 'site_title');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'text');
        $this->db->set('config_value', 'Backend');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '1');
        $this->db->insert('mother_config');
        /* INSERT BACKEND NAVIGATE COLOR */
        $this->db->set('config_name', 'Navigate Color');
        $this->db->set('config_code', 'navigate_color');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'color');
        $this->db->set('config_value', '#454545');
        $this->db->set('sort_priority', '2');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '1');
        $this->db->insert('mother_config');
        /* INSERT BACKEND NAVIGATE SHADOW */
        $this->db->set('config_name', 'Navigate Shadow Color');
        $this->db->set('config_code', 'navigate_color_shadow');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'color');
        $this->db->set('config_value', '#1f1f1f');
        $this->db->set('sort_priority', '3');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '1');
        $this->db->insert('mother_config');
        /* INSERT BACKEND NAVIGATE COLOR */
        $this->db->set('config_name', 'Navigate Color Active');
        $this->db->set('config_code', 'navigate_color_active');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'color');
        $this->db->set('config_value', '#6b6b6b');
        $this->db->set('sort_priority', '4');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '1');
        $this->db->insert('mother_config');

        /* Frontend Setting */
        /* INSERT Website Title */
        $this->db->set('config_name', 'Website Title');
        $this->db->set('config_code', 'website_title');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'text');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '2');
        $this->db->insert('mother_config');
        /* INSERT Favicon */
        $this->db->set('config_name', 'Favicon');
        $this->db->set('config_code', 'favicon');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'file');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '2');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '2');
        $this->db->insert('mother_config');
        /* INSERT Meta Keywords*/
        $this->db->set('config_name', 'Meta Keywords');
        $this->db->set('config_code', 'meta_keywords');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'textarea');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '3');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '2');
        $this->db->insert('mother_config');
        /* INSERT Meta Description */
        $this->db->set('config_name', 'Meta Description');
        $this->db->set('config_code', 'meta_description');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'textarea');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '4');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '2');
        $this->db->insert('mother_config');
        
        /* Email Setting */
        /* INSERT Email Account */
        $this->db->set('config_name', 'Email Account');
        $this->db->set('config_code', 'email_account');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'text');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '3');
        $this->db->insert('mother_config');
        /* INSERT Password */
        $this->db->set('config_name', 'Password');
        $this->db->set('config_code', 'password');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'text');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '2');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '3');
        $this->db->insert('mother_config');
        /* INSERT SMTP */
        $this->db->set('config_name', 'SMTP');
        $this->db->set('config_code', 'smtp');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'text');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '3');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '3');
        $this->db->insert('mother_config');
        /* INSERT Port */
        $this->db->set('config_name', 'Port');
        $this->db->set('config_code', 'port');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'text');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '4');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '3');
        $this->db->insert('mother_config');
        /* INSERT  To*/
        $this->db->set('config_name', 'To');
        $this->db->set('config_code', 'to');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'textarea');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '5');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '3');
        $this->db->insert('mother_config');
        /* INSERT  CC*/
        $this->db->set('config_name', 'CC');
        $this->db->set('config_code', 'cc');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'textarea');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '6');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '3');
        $this->db->insert('mother_config');
        /* INSERT  BCC*/
        $this->db->set('config_name', 'BCC');
        $this->db->set('config_code', 'bcc');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'textarea');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '7');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '3');
        $this->db->insert('mother_config');
        
        /* Statistics Setting */
        /* INSERT  Google Analytic*/
        $this->db->set('config_name', 'Google Analytic');
        $this->db->set('config_code', 'google_analytic');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'textarea');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '4');
        $this->db->insert('mother_config');
        /* INSERT  Google Remarketing*/
        $this->db->set('config_name', 'Google Remarketing');
        $this->db->set('config_code', 'google_remarketing');
        $this->db->set('config_type', 'admin');
        $this->db->set('config_field_type', 'textarea');
        $this->db->set('config_value', '');
        $this->db->set('sort_priority', '2');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->set('config_group_id', '4');
        $this->db->insert('mother_config');
    }

    public function down() {
        
    }

}