<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 003_insertdefualt_lang
 *
 * @author tomozard
 */
class Migration_Insertdefault_Lang extends CI_Migration {

    //put your code here
    public function up() {
        $query = $this->db->get('mother_lang');
        if ($query->num_rows() <= 0) {
            $this->db->set('lang_name', 'Thai');
            $this->db->set('lang_code', 'th');
            $this->db->set('sort_priority', '1');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->insert('mother_lang');
        }
    }

    public function down() {
        
    }

}
