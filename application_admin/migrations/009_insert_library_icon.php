<?php

class Migration_Insert_Library_Icon extends CI_Migration {

    //put your code here
    public function up() {
        // clear icon data
        $this->db->truncate('mother_icon');
        //- Insert New Icon
        //- Concept Icon
        $this->db->set('icon_name', 'Concept');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/concept.png');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
        //- News Icon
        $this->db->set('icon_name', 'News');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/news.png');
        $this->db->set('sort_priority', '2');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
        //- Contact Icon
        $this->db->set('icon_name', 'Contact');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/contact.png');
        $this->db->set('sort_priority', '3');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
        //- Facility Icon
        $this->db->set('icon_name', 'Facility');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/facility.png');
        $this->db->set('sort_priority', '4');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
        //- Floor Plan Icon
        $this->db->set('icon_name', 'Floor Plan');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/floorplan.png');
        $this->db->set('sort_priority', '5');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
        //- Gallery Icon
        $this->db->set('icon_name', 'Gallery');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/gallery.png');
        $this->db->set('sort_priority', '6');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
        //- Register Icon
        $this->db->set('icon_name', 'Register');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/register.png');
        $this->db->set('sort_priority', '7');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
        //- Room Type Icon
        $this->db->set('icon_name', 'Room type');
        $this->db->set('icon_image', '/userfiles/images/admin_icon/roomtype.png');
        $this->db->set('sort_priority', '8');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_icon');
    }

    public function down() {}

}