<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 007_update_structure
 *
 * @author tomozard
 */
class Migration_Update_Structure extends CI_Migration {

    public function up() {
        $fields = array(
            'column_remark' => array(
                'type' => 'VARCHAR',
                'constraint' => '255')
        );
        $this->dbforge->add_column('mother_column', $fields);
    }

    public function down() {
        $this->dbforge->drop_column('mother_column', 'column_remark');
    }

}