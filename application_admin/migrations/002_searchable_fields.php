<?php

class Migration_Searchable_fields extends CI_Migration {

    public function up() {
        //add searchable field to mother_column
        $fields = array(
            'searchable ENUM("Disable","Normal Search","Advance Search") NOT NULL'
        );
        $this->dbforge->add_column('mother_column', $fields);

        //create ci_query for query_string search feature
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'constraint' => '20'
            ),
            'query_string' => array(
                'type' => 'TEXT'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('ci_query');
    }

    public function down() {
        $this->dbforge->drop_column('mother_column', 'searchable');
        $this->dbforge->drop_table('ci_query');
    }

}