<?php

class Migration_Create_Library_Icon extends CI_Migration {

    //put your code here
    public function up() {
        $this->dbforge->add_field(array(
            'icon_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'constraint' => '11'
            ),
            'icon_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
            'icon_image' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'sort_priority' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'create_date' => array(
                'type' => 'DATETIME'
            ),
            'create_by' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'update_date' => array(
                'type' => 'DATETIME'
            ),
            'update_by' => array(
                'type' => 'INT',
                'constraint' => '11'
            )
        ));
        $this->dbforge->add_key('icon_id', TRUE);
        $this->dbforge->create_table('mother_icon');

        /*add column for select icon in mother table list and set default to 0*/
        $field_property = array(
            'icon_id INT NOT NULL DEFAULT 0'
        );
        $this->dbforge->add_column('mother_table', $field_property);
    }

    public function down() {
        $this->dbforge->drop_table('mother_icon');
        $this->dbforge->drop_column('mother_table', 'icon_id');
    }

}