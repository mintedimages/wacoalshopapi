<?php

class Migration_Create_Permission_Table extends CI_Migration {

    //put your code here
    public function up() {
        //create premission table
        $this->dbforge->add_field(array(
            'permission_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'constraint' => '11'
            ),
            'user_group_id' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'shop_id' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'table_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
            'view' => array(
                'type' => 'ENUM',
                'constraint' => "'true', 'false'",
                'default' => "false"
            ),
            'edit' => array(
                'type' => 'ENUM',
                'constraint' => "'true', 'false'",
                'default' => "false"
            )
        ));
        $this->dbforge->add_key('permission_id', TRUE);
        $this->dbforge->create_table('mother_permission');
    }

    public function down() {
        $this->dbforge->drop_column('mother_permission');
    }

}