<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 003_insertdefualt_lang
 *
 * @author tomozard
 */
class Migration_Multiple_Shop extends CI_Migration {

    //put your code here
    public function up() {
        $this->dbforge->add_field(array(
            'shop_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'constraint' => '11'
            ),
            'shop_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
            'shop_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
            'sort_priority' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'create_date' => array(
                'type' => 'DATETIME'
            ),
            'create_by' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
            'update_date' => array(
                'type' => 'DATETIME'
            ),
            'update_by' => array(
                'type' => 'INT',
                'constraint' => '11'
            )
        ));
        $this->dbforge->add_key('shop_id', TRUE);
        $this->dbforge->create_table('mother_shop');
        $this->db->set('shop_name', 'Main');
        $this->db->set('shop_code', 'main');
        $this->db->set('sort_priority', '1');
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', '0');
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', '0');
        $this->db->insert('mother_shop');
        $query = $this->db->get('mother_table');
        $field_property = array(
            'mother_shop_id INT NOT NULL DEFAULT 1'
        );
        foreach ($query->result() AS $table) {
            $this->dbforge->add_column('tbl_' . $table->table_code, $field_property);
            $this->db->set('tbl_' . $table->table_code . '.mother_shop_id', 1);
            $this->db->update('tbl_' . $table->table_code);
        }
    }

    public function down() {
        $this->dbforge->drop_table('mother_shop');
        $query = $this->db->get('mother_table');
        foreach ($query->result() AS $table) {
            $this->dbforge->drop_column('tbl_' . $table->table_code, 'mother_shop_id');
        }
    }

}
