<?php

class Migration_Create_Preview extends CI_Migration {

    //put your code here
    public function up() {
        $fields = array(
            'table_preview' => array(
                'type' => 'TEXT'
            )
        );
        $this->dbforge->add_column('mother_table', $fields);
    }

    public function down() {
        $this->dbforge->drop_column('mother_table', 'table_preview');
    }

}