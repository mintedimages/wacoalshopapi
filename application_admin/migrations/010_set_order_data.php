<?php

class Migration_Set_Order_Data extends CI_Migration {

    //put your code here
    public function up() {
        //update mother table
        //add table_order and table_newcontent
        $fields = array(
            'table_order' => array(
                'type' => 'ENUM',
                'constraint' => "'asc', 'desc'",
                'default' => "asc"),
            'table_newcontent' => array(
                'type' => 'ENUM',
                'constraint' => "'true', 'false'",
                'default' => "true")
        );
        $this->dbforge->add_column('mother_table', $fields);
    }

    public function down() {
        $this->dbforge->drop_column('mother_table', array('table_order', 'table_newcontent'));
    }

} 