<div id="in-sub-nav">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul>
                    <li><?php echo anchor('dashboard/', '<i class="batch plane"></i><br>Dashboard', array('title' => 'Dashboard', 'class' => ($sel_menu == 'dashboard') ? 'active' : '')); ?></li>
                    <?php
                    $rootTables = $this->StructureModel->getRootList();
                    foreach ($rootTables->result() AS $rootTable) {
                        if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission($rootTable->table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
                            $navText = '';
                            if ($rootTable->icon_id == 0) {
                                $navText = '<i class="batch forms"></i><br>' . $rootTable->table_name;
                            } else {
                                $icon = $this->IconModel->getIcon($rootTable->icon_id);
                                $navText = '<i class="batch" style="background:none;"><img src="' . $icon->icon_image . '" alt="' . $icon->icon_name . '" /></i><br>' . $rootTable->table_name;
                            }
                            echo '<li>' . anchor('backend/index/' . $rootTable->table_id, $navText, array('title' => $rootTable->table_name, 'class' => ($rootTable->table_id == $sel_menu) ? 'active' : '')) . '</li>';
                        }
                    }
                    if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission('s', 0, $this->session->userdata('user_group_id'))) {
                        ?>
                        <li><?php echo anchor('structure/content/', '<i class="batch settings"></i><br>Structure', array('title' => 'Structure Config', 'class' => ($sel_menu == 'structure') ? 'active' : '')); ?></li>
                        <?php
                    }
                    if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission('u', 0, $this->session->userdata('user_group_id')) || $this->PermissionModel->checkPermission('g', 0, $this->session->userdata('user_group_id'))) {
                        $link_path = 'user/usergroup/';
                        if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission('u',  0, $this->session->userdata('user_group_id'))) {
                            $link_path = 'user/user/';
                        }
                        ?>
                        <li><?php echo anchor($link_path, '<i class="batch users"></i><br>User', array('title' => 'User Management', 'class' => ($sel_menu == 'user') ? 'active' : '')); ?></li>
                    <?php } if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission('c', 0, $this->session->userdata('user_group_id'))) {
                        ?>
                        <li><?php echo anchor('config/', '<i class="batch settings"></i><br>Settings', array('title' => 'Settings', 'class' => ($sel_menu == 'setting') ? 'active' : '')); ?></li>
                    <?php }if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission('l', 0, $this->session->userdata('user_group_id'))) {
                        ?>
                        <li><?php echo anchor('log/', '<i class="batch b-code"></i><br>Log', array('title' => 'Log', 'class' => ($sel_menu == 'log') ? 'active' : '')); ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>