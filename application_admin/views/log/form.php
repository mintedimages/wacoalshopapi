<div class="container">
    <div class="row">
        <div class="span12">
            <div class="clearfix">
                <?php echo anchor('log/index/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
                <h4 class="header"><?php echo $title; ?></h4>
                <form action="" method="post" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label" for="">Table Name</label>
                        <div class="controls" style="padding-top: 5px;">
                            <?php echo $dat->table_name; ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="">Action Name</label>
                        <div class="controls" style="padding-top: 5px;">
                            <?php echo $dat->action_name; ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="">Action Detail</label>
                        <div class="controls" style="padding-top: 5px;">
                            <?php echo $dat->action_detail; ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="">Action Query</label>
                        <div class="controls" style="padding-top: 5px;">
                            <?php echo $dat->action_query; ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="">Date Time</label>
                        <div class="controls" style="padding-top: 5px;">
                            <?php echo $dat->create_date; ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="">By</label>
                        <div class="controls" style="padding-top: 5px;">
                            <?php echo $this->UserModel->getUserNameById($dat->create_by); ?> 
                            (
                            <?php
                            $user_group_id = $this->UserModel->getUserGroupIdById($dat->create_by);
                            if ($user_group_id > 0) {
                                echo $this->UserGroupModel->getUserGroupNameByGroupId($user_group_id);
                            } else {
                                echo 'Developer';
                            }
                            ?>
                            )
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="">IP</label>
                        <div class="controls" style="padding-top: 5px;">
                            <?php echo $dat->create_ip; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>