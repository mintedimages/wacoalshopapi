<div class="container">
    <div class="row">
        <div class="span12">
            <h4 class="header">Log</h4>
            <?php if ($query->num_rows() > 0) { ?>
                <table class="table table-striped sortable">
                    <thead>
                        <tr>
                            <th style="width:40px;" class="hidden-phone">#</th>
                            <th>Table Name</th>
                            <th>Action Name</th>
                            <th>Date Time</th>
                            <th>By</th>
                            <th style="width:100px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($query->result() AS $row) {
                            ?>
                            <tr>
                                <td class="hidden-phone"><?php echo $row->action_id; ?></td>
                                <td><?php echo anchor('log/form/' . $row->action_id, ($row->table_name != '') ? $row->table_name : '-', array('title' => 'Log Detail')); ?></td>
                                <td><?php echo anchor('log/form/' . $row->action_id, ($row->action_name != '') ? $row->action_name : '-', array('title' => 'Log Detail')); ?></td>
                                <td><?php echo anchor('log/form/' . $row->action_id, ($row->create_date != '') ? $row->create_date : '-', array('title' => 'Log Detail')); ?></td>
                                <td><?php echo anchor('log/form/' . $row->action_id, ($row->create_date != '') ? $this->UserModel->getUserNameById($row->create_by) : '-', array('title' => 'Log Detail')); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <?php echo anchor('log/form/' . $row->action_id, 'Detail', array('title' => 'Log Detail', 'class' => 'btn')); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
            <?php } ?>
            <div class="pagination pagination-centered">
                <?php
                $base_url = base_url() . 'admin.php/log/index/';
                echo $this->MyUtilitiesModel->create_pagination($base_url, $total_content, $per_page, 3);
                ?>
            </div>
        </div>
    </div>
</div>