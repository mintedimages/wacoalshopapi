<div class="container">
    <div class="row">
        <div class="span3">
            <h4>Config Group</h4>
            <div class="sidebar">
                <ul class="col-nav span3">
                    <?php
                    $query = $this->ConfigGroupModel->getQuery();
                    foreach ($query->result_array() as $row) {
                        ?>
                        <li><?php echo anchor('config/index/' . $row['config_group_id'], $row['config_group_name'], array('title' => $row['config_group_name'], 'class' => '')); ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="span9">
            <?php echo $content; ?>
        </div>
    </div>
</div>