
<div class="clearfix">
    <h4 class="header">Config</h4>
</div>
<?php echo form_open_multipart('config/form_post/' . $config_group_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<?php
$columns = $this->ConfigModel->getColumnList($config_group_id);
foreach ($columns->result_array() AS $column) {
    echo $this->ConfigModel->displayField($column, null, (isset($column['config_value'])) ? $column['config_value'] : '');
}
?>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
    </div>
</div>
<?php echo form_close(); ?>
