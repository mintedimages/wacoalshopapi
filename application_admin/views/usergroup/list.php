<?php echo  anchor('user/usergroup/form/', '<i class="icon icon-plus"></i> New Group', array('title' => 'New User Group', 'class' => 'btn pull-right')); ?>
<h4 class="header">User Group</h4>
<?php if ($query->num_rows() > 0) { ?>
    <table class="table table-striped sortable">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Group</th>
                <th>User</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() AS $row) {
                ?>
                <tr>
                    <td class="hidden-phone"><?php echo  $row->user_group_id; ?></td>
                    <td><?php echo  anchor('user/usergroup/form/' . $row->user_group_id, ($row->user_group_name != '') ? $row->user_group_name : '-', array('title' => 'User Group Detail')); ?></td>
                    <td><?php echo  anchor('user/usergroup/form/' . $row->user_group_id, $row->count_user, array('title' => 'User Group Detail')); ?></td>
                    <td>
                        <div class="btn-group">
                            <?php echo  anchor('user/usergroup/form/' . $row->user_group_id, 'Edit', array('title' => 'User Group Detail', 'class' => 'btn')); ?>
                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php echo  anchor('user/usergroup/delete/' . $row->user_group_id, 'Delete', array('title' => 'Delete')); ?>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>