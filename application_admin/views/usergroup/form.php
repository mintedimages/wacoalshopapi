<ul class="breadcrumb">
    <li>
        <?php echo anchor('user/usergroup/', 'User Group'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo $title; ?>
    </li>
</ul>
<div class="clearfix">
    <?php echo anchor('user/usergroup/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
    <h4 class="header"><?php echo $title; ?></h4>
</div>
<?php echo form_open_multipart('user/usergroup/form_post/' . $user_group_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="user_group_name">Group Name</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="user_group_name" id="user_group_name" <?php echo ($user_group_id != 0) ? 'value="' . $dat->user_group_name . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="permission">Group Permission</label>
    <div class="controls">
        <ul>
            <li><input type="checkbox" name="permission[]" value="s" <?php echo $this->PermissionModel->checkPermissionSelect('s', $user_group_id, 0); ?>> <i class="icon-file"></i> Manage Structure</li>
            <li><input type="checkbox" name="permission[]" value="u" <?php echo $this->PermissionModel->checkPermissionSelect('u', $user_group_id, 0); ?>> <i class="icon-file"></i> Manage User</li>
            <li><input type="checkbox" name="permission[]" value="g" <?php echo $this->PermissionModel->checkPermissionSelect('g', $user_group_id, 0); ?>> <i class="icon-file"></i> Manage User Group</li>
            <li><input type="checkbox" name="permission[]" value="c" <?php echo $this->PermissionModel->checkPermissionSelect('c', $user_group_id, 0); ?>> <i class="icon-file"></i> Manage Config</li>
            <li><input type="checkbox" name="permission[]" value="l" <?php echo $this->PermissionModel->checkPermissionSelect('l', $user_group_id, 0); ?>> <i class="icon-file"></i> Log</li>
        </ul>
        <?php
        $query = $this->ShopModel->queryShopList();
        foreach ($query->result_array() as $row) {
            ?>
            <ul id="checkboxes">
                <li>
                    <?php $checked = $this->PermissionModel->checkShopSelect($row['shop_id'], $user_group_id); ?>
                    <input type="checkbox" name="shop_<?php echo $row['shop_id']; ?>" value="true" <?php echo $checked; ?>> <i class="icon-file"></i> <?php echo $row['shop_name']; ?>
                    <?php echo $this->PermissionModel->listCategoryPermission(0, 0, $user_group_id, $row['shop_id']); ?>
                </li>
            </ul>
            <?php
        }
        ?>
        <?php ?>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php echo anchor('user/usergroup/', 'Back', array('title' => 'User Group List', 'class' => 'btn btn-inverse')); ?>
    </div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
    $(function() {
        $('ul#checkboxes input[type="checkbox"]').each(
        function() {
            $(this).bind('click change', function() {
                if ($(this).is(':checked')) {
                    $(this).siblings('ul').find('input[type="checkbox"]').prop('checked', true);
                    $(this).parents('ul').siblings('input[type="checkbox"]').prop('checked', true);
                } else {
                    $(this).siblings('ul').find('input[type="checkbox"]').prop('checked', false);
                }
            });
        })
    });
</script>