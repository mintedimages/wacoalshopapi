<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="description" content="Back End">
        <meta name="author" content="Mintedimages">
        <title><?php echo $this->ConfigModel->getConfig('site_title'); ?></title>
        <?php echo  link_tag('asset_admin/css/bootstrap.css'); ?>
        <?php echo  link_tag('asset_admin/css/bootstrap-responsive.css'); ?>
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic">
        <?php echo  link_tag('asset_admin/css/styles.css'); ?>
        <?php echo  link_tag('asset_admin/css/toastr.css'); ?>
        <?php echo  link_tag('asset_admin/css/fullcalendar.css'); ?>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/bootstrap.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/jquery.knob.js"></script>
        <script src="http://d3js.org/d3.v3.min.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/jquery.sparkline.min.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/toastr.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/jquery.tablesorter.min.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/jquery.peity.min.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/fullcalendar.min.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/bootstrap-colorpicker.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/gcal.js"></script>
        <script src="<?php echo  base_url(); ?>asset_admin/js/setup.js"></script>
    </head>
    <body>
        <div id="in-nav">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <?php echo  anchor(base_url(), '<h4>' . $this->ConfigModel->getConfig('site_title') . '</h4>', array('id' => 'logo')); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="span6 offset2">
                    <div class="login">
                        <?php echo  form_open('welcome/login', array('class' => 'form-horizontal')); ?>
                        <div class="control-group">
                            <div class="controls">
                                <h4>Login</h4>
                            </div>
                        </div>
                        <?php
                            $shopList = $this->ShopModel->queryShopList();
                            if ($shopList->num_rows() > 1) {
                                ?>
                                <div class="control-group">
                                    <label for="shop_id" class="control-label">Shop </label>
                                    <div class="controls">
                                        <select name="shop_id" id="shop_id">
                                            <?php foreach ($shopList->result() AS $shop) { ?>
                                                <option value="<?php echo $shop->shop_id; ?>" <?php echo ($shop->shop_id == $this->session->userdata('mother_shop_id')) ? 'selected="selected"' : ''; ?> ><?php echo $shop->shop_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                        <?php } ?>
                        <div class="control-group">
                            <label for="user_login_name" class="control-label">Login Name </label>
                            <div class="controls">
                                <input id="user_login_name" name="user_login_name" type="text" placeholder="Login Name" value="<?php echo  $user_login_name; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="user_login_password" class="control-label">Password </label>
                            <div class="controls">
                                <input id="user_login_password" name="user_login_password" type="password" placeholder="Password"/>
                            </div>
                        </div>
                        <div class="control-group"> 
                            <div class="controls">
                                <button type="submit" class="btn">Login</button>
                            </div>
                        </div>
                        <?php form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        <?php
            if($err_id) {
        ?>
            (function() {
                toastr.options = {
                    positionClass: 'toast-top-right'
                };
                
                toastr.error('Your Login Name or Password not found in system, Please login agian.', 'Login Error !');
            }).call(this);
        <?php
            }
        ?>
        </script>
    </body>
</html>