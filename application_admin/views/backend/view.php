<?php
    if($condition_selection == 'listview') {
        foreach ($query->result_array() AS $row) {
        ?>
        <tr data-parameter="<?php echo $table_id.'/'.$row[$table_code . '_id'].'/'.$parent_id; ?>">
            <td>
                <input type="checkbox" name="checkbox[]" id="checkbox<?php echo $row[$table_code . '_id']; ?>" class="checkbox" value="<?php echo $row[$table_code . '_id']; ?>"/>
            </td>
            <td class="hidden-phone"><?php echo $row[$table_code . '_id']; ?></td>
            <?php
            $columns = $this->StructureModel->getColumnList($table_id, 1);
            foreach ($columns->result() AS $column) {
                $value = '';
                if ($column->column_field_type == 'dropdown' || $column->column_field_type == 'radio') {
                    $value = $this->BackendModel->getFirstField($column->column_relation_table, $row[$column->column_code]);
                } else if ($column->column_field_type == 'checkbox') {
                    $code = $row[$column->column_code];
                    if ($code != '') {
                        $value_array = explode(",", $code);
                        foreach ($value_array as $val) {
                            if ($value != '') {
                                $value .= ',';
                            }
                            $value .= $this->BackendModel->getFirstField($column->column_relation_table, $val);
                        }
                    } else {
                        $value = '-';
                    }
                } else if ($column->column_field_type == 'image') {
                    $value = '<img src="' . $row[$column->column_code] . '" width="200" />';
                } else {
                    $value = $row[$column->column_code];
                }
                ?>
                <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row[$column->column_code] != '') ? $value : '-', array('title' => $table_name . ' Detail')); ?></td>
                <?php
            }

            $children_table = $this->StructureModel->getChildTableList($table_id);
            foreach ($children_table->result() AS $child_table) {
                ?>
                <td>
                    <?php echo anchor('backend/index/' . $child_table->table_id . '/' . $row[$table_code . '_id'], $this->BackendModel->getCountContentList($child_table->table_id, $row[$table_code . '_id']), array('title' => $child_table->table_name . ' List')); ?>
                </td>
            <?php } ?>
            <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row['enable_status'] == 'show') ? 'SHOW' : 'HIDE', array('title' => $table_name . ' Detail')); ?></td>
            <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, $row['create_date'], array('title' => $table_name . ' Detail')); ?></td>
            <td>
                <div class="btn-group">
                    <?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, 'Edit', array('title' => 'Content Detail', 'class' => 'btn')); ?>
                    <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>
                            <?php echo anchor('backend/moveup/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Up', array('title' => 'Move Up')); ?>
                            <?php echo anchor('backend/movedown/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Down', array('title' => 'Move Down')); ?>
                            <?php echo anchor('backend/delete/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <?php
        }
    }
    else if($condition_selection == 'gridview') {
        $index = 0;
        foreach ($query->result_array() AS $row) {
            $no_margin = '';
            if($index == 3) {
                $index = 0;
                $no_margin = 'no-margin';
            }
        ?>
        <div data-parameter="<?php echo $table_id.'/'.$row[$table_code . '_id'].'/'.$parent_id; ?>" class="span4 gridview <?php echo $no_margin; ?>">
            <div class="gridview-header">
                <input type="checkbox" name="checkbox[]" id="checkbox<?php echo $row[$table_code . '_id']; ?>" class="checkbox" value="<?php echo $row[$table_code . '_id']; ?>"/>
                <span># <?php echo $row[$table_code . '_id']; ?></span>
                <div class="btn-group">
                    <?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, 'Edit', array('title' => 'Content Detail', 'class' => 'btn')); ?>
                    <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>
                            <?php echo anchor('backend/moveup/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Up', array('title' => 'Move Up')); ?>
                            <?php echo anchor('backend/movedown/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Down', array('title' => 'Move Down')); ?>
                            <?php echo anchor('backend/delete/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                        </li>
                    </ul>
                </div>
            </div>
            <table class="table table-striped">
                <tbody>
                <?php
                    $columns = $this->StructureModel->getColumnList($table_id, 1);
                    foreach ($columns->result() AS $column) {
                        $value = '';
                        if ($column->column_field_type == 'dropdown' || $column->column_field_type == 'radio') {
                            $value = $this->BackendModel->getFirstField($column->column_relation_table, $row[$column->column_code]);
                        }
                        else if ($column->column_field_type == 'checkbox') {
                            $code = $row[$column->column_code];
                            if ($code != '') {
                                $value_array = explode(",", $code);
                                foreach ($value_array as $val) {
                                    if ($value != '') {
                                        $value .= ',';
                                    }
                                    $value .= $this->BackendModel->getFirstField($column->column_relation_table, $val);
                                }
                            } else {
                                $value = '-';
                            }
                        }
                        else if ($column->column_field_type == 'image') {
                            $value = '<img src="' . $row[$column->column_code] . '" width="200" />';
                        }
                        else {
                            $value = $row[$column->column_code];
                        }
                    ?>
                        <tr>
                            <th><?php echo  $column->column_name; ?></th>
                            <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row[$column->column_code] != '') ? $value : '-', array('title' => $table_name . ' Detail')); ?></td>
                        </tr>
                    <?php
                    }

                    $children_table = $this->StructureModel->getChildTableList($table_id);
                    foreach ($children_table->result() AS $child_table) {
                    ?>
                        <tr>
                            <th><?php echo  $child_table->table_name; ?></th>
                            <td><?php echo anchor('backend/index/' . $child_table->table_id . '/' . $row[$table_code . '_id'], $this->BackendModel->getCountContentList($child_table->table_id, $row[$table_code . '_id']), array('title' => $child_table->table_name . ' List')); ?></td>
                        </tr>
                    <?php
                    }
                ?>
                    <tr>
                        <th>STATUS</th>
                        <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row['enable_status'] == 'show') ? 'SHOW' : 'HIDE', array('title' => $table_name . ' Detail')); ?></td>
                    </tr>
                    <tr>
                        <th>DATE</th>
                        <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, $row['create_date'], array('title' => $table_name . ' Detail')); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <?php
            $index++;
        }
    }
?>