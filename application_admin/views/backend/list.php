<div class="container">
    <div class="row">
        <div class="span3">
            <h4 class="header">Search</h4>
            <?php echo form_open('backend/search/' . $table_id . '/' . $parent_id . '/1', array('id' => 'frm-search')); ?>
            <fieldset>
                <?php
                $getSearchColumnList = $this->StructureModel->getSearchColumnList($table_id);
                if ($getSearchColumnList->num_rows() > 0) {
                    foreach ($getSearchColumnList->result_array() AS $row) {
                        if ($row['column_lang'] > 0) {
                            $queryLang = $this->LangModel->queryLangName();
                            foreach ($queryLang->result_array() AS $lang) {
                                echo $this->FieldTypeModel->displayField($row, $lang, (isset($query_array[$row['column_code'] . '_' . $lang['lang_code']])) ? $query_array[$row['column_code'] . '_' . $lang['lang_code']] : '', true);
                            }
                        } else {
                            echo $this->FieldTypeModel->displayField($row, null, (isset($query_array[$row['column_code']])) ? $query_array[$row['column_code']] : '', true);
                        }
                    }
                }
                ?>
                <input type="hidden" id="per_page" name="per_page" value="<?php echo $per_page; ?>">
                <button type="submit" class="btn">Search</button>
            </fieldset>
            <?php echo form_close(); ?>
        </div>
        <div class="span9">
            <ul class="breadcrumb">
                <?php echo $this->BackendModel->createBreadCrumb($table_id, 0, $parent_id, 0); ?>
            </ul>
            <div class="clearfix">
                <div class="pull-right">
                    <div class="btn-group">
                        <input id="del-top" class="pull-left btn btn-danger del" type="button" value="Delete">
                    </div>
                    <div class="btn-group">
                        <?php
                        $disabled = $this->PermissionModel->checkEditPermission($table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'));
                        if ($table_newcontent == 'true') {
                            ?>
                            <?php echo anchor('backend/form/' . $table_id . '/0/' . $parent_id . '/0/', '<i class="icon icon-plus"></i> New ' . $table_name, array('title' => 'New Content', 'class' => 'btn ' . $disabled)); ?>
                        <?php } ?>
                        <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li>
                                <?php echo anchor('backend/exportdata/' . $table_id . '/' . $parent_id, '<i class="icon icon-download"></i> Export Data', array('target' => '_blank', 'class' => '')); ?>
                                <?php echo anchor('backend/exportdataexcel/' . $table_id . '/' . $parent_id, '<i class="icon icon-download"></i> Export Data : Excel', array('target' => '_blank', 'class' => '')); ?>
                                <a href="#importModal" role="button" data-toggle="modal"><i class="icon icon-upload"></i> Import Data</a>
                                <a href="#importExcelModal" role="button" data-toggle="modal"><i class="icon icon-upload"></i> Import Data : Excel</a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <label class="lbl-showtext">Show per page</label>
                        <select id="show_per_page" class="input-mini">
                            <option value="20" <?php echo ($per_page == 20) ? 'selected="selected"' : ''; ?> >20</option>
                            <option value="50" <?php echo ($per_page == 50) ? 'selected="selected"' : ''; ?> >50</option>
                            <option value="100" <?php echo ($per_page == 100) ? 'selected="selected"' : ''; ?> >100</option>
                            <option value="<?php echo $total_content; ?>" <?php echo ($per_page == $total_content) ? 'selected="selected"' : ''; ?> >All</option>
                        </select>
                    </div>
                    <div class="btn-group" data-toggle="buttons-radio">
                        <button type="button" class="btn-view btn btn-primary active" data-toggle="listview"><i class="icon-white icon-th-list"></i></button>
                        <button type="button" class="btn-view btn btn-primary" data-toggle="gridview"><i class="icon-white icon-th"></i></button>
                    </div>
                </div>
                <h4 class="header"><?php echo $table_name; ?></h4>
                <?php if ($total_content > 0) { ?>
                    <?php echo form_open('backend/deletecheck/' . $table_id . '/' . $parent_id, array('id' => 'frm-showlist')); ?>
                    <table id="listview" class="table table-striped view-style">
                        <thead>
                            <tr>
                                <th style="width:40px;"><input type="checkbox" name="checkall" id="checkall" value="all"/></th>
                                <th style="width:40px;" class="hidden-phone">#</th>
                                <?php
                                $columns = $this->StructureModel->getColumnList($table_id, 1);
                                foreach ($columns->result() AS $column) {
                                    ?>
                                    <th><?php echo $column->column_name; ?></th>
                                    <?php
                                }
                                $children_table = $this->StructureModel->getChildTableList($table_id);
                                foreach ($children_table->result() AS $child_table) {
                                    ?>
                                    <th><?php echo $child_table->table_name; ?></th>
                                <?php } ?>
                                <th style="width:100px;">STATUS</th>
                                <th style="width:100px;">DATE</th>
                                <th style="width:100px;"></th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
                            <?php foreach ($query->result_array() AS $row) {
                                ?>
                                <tr data-parameter="<?php echo $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id; ?>">
                                    <td>
                                        <input type="checkbox" name="checkbox[]" id="checkbox<?php echo $row[$table_code . '_id']; ?>" class="checkbox" value="<?php echo $row[$table_code . '_id']; ?>"/>
                                    </td>
                                    <td class="hidden-phone"><?php echo $row[$table_code . '_id']; ?></td>
                                    <?php
                                    $columns = $this->StructureModel->getColumnList($table_id, 1);
                                    foreach ($columns->result() AS $column) {
                                        $value = '';
                                        if ($column->column_field_type == 'dropdown' || $column->column_field_type == 'radio') {
                                            $value = $this->BackendModel->getFirstField($column->column_relation_table, $row[$column->column_code]);
                                            if ($value == '') {
                                                $value = '-';
                                            }
                                        } else if ($column->column_field_type == 'checkbox') {
                                            $code = $row[$column->column_code];
                                            if ($code != '') {
                                                $value_array = explode(",", $code);
                                                foreach ($value_array as $val) {
                                                    if ($value != '') {
                                                        $value .= ', ';
                                                    }
                                                    $value .= $this->BackendModel->getFirstField($column->column_relation_table, $val);
                                                }
                                            } else {
                                                $value = '-';
                                            }
                                        } else if ($column->column_field_type == 'image') {
                                            $value = '<img src = "' . $row[$column->column_code] . '" width = "200" />';
                                        } else {
                                            $value = $row[$column->column_code];
                                        }
                                        ?>
                                        <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id. '/' . $row['recursive_id'], ($row[$column->column_code] != '') ? $value : '-', array('title' => $table_name . ' Detail')); ?></td>
                                        <?php
                                    }

                                    $children_table = $this->StructureModel->getChildTableList($table_id);
                                    foreach ($children_table->result() AS $child_table) {
                                        ?>
                                        <td>
                                            <?php echo anchor('backend/index/' . $child_table->table_id . '/' . $row[$table_code . '_id'], $this->BackendModel->getCountContentList($child_table->table_id, $row[$table_code . '_id']), array('title' => $child_table->table_name . ' List')); ?>
                                        </td>
                                    <?php } ?>
                                    <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id . '/' . $row['recursive_id'], ($row['enable_status'] == 'show') ? 'SHOW' : 'HIDE', array('title' => $table_name . ' Detail')); ?></td>
                                    <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id . '/' . $row['recursive_id'], $row['create_date'], array('title' => $table_name . ' Detail')); ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id. '/' . $row['recursive_id'], ($disabled == 'disabled') ? 'View' : 'Edit', array('title' => 'Content Detail', 'class' => 'btn')); ?>
                                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <?php
                                                    $getPreviewUrl = $this->StructureModel->getPreviewUrl($table_id);
                                                    echo ($getPreviewUrl != "") ? anchor(base_url() . 'index.php/' . $getPreviewUrl . '' . $row[$table_code . '_id'], 'Preview', array('title' => 'Preview', 'target' => '_blank')) : '';
                                                    ?>
                                                    <?php
                                                    $isRecursive = $this->StructureModel->isRecursive($table_id);
                                                    echo ($isRecursive) ? anchor('backend/form/' . $table_id . '/0/' . $parent_id . '/' . $row[$table_code . '_id'], 'Create Sub', array('title' => 'Create Sub')) : '';
                                                    ?>
                                                    <?php echo anchor('backend/moveup/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Up', array('title' => 'Move Up')); ?>
                                                    <?php echo anchor('backend/movedown/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Down', array('title' => 'Move Down')); ?>
                                                    <?php echo anchor('backend/delete/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <input class="pull-left btn btn-danger del" type="submit" value="Delete"> 
                                    <a class="pull-right btn btn-info" href="<?php echo base_url(); ?>admin.php/backend/export/<?php echo $table_id; ?>/<?php echo $parent_id; ?>" style="float:right;">Export Excel</a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <div id="gridview" class="view-style hide">
                        <div class="row-fluid sortable">
                            <?php
                            $index = 0;
                            foreach ($query->result_array() AS $row) {
                                $no_margin = '';
                                if ($index == 3) {
                                    $index = 0;
                                    $no_margin = 'no-margin';
                                }
                                ?>
                                <div data-parameter="<?php echo $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id; ?>" class="span4 gridview <?php echo $no_margin; ?>">
                                    <div class="gridview-header">
                                        <input type="checkbox" name="checkbox[]" id="checkbox<?php echo $row[$table_code . '_id']; ?>" class="checkbox" value="<?php echo $row[$table_code . '_id']; ?>"/>
                                        <span># <?php echo $row[$table_code . '_id']; ?></span>
                                        <div class="btn-group">
                                            <?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, 'Edit', array('title' => 'Content Detail', 'class' => 'btn')); ?>
                                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <?php echo anchor('backend/moveup/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Up', array('title' => 'Move Up')); ?>
                                                    <?php echo anchor('backend/movedown/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Down', array('title' => 'Move Down')); ?>
                                                    <?php echo anchor('backend/delete/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <table class="table table-striped">
                                        <tbody>
                                            <?php
                                            $columns = $this->StructureModel->getColumnList($table_id, 1);
                                            foreach ($columns->result() AS $column) {
                                                $value = '';
                                                if ($column->column_field_type == 'dropdown' || $column->column_field_type == 'radio') {
                                                    $value = $this->BackendModel->getFirstField($column->column_relation_table, $row[$column->column_code]);
                                                } else if ($column->column_field_type == 'checkbox') {
                                                    $code = $row[$column->column_code];
                                                    if ($code != '') {
                                                        $value_array = explode(",", $code);
                                                        foreach ($value_array as $val) {
                                                            if ($value != '') {
                                                                $value .= ', ';
                                                            }
                                                            $value .= $this->BackendModel->getFirstField($column->column_relation_table, $val);
                                                        }
                                                    } else {
                                                        $value = '-';
                                                    }
                                                } else if ($column->column_field_type == 'image') {
                                                    $value = '<img src = "' . $row[$column->column_code] . '" width = "200" />';
                                                } else {
                                                    $value = $row[$column->column_code];
                                                }
                                                ?>
                                                <tr>
                                                    <th><?php echo $column->column_name; ?></th>
                                                    <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row[$column->column_code] != '') ? $value : '-', array('title' => $table_name . ' Detail')); ?></td>
                                                </tr>
                                                <?php
                                            }

                                            $children_table = $this->StructureModel->getChildTableList($table_id);
                                            foreach ($children_table->result() AS $child_table) {
                                                ?>
                                                <tr>
                                                    <th><?php echo $child_table->table_name; ?></th>
                                                    <td><?php echo anchor('backend/index/' . $child_table->table_id . '/' . $row[$table_code . '_id'], $this->BackendModel->getCountContentList($child_table->table_id, $row[$table_code . '_id']), array('title' => $child_table->table_name . ' List')); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            <tr>
                                                <th>STATUS</th>
                                                <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row['enable_status'] == 'show') ? 'SHOW' : 'HIDE', array('title' => $table_name . ' Detail')); ?></td>
                                            </tr>
                                            <tr>
                                                <th>DATE</th>
                                                <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, $row['create_date'], array('title' => $table_name . ' Detail')); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <?php
                                $index++;
                            }
                            ?>
                        </div>
                        <div class="row-fluid">
                            <input class="pull-left btn btn-danger del" type="submit" value="Delete">
                            <a class="pull-right btn btn-info" href="<?php echo base_url(); ?>admin.php/backend/export/<?php echo $table_id; ?>/<?php echo $parent_id; ?>" style="float:right;">Export Excel</a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                <?php } else { ?>
                    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
                <?php } ?>
                <div class="pagination pagination-centered">
                <?php
                    $base_url = base_url() . 'admin.php/backend/index/' . $table_id . '/' . $parent_id . '/' . $query_id . '/';
                    echo $this->MyUtilitiesModel->create_pagination($base_url, $total_content, $per_page, 6);
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $("#show_per_page").on('change', function(){
            $per_page = $(this).val();
            $('#per_page').val($per_page);
            $('#frm-search').submit();
        });
        
        $('.delete').click(function() {
            return (confirm('Do you want to delete this field ?') == true) ? true : false;
        });

        //- view button toggle
        $('.btn-view').button();
        $('.btn-view').click(function() {
            $view = $(this).data('toggle');

            $('#' + $view).css('opacity', '0');

            var url = window.location.href;
            // var new_url = url.substring(url.indexOf('index')).split('/');
            // console.log(new_url[0].replace(/index/g, "query_sort_priority"));

            var get_url = url.replace(/index/i, "query_sort_priority");
            console.clear();
            console.log(get_url, $view);
            $.post(get_url, {condition: $view}, function(result) {
                // console.log('loading...');

                $('#' + $view + ' .sortable').empty();
                $('#' + $view + ' .sortable').html(result);

                $('.view-style').animate({
                    opacity: 0
                }, 400, function() {
                    $('.view-style').addClass('hide');

                    $('#' + $view).removeClass('hide');
                    $('#' + $view).animate({
                        opacity: 1
                    }, 400);

                    // console.log('success');
                });
            });
        });

        //- fix table tr an original width
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        };

        // here, allow the user to sort the items
        $('#listview .sortable').sortable({
            axis: "y",
            cursor: "move",
            containment: 'tbody.sortable',
            helper: fixHelperModified,
            start: function(e, ui) {
                ui.placeholder.height(ui.item.height());
            },
            stop: function(e, ui) {
                $page = <?php echo $page; ?>;
                $per_page = <?php echo $per_page; ?>;
                $parameter = ui.item.data('parameter');
                $priority = (parseInt($('tr[data-parameter="' + $parameter + '"]', $(this)).index()) + 1);
                if ($page > 1) {
                    $priority += $per_page;
                }

                $.post("<?php echo base_url(); ?>admin.php/backend/update_sort_priority/" + $parameter, {sort_priority: $priority});
            }
        }).disableSelection();

        $('#gridview .sortable').sortable({
            cursor: "move",
            containment: 'div.sortable',
            start: function(e, ui) {
                ui.placeholder.height(ui.item.height());

                var $index = 0;
                $('.gridview').removeClass('no-margin');
                $('.gridview').each(function(index) {
                    // ($('.gridview:eq(' + index + ')').hasClass('.ui-sortable-placeholder')) ? $('.gridview:eq(' + index + ')').addClass('no-margin') : '';
                    if ($index === 3) {
                        $index = 0;
                        ($('.gridview:eq(' + index + ')').hasClass('.ui-sortable-placeholder')) ? $('.gridview:eq(' + index + ')').addClass('no-margin') : '';
                    }
                    $index++;
                });
            },
            stop: function(e, ui) {
                var $index = 0;
                $('.gridview').removeClass('no-margin');
                $('.gridview').each(function(index) {
                    if ($index === 3) {
                        $index = 0;
                        $('.gridview:eq(' + index + ')').addClass('no-margin');
                    }
                    $index++;
                });

                $page = <?php echo $page; ?>;
                $per_page = <?php echo $per_page; ?>;
                $parameter = ui.item.data('parameter');
                $priority = (parseInt($('div[data-parameter="' + $parameter + '"]', $(this)).index()) + 1);
                if ($page > 1) {
                    $priority += $per_page;
                }

                $.post("<?php echo base_url(); ?>admin.php/backend/update_sort_priority/" + $parameter, {sort_priority: $priority});
            }
        }).disableSelection();
    });

<?php
    if ($del_status == 'delete') {
        ?>
                (function() {
                    toastr.options = {
                        positionClass: 'toast-top-right'
                    };

                    toastr.success('Delete data successfully.', 'Success');
                }).call(this);
        <?php
    }
?>
</script>
<div id="importModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <?php echo form_open_multipart('backend/importdata/' . $table_id . '/' . $parent_id, array('style' => 'margin:0;')); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="importModalLabel">Import Data</h3>
    </div>
    <div class="modal-body">
        <input type="file" name="file_data" id="file_data" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="submit" class="btn btn-primary">Import Data</button>
    </div>
    <?php echo form_close(); ?>
</div>
<div id="importExcelModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <?php echo form_open_multipart('backend/importdataexcel/' . $table_id . '/' . $parent_id, array('style' => 'margin:0;')); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="importModalLabel">Import Excel Data</h3>
    </div>
    <div class="modal-body">
        <input type="file" name="file_data" id="file_data" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="submit" class="btn btn-primary">Import Data</button>
    </div>
    <?php echo form_close(); ?>
</div>
