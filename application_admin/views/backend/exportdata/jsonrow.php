"tbl_<?php echo $table->table_code; ?>":[
<?php
$start_row = 0;
foreach ($query_data->result_array() as $row_data) {
    if ($start_row > 0) {
        echo ',';
    }
    echo '{';
    $start_column = 0;
    echo '"' . $table->table_code . '_id": ' . $row_data[$table->table_code . '_id'] . ',';
    echo '"mother_shop_id": ' . $row_data['mother_shop_id'] . ',';
    echo '"parent_id": ' . $row_data['parent_id'] . ',';
    foreach ($query_column->result() as $row_column) {
        if ($row_column->column_lang == 0) {
            echo '"' . $row_column->column_code . '": "' . $row_data[$row_column->column_code] . '",';
        } else {
            foreach ($query_lang->result() as $row_lang) {
                echo '"' . $row_column->column_code . '_' . $row_lang->lang_code . '": "' . $row_data[$row_column->column_code . '_' . $row_lang->lang_code] . '",';
            }
        }
        $start_column++;
    }
    echo '"recursive_id": ' . $row_data['recursive_id'] . ',';
    echo '"sort_priority": ' . $row_data['sort_priority'] . ',';
    echo '"enable_status": "' . $row_data['enable_status'] . '",';
    echo '"create_date": "' . $row_data['create_date'] . '",';
    echo '"create_by": ' . $row_data['create_by'] . ',';
    echo '"update_date": "' . $row_data['update_date'] . '",';
    echo '"update_by": ' . $row_data['update_by'] . '';
    $this->db->where('parent_table_id', $table_id);
    $this->db->order_by('sort_priority', 'asc');
    $queryTable = $this->db->get('mother_table');
    foreach ($queryTable->result() as $rowChildTable) {
        echo ",";
        $this->ExportDataModel->printData($rowChildTable->table_id, $row_data[$table->table_code . '_id']);
    }
    echo '}';
    $start_row++;
}
?>
]