<?php $disabled = $this->PermissionModel->checkEditPermission($table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id')); ?>
<tr data-parameter="<?php echo $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id; ?>">
    <td>
        <input type="checkbox" name="checkbox[]" id="checkbox<?php echo $row[$table_code . '_id']; ?>" class="checkbox" value="<?php echo $row[$table_code . '_id']; ?>"/>
    </td>
    <td class="hidden-phone"><?php echo $row[$table_code . '_id']; ?></td>
    <?php
    $columns = $this->StructureModel->getColumnList($table_id, 1);
    foreach ($columns->result() AS $column) {
        $value = '';
        if ($column->column_field_type == 'dropdown' || $column->column_field_type == 'radio') {
            $value = $this->BackendModel->getFirstField($column->column_relation_table, $row[$column->column_code]);
        } else if ($column->column_field_type == 'checkbox') {
            $code = $row[$column->column_code];
            if ($code != '') {
                $value_array = explode(",", $code);
                foreach ($value_array as $val) {
                    if ($value != '') {
                        $value .= ', ';
                    }
                    $value .= $this->BackendModel->getFirstField($column->column_relation_table, $val);
                }
            } else {
                $value = '-';
            }
        } else if ($column->column_field_type == 'image') {
            $value = '<img src = "' . $row[$column->column_code] . '" width = "200" />';
        } else {
            $value = $row[$column->column_code];
        }
        ?>
        <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row[$column->column_code] != '') ? $value : '-', array('title' => $table_name . ' Detail')); ?></td>
        <?php
    }

    $children_table = $this->StructureModel->getChildTableList($table_id);
    foreach ($children_table->result() AS $child_table) {
        ?>
        <td>
            <?php echo anchor('backend/index/' . $child_table->table_id . '/' . $row[$table_code . '_id'], $this->BackendModel->getCountContentList($child_table->table_id, $row[$table_code . '_id']), array('title' => $child_table->table_name . ' List')); ?>
        </td>
    <?php } ?>
    <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($row['enable_status'] == 'show') ? 'SHOW' : 'HIDE', array('title' => $table_name . ' Detail')); ?></td>
    <td><?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, $row['create_date'], array('title' => $table_name . ' Detail')); ?></td>
    <td>
        <div class="btn-group">
            <?php echo anchor('backend/form/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $parent_id, ($disabled == 'disabled') ? 'View' : 'Edit', array('title' => 'Content Detail', 'class' => 'btn')); ?>
            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li>
                    <?php
                    $getPreviewUrl = $this->StructureModel->getPreviewUrl($table_id);
                    echo ($getPreviewUrl != "") ? anchor(base_url() . 'index.php/' . $getPreviewUrl . '' . $row[$table_code . '_id'], 'Preview', array('title' => 'Preview', 'target' => '_blank')) : '';
                    ?>
                    <?php
                    $isRecursive = $this->StructureModel->isRecursive($table_id);
                    echo ($isRecursive) ? anchor('backend/form/' . $table_id . '/0/' . $parent_id . '/' . $row[$table_code . '_id'], 'Create Sub', array('title' => 'Create Sub')) : '';
                    ?>
                    <?php echo anchor('backend/moveup/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Up', array('title' => 'Move Up')); ?>
                    <?php echo anchor('backend/movedown/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Move Down', array('title' => 'Move Down')); ?>
                    <?php echo anchor('backend/delete/' . $table_id . '/' . $row[$table_code . '_id'] . '/' . $page, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                </li>
            </ul>
        </div>
    </td>
</tr>