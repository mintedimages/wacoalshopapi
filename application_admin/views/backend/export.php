<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="' . $filename . '.xls'); #ชื่อไฟล์
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns="http://www.w3.org/TR/REC-html40">
    <HTML>
        <HEAD>
            <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        </HEAD>
        <BODY>
            <table x:str BORDER="1">
                <thead>
                    <tr>
                        <th>No.</th>
                        <?php
                        $columns = $this->StructureModel->getColumnList($table_id, 0);
                        foreach ($columns->result() AS $column) {
                            ?>
                            <th><?php echo $column->column_name; ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody class="content">
                    <?php $count = 0; ?>
                    <?php
                    foreach ($query->result_array() AS $row) {
                        $count++;
                        ?>
                        <tr>
                            <td><?php echo $count; ?></td>
                            <?php
                            $columns = $this->StructureModel->getColumnList($table_id, 0);
                            foreach ($columns->result() AS $column) {
                                ?>
                                <td><?php echo (isset($row[$column->column_code]) && $row[$column->column_code] != '') ? $row[$column->column_code] : '-'; ?></td>
                            <?php } ?>
                            <td><?php echo $row['create_date'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </BODY>
    </HTML>