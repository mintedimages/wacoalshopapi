<div class="container">
    <div class="row">
        <?php
        $cols2 = 'span12';
        if ($content_id != 0) {
            $children_table = $this->StructureModel->getChildTableList($table_id);
            if ($children_table->num_rows() > 0) {
                $cols2 = 'span9';
                ?>
                <div class="span3">
                    <h4>Sub Navigate</h4>
                    <div class="sidebar">
                        <?php echo $this->Subnavigatemodel->createSubNavigate($content_id, $table_id); ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <div class="<?php echo $cols2; ?>">
            <ul class="breadcrumb">
                <?php echo $this->BackendModel->createBreadCrumb($table_id, $content_id, $parent_id, 1); ?>
            </ul>
            <div class="clearfix">
                <?php echo anchor('backend/index/' . $table_id . '/' . $parent_id, 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
                <h4 class="header"><?php echo $title; ?></h4>
            </div>
            <?php echo form_open_multipart('backend/form_post/' . $table_id . '/' . $content_id . '/' . $parent_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
            <?php if ($table_recursive == 'true') { ?>
                <div class="control-group">
                    <label class="control-label" for="recursive_id">Parent</label>
                    <div class="controls">
                        <select name="recursive_id" id="recursive_id" class="input-large" <?php echo ($table_recursive == 'false') ? 'disabled="disabled"' : ''; ?>> 
                            <option value="0" <?php echo ($recursive_id == 0) ? 'selected="selected"' : ''; ?>>root</option>

                            <?php $this->BackendModel->showRecursiveDropdown($table_id, $parent_id, 0, 1, $query_array, $this->session->userdata('mother_shop_id'), 0, 0); ?>
                        </select>
                    </div>
                </div>
            <?php } ?>
            <?php
            $columns = $this->StructureModel->getColumnList($table_id);
            foreach ($columns->result_array() AS $column) {
                if ($column['column_lang'] > 0) {
                    $queryLang = $this->LangModel->queryLangName();
                    foreach ($queryLang->result_array() AS $row) {
                        echo $this->FieldTypeModel->displayField($column, $row, (isset($dat[$column['column_code'] . '_' . $row['lang_code']])) ? $dat[$column['column_code'] . '_' . $row['lang_code']] : '');
                    }
                } else {
                    echo $this->FieldTypeModel->displayField($column, null, (isset($dat[$column['column_code']])) ? $dat[$column['column_code']] : '');
                }
            }
            ?>
            <?php if ($table_type !== 'static') { ?>
                <div class="control-group">
                    <label class="control-label" for="sort_priority">Priority</label>
                    <div class="controls">
                        <?php
                        if ($content_id != 0) {
                            $sort_priority_select = $dat['sort_priority'];
                        } else if ($content_id == 0 && $table_order == 'asc') {
                            $sort_priority_select = $max_priority;
                        } else if ($content_id == 0 && $table_order == 'desc') {
                            $sort_priority_select = 1;
                        }
                        ?>
                        <select name="sort_priority" id="sort_priority" class="input-large">
                            <?php for ($i = 1; $i <= $max_priority; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php echo ($sort_priority_select == $i) ? 'selected="selected"' : ''; ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="enable_status">Status</label>
                    <div class="controls">
                        <select name="enable_status" id="enable_status" class="input-large">
                            <option value="show" <?php echo ($content_id != 0 && $dat['enable_status'] == 'show') ? 'selected="selected"' : ''; ?>>Show</option>
                            <option value="hide" <?php echo ($content_id != 0 && $dat['enable_status'] == 'hide') ? 'selected="selected"' : ''; ?>>Hide</option>
                        </select>
                    </div>
                </div>
            <?php } ?>
            <div class="control-group">
                <div class="controls">
                    <?php
                    $disabled = $this->PermissionModel->checkEditPermission($table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'));
                    if ($disabled == 'enabled') {
                        ?>
                        <button type="submit" class="btn btn-primary">
                            Save Changes
                        </button>
                        <?php
                    }
                    ?>
                    <?php
                    /*
                      if ($content_id != 0) {
                      $children_table = $this->StructureModel->getChildTableList($table_id);
                      foreach ($children_table->result() AS $child_table) {
                      ?>
                      <?php echo anchor('backend/index/' . $child_table->table_id . '/' . $content_id, $child_table->table_name . '(' . $this->BackendModel->getCountContentList($child_table->table_id, $content_id) . ')', array('title' => $child_table->table_name . ' List', 'class' => 'btn')); ?>
                      <?php
                      }
                      }
                     */
                    ?>
                    <?php echo anchor('backend/index/' . $table_id . '/' . $parent_id, 'Back', array('title' => 'Content List', 'class' => 'btn btn-inverse')); ?>
                    <?php
                    $getPreviewUrl = $this->StructureModel->getPreviewUrl($table_id);
                    echo ($getPreviewUrl != "") ? anchor(base_url() . 'index.php/' . $getPreviewUrl . '' . $content_id, 'Preview', array('title' => 'Preview', 'target' => '_blank', 'class' => 'btn btn-link')) : '';
                    ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
<?php
if ($status == 'success') {
    ?>
            (function() {
                toastr.options = {
                    positionClass: 'toast-top-right'
                };

                toastr.success('Save data successfully.', 'Success');
            }).call(this);
    <?php
}
?>
</script>