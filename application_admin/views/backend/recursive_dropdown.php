<?php
if ($row[$table_code . '_id'] != $content_id) {
    $columns = $this->StructureModel->getColumnList($table_id, 1);
    foreach ($columns->result() AS $column) {
        $value = '';
        if ($column->column_field_type == 'dropdown' || $column->column_field_type == 'radio') {
            $value = $this->BackendModel->getFirstField($column->column_relation_table, $row[$column->column_code]);
        } else if ($column->column_field_type == 'checkbox') {
            $code = $row[$column->column_code];
            if ($code != '') {
                $value_array = explode(",", $code);
                foreach ($value_array as $val) {
                    if ($value != '') {
                        $value .= ', ';
                    }
                    $value .= $this->BackendModel->getFirstField($column->column_relation_table, $val);
                }
            } else {
                $value = '-';
            }
        } else if ($column->column_field_type == 'image') {
            $value = '<img src = "' . $row[$column->column_code] . '" width = "200" />';
        } else {
            $value = $row[$column->column_code];
        }

        if ($content_id > 0) {
            $recursive_active = $dat['recursive_id'];
        } else {
            $recursive_active = $recursive_id;
        }
        $space = '';
        for ($i = 0; $i < $level; $i++) {
            $space .= '&nbsp;&nbsp;&nbsp;';
        }
        ?>
        <option value="<?php echo $row[$table_code . '_id']; ?>" <?php echo ($row[$table_code . '_id'] == $recursive_active) ? 'selected="selected"' : ''; ?>>
            <?php echo $space . $value; ?>
        </option>
        <?php
    }
}
?>