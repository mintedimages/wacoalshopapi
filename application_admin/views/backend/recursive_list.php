<div class="container">
    <div class="row">
        <div class="span3">
            <h4 class="header">Search</h4>
            <?php echo form_open('backend/search/' . $table_id . '/' . $parent_id . '/1', array('id' => 'frm-search')); ?>
            <fieldset>
                <?php
                $getSearchColumnList = $this->StructureModel->getSearchColumnList($table_id);
                if ($getSearchColumnList->num_rows() > 0) {
                    foreach ($getSearchColumnList->result_array() AS $row) {
                        if ($row['column_lang'] > 0) {
                            $queryLang = $this->LangModel->queryLangName();
                            foreach ($queryLang->result_array() AS $lang) {
                                echo $this->FieldTypeModel->displayField($row, $lang, (isset($query_array[$row['column_code'] . '_' . $lang['lang_code']])) ? $query_array[$row['column_code'] . '_' . $lang['lang_code']] : '', true);
                            }
                        } else {
                            echo $this->FieldTypeModel->displayField($row, null, (isset($query_array[$row['column_code']])) ? $query_array[$row['column_code']] : '', true);
                        }
                    }
                }
                ?>
                <button type="submit" class="btn">Search</button>
            </fieldset>
            <?php echo form_close(); ?>
        </div>
        <div class="span9">
            <ul class="breadcrumb">
                <?php echo $this->BackendModel->createBreadCrumb($table_id, 0, $parent_id, 0); ?>
            </ul>
            <div class="clearfix">
                <div class="pull-right">
                    <div class="btn-group">
                        <?php
                        if ($table_newcontent == 'true') {
                            $disabled = $this->PermissionModel->checkEditPermission($table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'));
                            ?>
                            <?php echo anchor('backend/form/' . $table_id . '/0/' . $parent_id . '/0/', '<i class="icon icon-plus"></i> New ' . $table_name, array('title' => 'New Content', 'class' => 'btn ' . $disabled)); ?>
                        <?php } ?>
                        <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li>
                                <?php echo anchor('backend/exportdata/' . $table_id . '/' . $parent_id, '<i class="icon icon-download"></i> Export Data', array('target' => '_blank', 'class' => '')); ?>
                                <?php echo anchor('backend/exportdataexcel/' . $table_id . '/' . $parent_id, '<i class="icon icon-download"></i> Export Data : Excel', array('target' => '_blank', 'class' => '')); ?>
                                <a href="#importModal" role="button" data-toggle="modal"><i class="icon icon-upload"></i> Import Data</a>
                                <a href="#importExcelModal" role="button" data-toggle="modal"><i class="icon icon-upload"></i> Import Data : Excel</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <h4 class="header"><?php echo $table_name; ?></h4>
                <?php if ($query->num_rows > 0) { ?>
                    <?php echo form_open('backend/deletecheck/' . $table_id . '/' . $parent_id); ?>
                    <table id="listview" class="table table-striped view-style">
                        <thead>
                            <tr>
                                <th style="width:40px;"><input type="checkbox" name="checkall" id="checkall" value="all"/></th>
                                <th style="width:40px;" class="hidden-phone">#</th>
                                <?php
                                $columns = $this->StructureModel->getColumnList($table_id, 1);
                                foreach ($columns->result() AS $column) {
                                    ?>
                                    <th><?php echo $column->column_name; ?></th>
                                    <?php
                                }
                                $children_table = $this->StructureModel->getChildTableList($table_id);
                                foreach ($children_table->result() AS $child_table) {
                                    ?>
                                    <th><?php echo $child_table->table_name; ?></th>
                                <?php } ?>
                                <th style="width:100px;">STATUS</th>
                                <th style="width:100px;">DATE</th>
                                <th style="width:100px;"></th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
                            <?php $this->BackendModel->showRecursiveList($table_id, $parent_id, 0, 1, $query_array, $this->session->userdata('mother_shop_id'), 0, 0); ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <input class="pull-left btn btn-danger del" type="submit" value="Delete"> 
                                    <a class="pull-right btn btn-info" href="<?php echo base_url(); ?>admin.php/backend/export/<?php echo $table_id; ?>/<?php echo $parent_id; ?>" style="float:right;">Export Excel</a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php echo form_close(); ?>
                <?php } else { ?>
                    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $("#show_per_page").on('change', function(){
            $per_page = $(this).val();
            $('#per_page').val($per_page);
            $('#frm-search').submit();
        });
        
        $('.delete').click(function() {
            return (confirm('Do you want to delete this field ?') == true) ? true : false;
        });

        //- view button toggle
        $('.btn-view').button();
        $('.btn-view').click(function() {
            $view = $(this).data('toggle');

            $('#' + $view).css('opacity', '0');

            var url = window.location.href;
            // var new_url = url.substring(url.indexOf('index')).split('/');
            // console.log(new_url[0].replace(/index/g, "query_sort_priority"));

            var get_url = url.replace(/index/g, "query_sort_priority");
            //console.clear();
            //console.log(get_url);
            $.get(get_url, {condition: $view}, function(result) {
                // console.log('loading...');

                $('#' + $view + ' .sortable').empty();
                $('#' + $view + ' .sortable').html(result);

                $('.view-style').animate({
                    opacity: 0
                }, 400, function() {
                    $('.view-style').addClass('hide');

                    $('#' + $view).removeClass('hide');
                    $('#' + $view).animate({
                        opacity: 1
                    }, 400);

                    // console.log('success');
                });
            });
        });

        //- fix table tr an original width
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        };
    });

<?php
if ($del_status == 'delete') {
    ?>
            (function() {
                toastr.options = {
                    positionClass: 'toast-top-right'
                };

                toastr.success('Delete data successfully.', 'Success');
            }).call(this);
    <?php
}
?>
</script>
<div id="importModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <?php echo form_open_multipart('backend/importdata/' . $table_id . '/' . $parent_id, array('style' => 'margin:0;')); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="importModalLabel">Import Data</h3>
    </div>
    <div class="modal-body">
        <input type="file" name="file_data" id="file_data" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="submit" class="btn btn-primary">Import Data</button>
    </div>
    <?php echo form_close(); ?>
</div>
<div id="importExcelModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <?php echo form_open_multipart('backend/importdataexcel/' . $table_id . '/' . $parent_id, array('style' => 'margin:0;')); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="importModalLabel">Import Excel Data</h3>
    </div>
    <div class="modal-body">
        <input type="file" name="file_data" id="file_data" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="submit" class="btn btn-primary">Import Data</button>
    </div>
    <?php echo form_close(); ?>
</div>