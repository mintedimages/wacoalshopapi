<?php
$this->output->set_header("HTTP/1.0 200 OK");
$this->output->set_header("HTTP/1.1 200 OK");
//$last_update = date('D, d M Y H:i:s', time());
$this->output->set_header('Last-Modified: ' . date('D, d M Y H:i:s', time()) . ' GMT');
$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
$this->output->set_header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="description" content="Back End">
        <meta name="author" content="Mintedimages">
        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title><?php echo $this->ConfigModel->getConfig('site_title'); ?></title>
        <?php echo link_tag('asset_admin/css/bootstrap.css'); ?>
        <?php echo link_tag('asset_admin/css/bootstrap-responsive.css'); ?>
        <?php echo link_tag('asset_admin/css/colorpicker.css'); ?>
        <?php echo link_tag('asset_admin/css/datepicker.css'); ?>
        <?php echo link_tag('asset_admin/css/timepicker.css'); ?>
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic">
        <?php echo link_tag('asset_admin/css/styles.css'); ?>
        <?php echo link_tag('asset_admin/css/toastr.css'); ?>
        <?php echo link_tag('asset_admin/css/fullcalendar.css'); ?>
        <?php echo link_tag('asset_admin/css/custom.css'); ?>
        <?php echo link_tag('asset_admin/css/jquery.Jcrop.css'); ?>
        <?php echo link_tag('asset_admin/select2/select2.css'); ?>
        <?php echo link_tag('asset_admin/select2/custom-select2.css'); ?>
        <?php echo link_tag('asset_admin/css/croppic.css'); ?>
        <style>
            #in-sub-nav{
                background-color:<?php echo $this->ConfigModel->getConfig('navigate_color'); ?>;
                -webkit-box-shadow:inset 0px 0px 10px <?php echo $this->ConfigModel->getConfig('navigate_color_shadow'); ?>;
                box-shadow:inset 0px 0px 10px <?php echo $this->ConfigModel->getConfig('navigate_color_shadow'); ?>;
            }
            #in-sub-nav li a:hover, #in-sub-nav li a.active{background-color:<?php echo $this->ConfigModel->getConfig('navigate_color_active'); ?>;}
        </style>
        <!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script-->
        <!--<script src="<?php echo base_url(); ?>asset_admin/js/jquery.js"></script>-->
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery-1.9.1.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-colorpicker.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery.knob.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/d3.v3.min.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/toastr.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery.tablesorter.min.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery.peity.min.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/fullcalendar.min.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/gcal.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery.slug.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/setup.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/jquery.Jcrop.js"></script>
        <script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script> 
        <script src="<?php echo base_url(); ?>ckfinder/ckfinder.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/select2/select2.min.js"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/croppic.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $(".select2").select2();
            });
        </script>
    </head>
    <body>
        <div class="wrapper">
            <div id="in-nav">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <ul class="pull-right">
                                <li>Welcome <?php echo $this->session->userdata('user_login_name'); ?></li>
                                <li><?php echo anchor('welcome/logout/', 'Sign Out', array('title' => 'Sign Out')); ?></li>
                            </ul>
                            <h4 id="logo" class="pull-left"><?php echo $this->ConfigModel->getConfig('site_title'); ?></h4>
                            <div class="pull-left">
                                <?php
                                $shopList = $this->ShopModel->queryShopList();
                                if ($shopList->num_rows() > 1) {
                                    ?>
                                    <?php echo form_open('welcome/changeShop', array('id' => 'formChangeShop')); ?>
                                    <select name="change_shop" id="change_shop">
                                        <?php foreach ($shopList->result() AS $shop) { ?>
                                            <option value="<?php echo $shop->shop_id; ?>" <?php echo ($shop->shop_id == $this->session->userdata('mother_shop_id')) ? 'selected="selected"' : ''; ?> ><?php echo $shop->shop_name; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php echo form_close(); ?>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            $('#change_shop').change(function() {
                                                $('#formChangeShop').submit();
                                            });
                                        });
                                    </script>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->load->view('navigate'); ?>
            <div>
                <div class="page">
                    <div class="page-container">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
            <div class="push"></div>
        </div>
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <p class="pull-right">Copyright &copy; 2013 Created by <a href="http://www.mintedimages.com/" target="_blank">Mintedimages</a></p>
                        <p>Tel : <a href="tel:023742155">023742155</a></p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
    </body>
</html>