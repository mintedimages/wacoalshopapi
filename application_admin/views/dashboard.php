<div class="container">
    <div class="row">
        <div class="span12">
            <ul class="breadcrumb">
                Dashboard
            </ul>
            <div class="clearfix">
                <h4 class="header">Dashboard</h4>
                <?php if ($query->num_rows() > 0) { ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Content</th>
                                <th style="width:100px;">RECORD</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($query->result() AS $row) {
                                ?>
                                <tr>
                                    <td><?php echo  anchor('backend/index/' . $row->table_id, $row->table_name, array('title' => $row->table_name, 'class' => '')); ?></td>
                                    <td><?php echo  $this->BackendModel->getCountContentList($row->table_id, 0); ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>