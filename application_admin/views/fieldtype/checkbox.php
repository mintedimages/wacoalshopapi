<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <?php
        $relateTable = $this->StructureModel->getTable($relation_table);
        $query = $this->BackendModel->getContentList($relation_table, 0);
        $value_array = explode(",", $value);
        foreach ($query->result_array() AS $row) {
            $checked = "";
            foreach ($value_array as $val) {
                if ($val == $row[$relateTable->table_code . '_id']) {
                    $checked = 'checked';
                }
            }
            ?>
            <input type="checkbox" name="<?php echo $code; ?>[]" value="<?php echo $row[$relateTable->table_code . '_id']; ?>" <?php echo $checked; ?>> <?php echo $this->BackendModel->getFirstField($relation_table, $row[$relateTable->table_code . '_id']); ?>
            <?php
        }
        ?>
    </div>
</div>