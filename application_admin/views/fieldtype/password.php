<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <input type="text" class="input-block-level" id="<?php echo $code; ?>" name="<?php echo $code; ?>" value="" />
    </div>
</div>