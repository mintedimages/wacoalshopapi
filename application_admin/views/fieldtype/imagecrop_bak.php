<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>'">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <div id="<?php echo $code . '_div_preview_container'; ?>" class="well well-small" style="display:none;">
            <div id="<?php echo $code . '_div_preview'; ?>" style="overflow:hidden;">
                <img src="" id="<?php echo $code . '_preview'; ?>" style="max-width:none;" />
            </div>
            <div>Preview Crop Image</div>
        </div>
        <div id="<?php echo $code; ?>_default">
            <div id="<?php echo $code; ?>_default_img">
                <?php
                $data = '';
                if ($value != "") {
                    if (@getimagesize($_SERVER['DOCUMENT_ROOT'] . $value)) {
                        $info = @getimagesize($_SERVER['DOCUMENT_ROOT'] . $value);
                        if ($info[0] > 250) {
                            $data .= '<img src="' . $value . '?rand=' . time() . '" alt="' . $value . '" width="250"/><br/>';
                        } else {
                            $data .= '<img src="' . $value . '?rand=' . time() . '" alt="' . $value . '"/><br/>';
                        }
                    } else {
                        $data .= '<img src="' . $value . '?rand=' . time() . '" alt="' . $value . '"/><br/>';
                    }
                }
                echo $data;
                ?>
            </div>
            <button type="button" class="btn btn-primary" onclick="CropImage<?php echo $code; ?>();">Crop Image</button>
        </div>
        <div id="<?php echo $code; ?>_form_crop" style="display:none;" class="well well-small">
            <img src="" id="img_<?php echo $code; ?>" style="max-width:none;" />
            <input type="hidden" class="input-xxlarge" id="<?php echo $code; ?>" name="<?php echo $code; ?>" value="<?php echo $value; ?>" /> 
            <input type="hidden" class="input-xxlarge" id="<?php echo $code; ?>_master_image" name="<?php echo $code; ?>_master_image" /> 
            <input type="hidden" name="<?php echo $code . '_crop_config'; ?>" id="<?php echo $code . '_crop_config'; ?>" value="<?php echo $column_option; ?>" />
            <input type="hidden" name="<?php echo $code . '_crop_target_w'; ?>" id="<?php echo $code . '_crop_target_w'; ?>" />
            <input type="hidden" name="<?php echo $code . '_crop_target_h'; ?>" id="<?php echo $code . '_crop_target_h'; ?>" />
            <input type="hidden" name="<?php echo $code . '_crop_x'; ?>" id="<?php echo $code . '_crop_x'; ?>" />
            <input type="hidden" name="<?php echo $code . '_crop_y'; ?>" id="<?php echo $code . '_crop_y'; ?>" />
            <input type="hidden" name="<?php echo $code . '_crop_w'; ?>" id="<?php echo $code . '_crop_w'; ?>" />
            <input type="hidden" name="<?php echo $code . '_crop_h'; ?>" id="<?php echo $code . '_crop_h'; ?>" />
            <button type="button" class="btn btn-primary" onclick="BrowseServer<?php echo $code; ?>();">Browse Image</button>
            <button type="button" class="btn btn-success" onclick="ConfirmChange<?php echo $code; ?>();">Confirm Crop</button>
            <button type="button" class="btn btn-danger" onclick="Cancel<?php echo $code; ?>();">Cancel</button>
        </div>
        <script type="text/javascript">
                var <?php echo $code; ?>_jcrop_api = false;
                var <?php echo $code; ?>_bounds, <?php echo $code; ?>_boundx, <?php echo $code; ?>_boundy;
                eval("var optionColumn<?php echo $code; ?> = " + $('#<?php echo $code . '_crop_config'; ?>').val() + ";");
                $(document).ready(function() {
                    $previewDiv = $('#<?php echo $code . '_div_preview'; ?>');
                    $previewDiv.width(optionColumn<?php echo $code; ?>.imagecropWidth);
                    $previewDiv.height(optionColumn<?php echo $code; ?>.imagecropHeight);
                    $('#<?php echo $code . '_crop_target_w'; ?>').val(optionColumn<?php echo $code; ?>.imagecropWidth);
                    $('#<?php echo $code . '_crop_target_h'; ?>').val(optionColumn<?php echo $code; ?>.imagecropHeight);
                });
                function CropImage<?php echo $code; ?>() {
                    $code = '<?php echo $code; ?>';
                    $('#' + $code + '_default').hide();
                    $('#' + $code + '_default_img').hide();
                    $('#' + $code + '_div_preview_container').show();
                    $('#' + $code + '_div_preview').show();
                    $('#' + $code + '_form_crop').show();
                }
                function ConfirmChange<?php echo $code; ?>() {
                    $code = '<?php echo $code; ?>';
                    $('#' + $code + '_default').show();
                    $('#' + $code + '_default_img').hide();
                    $('#' + $code + '_div_preview_container').show();
                    $('#' + $code + '_div_preview').show();
                    $('#' + $code + '_form_crop').hide();
                }
                function Cancel<?php echo $code; ?>() {
                    $code = '<?php echo $code; ?>';
                    $('#' + $code + '_default').show();
                    $('#' + $code + '_default_img').show();
                    $('#' + $code + '_div_preview_container').hide();
                    $('#' + $code + '_div_preview').hide();
                    $('#' + $code + '_form_crop').hide();
                    $('#' + $code + '_master_image').val('');
                }
                function BrowseServer<?php echo $code; ?>() {
                    var finder = new CKFinder();
                    finder.selectActionFunction = SetFileField<?php echo $code; ?>;
                    finder.popup();
                }
                function SetFileField<?php echo $code; ?>(fileUrl) {
                    var $code = '<?php echo $code; ?>';
                    var optionColumn = optionColumn<?php echo $code; ?>;
                    document.getElementById($code + '_master_image').value = fileUrl;
                    //show image from ckfinder
                    $('#img_' + $code).attr('src', fileUrl).width('auto').height('auto');
                    $('#' + $code + '_preview').attr('src', fileUrl);
                    var jcrop_api = eval($code + '_jcrop_api');
                    if (jcrop_api !== false) {
                        jcrop_api.destroy();
                    }
                    $('#img_' + $code).Jcrop({
                        onChange: eval('showPreview' + $code),
                        onSelect: eval('showPreview' + $code),
                        aspectRatio: optionColumn.imagecropWidth / optionColumn.imagecropHeight
                    }, function() {
                        eval($code + '_jcrop_api = this');
                        eval($code + '_bounds = ' + $code + '_jcrop_api.getBounds()');
                        eval($code + '_boundx = ' + $code + '_bounds[0]');
                        eval($code + '_boundy = ' + $code + '_bounds[1]');
                    });
                }
                function showPreview<?php echo $code; ?>(coords)
                {
                    $code = '<?php echo $code; ?>';
                    var boundx = eval($code + '_boundx');
                    var boundy = eval($code + '_boundy');
                    if (parseInt(coords.w) > 0)
                    {
                        var rx = optionColumn<?php echo $code; ?>.imagecropWidth / coords.w;
                        var ry = optionColumn<?php echo $code; ?>.imagecropHeight / coords.h;
                        $('#' + $code + '_crop_x').val(Math.round(coords.x));
                        $('#' + $code + '_crop_y').val(Math.round(coords.y));
                        $('#' + $code + '_crop_w').val(coords.w);
                        $('#' + $code + '_crop_h').val(coords.h);
                        $('#' + $code + '_preview').css({
                            width: Math.round(rx * boundx) + 'px',
                            height: Math.round(ry * boundy) + 'px',
                            marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                            marginTop: '-' + Math.round(ry * coords.y) + 'px'
                        });
                    }
                }
        </script>
    </div>
</div>