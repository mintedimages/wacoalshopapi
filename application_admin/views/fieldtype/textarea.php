<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <textarea class="input-block-level"  rows="4" id="<?php echo $code; ?>" name="<?php echo $code; ?>"><?php echo $value; ?></textarea>
    </div>
</div>