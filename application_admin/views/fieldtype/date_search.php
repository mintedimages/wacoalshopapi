<?php
if ($value == '') {
    $value = $this->MyUtilitiesModel->thisDate();
}
?>
<div class="control-group">
    <label class="control-label" for="<?php echo  $code; ?>_start"><?php echo  $name; ?></label>
    <div class="controls">
        <input type="text" class="input-small datepicker" id="<?php echo  $code; ?>_start" name="<?php echo  $code; ?>_start" value="<?php echo  $value; ?>" data-date-format="yyyy-mm-dd"/>
        -
        <input type="text" class="input-small datepicker" id="<?php echo  $code; ?>_end" name="<?php echo  $code; ?>_end" value="<?php echo  $value; ?>" data-date-format="yyyy-mm-dd"/>
    </div>
</div>