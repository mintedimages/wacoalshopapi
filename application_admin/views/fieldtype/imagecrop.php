<?php
$bodytag = str_replace("'", '"', $column_option);
$arr = (array) json_decode($bodytag, true);
?>
<style>
    #croppic_<?php echo $code; ?> {
        width: <?php echo $arr['imagecropWidth']; ?>px;
        height: <?php echo $arr['imagecropHeight']; ?>px;
        position:relative; /* or fixed or absolute */
        margin: 0;
        border:2px solid #ddd;
        box-shadow:none;
        margin-bottom: 10px;
        background:#eee;
    }
    #croppic_<?php echo $code; ?> img {max-width: none;}
</style>
<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>'">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <div id="croppic_<?php echo $code; ?>"><img src="<?php echo $value; ?>"/></div>
        <input type="hidden" name="<?php echo $code; ?>" id="cropOutput_<?php echo $code; ?>" value="<?php echo $value; ?>"/>
        <span class="btn" id="cropContainerHeaderButton_<?php echo $code; ?>">Upload Image</span>
    </div>
</div>
<script type="text/javascript">
    var croppicHeaderOptions = {
        uploadUrl: '<?php echo base_url('admin.php/backend/uploadImage'); ?>',
        cropData: {
            "dummyData": 1,
            "dummyData2": "asdas"
        },
        imgEyecandy: true,
        imgEyecandyOpacity: 0,
        cropUrl: '<?php echo base_url('admin.php/backend/uploadImageCrop'); ?>',
        customUploadButtonId: 'cropContainerHeaderButton_<?php echo $code; ?>',
        modal: false,
        outputUrlId: 'cropOutput_<?php echo $code; ?>',
        loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
        onBeforeImgUpload: function() {
            console.log('onBeforeImgUpload')
            $('#croppic_<?php echo $code; ?>').html('');
        },
        onAfterImgUpload: function() {
            console.log('onAfterImgUpload')
        },
        onImgDrag: function() {
            console.log('onImgDrag')
        },
        onImgZoom: function() {
            console.log('onImgZoom')
        },
        onBeforeImgCrop: function() {
            console.log('onBeforeImgCrop')
        },
        onAfterImgCrop: function() {
            console.log('onAfterImgCrop')
        }
    }
    var croppic = new Croppic('croppic_<?php echo $code; ?>', croppicHeaderOptions);
</script>
