<?php
if ($value == '') {
    $value = $this->MyUtilitiesModel->thisDate();
}
?>
<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <input type="text" class="input-small datepicker" id="<?php echo $code; ?>" name="<?php echo $code; ?>" value="<?php echo $value; ?>" data-date-format="yyyy-mm-dd"/>
    </div>
</div>