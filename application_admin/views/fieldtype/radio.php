<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <?php
        $relateTable = $this->StructureModel->getTable($relation_table);
        $query = $this->BackendModel->getContentList($relation_table, 0);
        foreach ($query->result_array() AS $row) {
            ?>
            <input type="radio" name="<?php echo $code; ?>" value="<?php echo $row[$relateTable->table_code . '_id']; ?>" <?php echo ($row[$relateTable->table_code . '_id'] == $value) ? ' checked="checked"' : ''; ?>> <?php echo $this->BackendModel->getFirstField($relation_table, $row[$relateTable->table_code . '_id']); ?>
            <?php
        }
        ?>
    </div>
</div>