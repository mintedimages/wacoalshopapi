<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <input type="upload" class="input-xxlarge" id="<?php echo $code; ?>" name="<?php echo $code; ?>" value="<?php echo $value; ?>" /> 
        <button type="button" class="btn btn-primary" onclick="BrowseServer<?php echo $code; ?>();">Browse Server</button>
        <script type="text/javascript">
            function BrowseServer<?php echo $code; ?>() {
                var finder = new CKFinder();
                finder.selectActionFunction = SetFileField<?php echo $code; ?>;
                finder.popup();
            }
            function SetFileField<?php echo $code; ?>(fileUrl) {
                document.getElementById('<?php echo $code; ?>').value = fileUrl;
            }
        </script>
    </div>
</div>