<div class="control-group">
    <label class="control-label" for="<?php echo $code; ?>">
        <?php echo $name; ?>
        <?php if (isset($remark)) { ?>
            <br/><small><?php echo $remark; ?></small>
        <?php } ?>
    </label>
    <div class="controls">
        <?php
        $relateTable = $this->StructureModel->getTable($relation_table);
        $query = $this->BackendModel->getContentList($relation_table, 0);
        ?>

        <select name="<?php echo $code; ?>">
            <option value="0" <?php echo ($value == "") ? ' selected="selected"' : ''; ?>>-</option>
        <?php
            $this->BackendModel->showRecursiveFieldtypeDropdown($relation_table,0,0,0,array(),0,0,0);
        ?>
        </select>
    </div>
</div>