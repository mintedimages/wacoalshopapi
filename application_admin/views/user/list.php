<?php echo  anchor('user/user/form/', '<i class="icon icon-plus"></i> New User', array('title' => 'New User', 'class' => 'btn pull-right')); ?>
<h4 class="header">User</h4>
<?php if ($query->num_rows() > 0) { ?>
    <table class="table table-striped sortable">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>User</th>
                <th>Group</th>
                <th>Status</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() AS $row) {
                ?>
                <tr>
                    <td class="hidden-phone"><?php echo  $row->user_id; ?></td>
                    <td><?php echo  anchor('user/user/form/' . $row->user_id, ($row->user_login_name != '') ? $row->user_login_name : '-', array('title' => 'User Detail')); ?></td>
                    <td><?php echo  anchor('user/user/form/' . $row->user_id, ($row->user_group_name != '') ? $row->user_group_name : (($row->user_group_id == 0)?'Developer':''), array('title' => 'User Detail')); ?></td>
                    <td><?php echo  anchor('user/user/form/' . $row->user_id, ($row->enable_status != '') ? $row->enable_status : '-', array('title' => 'User Detail')); ?></td>
                    <td>
                        <div class="btn-group">
                            <?php echo  anchor('user/user/form/' . $row->user_id, 'Edit', array('title' => 'User Detail', 'class' => 'btn')); ?>
                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php echo  anchor('user/user/changepassword/' . $row->user_id, 'Change Password', array('title' => 'Change Password')); ?>
                                </li>
                                <li>
                                    <?php echo  anchor('user/user/delete/' . $row->user_id, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <script type="text/javascript">
        $(function(){
            $('.delete').click(function(){
                return (confirm('Do you want to delete this field ?') == true) ? true : false;
            });
        });
    </script>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>