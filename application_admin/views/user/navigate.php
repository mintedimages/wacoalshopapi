<div class="container">
    <div class="row">
        <div class="span3">
            <h4>User</h4>
            <div class="sidebar ">
                <ul class="col-nav span3">
                    <?php
                    if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission('u', 0, $this->session->userdata('user_group_id'))) {
                        ?>
                        <li><?php echo  anchor('user/user/', 'User', array('title' => 'Manage User Admin')); ?></li>
                        <?php
                    }
                    if ($this->session->userdata('user_group_id') == 0 || $this->PermissionModel->checkNavigatePermission('g', 0, $this->session->userdata('user_group_id'))) {
                        ?>
                        <li><?php echo  anchor('user/usergroup/', 'User Group', array('title' => 'User Group Permission')); ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="span9">
            <?php echo  $content; ?>
        </div>
    </div>
</div>