<ul class="breadcrumb">
    <li>
        <?php echo anchor('user/user/', 'User'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo $title; ?>
    </li>
</ul>
<div class="clearfix">
    <?php echo anchor('user/user/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
    <h4 class="header"><?php echo $title; ?></h4>
</div>
<?php echo form_open_multipart('user/user/form_post/' . $user_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label">User Group</label>
    <div class="controls">
        <select name="user_group_id" id="user_group_id" class="input-large">
            <?php if ($this->session->userdata('user_group_id') == 0) { ?>
                <option value="0" <?php echo ($user_id != 0 && $dat->user_group_id == 0 ) ? 'selected="selected"' : ''; ?>>Developer</option>
            <?php } ?>
            <?php
            $query = $this->db->get('mother_user_group');
            foreach ($query->result() AS $row) {
                ?>
                <option value="<?php echo $row->user_group_id ?>" <?php echo ($user_id != 0 && $dat->user_group_id == $row->user_group_id ) ? 'selected="selected"' : ''; ?>><?php echo $row->user_group_name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="user_login_name">Login Name</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="user_login_name" id="user_login_name" <?php echo ($user_id != 0) ? 'value="' . $dat->user_login_name . '"' : ''; ?> />
    </div>
</div>
<?php if ($user_id == 0) { ?>
    <div class="control-group">
        <label class="control-label" for="user_login_password">Password</label>
        <div class="controls">
            <input class="input-xxlarge" type="password" name="user_login_password" id="user_login_password" />
        </div>
    </div>
<?php } ?>
<div class="control-group">
    <label class="control-label" for="enable_status">Status</label>
    <div class="controls">
        <select name="enable_status" id="enable_status" class="input-large">
            <option value="enable" <?php echo ($user_id != 0 && $dat->enable_status == 'enable') ? 'selected="selected"' : ''; ?>>Enable</option>
            <option value="disable" <?php echo ($user_id != 0 && $dat->enable_status == 'disable') ? 'selected="selected"' : ''; ?>>Disable</option>
        </select>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php echo anchor('user/user/', 'Back', array('title' => 'User List', 'class' => 'btn btn-inverse')); ?>
    </div>
</div>
<?php echo form_close(); ?>
