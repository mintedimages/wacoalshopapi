<div class="container">
    <div class="row">
        <div class="span12">
            <ul class="breadcrumb">
                <li>
                    <?php echo  anchor('user/user/', 'User'); ?><span class="divider">/</span>
                </li>
                <li>
                    <?php echo  $title; ?><span class="divider">/</span>
                </li>
                <li class="active">
                    Change Password
                </li>
            </ul>
            <?php echo  anchor('user/user/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
            <h4 class="header">Change Password</h4>
            <?php echo  form_open_multipart('user/user/changepassword_post/' . $user_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
            <div class="control-group">
                <label class="control-label">User Group</label>
                <div class="controls">
                    <span class="input-xlarge uneditable-input"><?php echo  $dat->user_group_name; ?></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="user_login_name">Login Name</label>
                <div class="controls">
                    <span class="input-xlarge uneditable-input"><?php echo  $dat->user_login_name; ?></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="user_login_password">Password</label>
                <div class="controls">
                    <input class="input-xxlarge" type="password" name="user_login_password" id="user_login_password" />
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary">
                        Save Changes
                    </button>
                    <?php echo  anchor('user/user/', 'Back', array('title' => 'User List', 'class' => 'btn btn-inverse')); ?>
                </div>
            </div>
            <?php echo  form_close(); ?>
        </div>
    </div>
</div>