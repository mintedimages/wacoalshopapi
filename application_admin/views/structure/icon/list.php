<?php echo anchor('structure/icon/form/', '<i class="icon icon-plus"></i> New Icon', array('title' => 'New Icon', 'class' => 'btn pull-right')); ?>
<h4 class="header">Icon</h4>
<?php if ($query->num_rows() > 0) { ?>
    <table class="table table-striped sortable">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Icon</th>
                <th>Image</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() AS $row) {
                ?>
                <tr>
                    <td class="hidden-phone"><?php echo $row->icon_id; ?></td>
                    <td><?php echo anchor('structure/icon/form/' . $row->icon_id, ($row->icon_name != '') ? $row->icon_name : '-', array('title' => 'Icon Detail')); ?></td>
                    <td><?php echo anchor('structure/icon/form/' . $row->icon_id, ($row->icon_image != '') ? '<span style="background-color: #454545;display:inline-block;"><img src="' . $row->icon_image . '" style="max-width:200px;" /></span>' : '-', array('title' => 'Icon Detail')); ?></td>
                    <td>
                        <div class="btn-group">
                            <?php echo anchor('structure/icon/form/' . $row->icon_id, 'Edit', array('title' => 'Icon Detail', 'class' => 'btn')); ?>
                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php echo anchor('structure/icon/moveup/' . $row->icon_id, 'Move Up', array('title' => 'Move Up')); ?>
                                    <?php echo anchor('structure/icon/movedown/' . $row->icon_id, 'Move Down', array('title' => 'Move Down')); ?>
                                    <?php echo anchor('structure/icon/delete/' . $row->icon_id, 'Delete', array('title' => 'Delete')); ?>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>