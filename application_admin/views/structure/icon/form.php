<ul class="breadcrumb">
    <li>
        <?php echo anchor('structure/icon/', 'Icon'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo $title; ?>
    </li>
</ul>
<div class="clearfix">
    <?php echo anchor('structure/icon/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
    <h4 class="header"><?php echo $title; ?></h4>
</div>
<?php echo form_open_multipart('structure/icon/form_post/' . $icon_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="icon_name">Icon Name</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="icon_name" id="icon_name" <?php echo ($icon_id != 0) ? 'value="' . $dat->icon_name . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="icon_image">Image</label>
    <div class="controls">
        <?php
        $data = '';
        $value = ($icon_id != 0) ? $dat->icon_image : '';
        if ($value != "") {
            if (@getimagesize($_SERVER['DOCUMENT_ROOT'] . $value)) {
                $info = @getimagesize($_SERVER['DOCUMENT_ROOT'] . $value);
                if ($info[0] > 250) {
                    $data .= '<img src="' . $value . '" alt="' . $value . '" width="250"/><br/>';
                } else {
                    $data .= '<img src="' . $value . '" alt="' . $value . '"/><br/>';
                }
            } else {
                $data .= '<img src="' . $value . '" alt="' . $value . '"/><br/>';
            }
        }
        echo $data;
        ?>
        <input type="upload" class="input-xxlarge" id="icon_image" name="icon_image" value="<?php echo $value; ?>" /> 
        <button type="button" class="btn btn-primary" onclick="BrowseServerIconImage();">Browse Server</button>
        <script type="text/javascript">
            function BrowseServerIconImage() {
                var finder = new CKFinder();
                finder.selectActionFunction = SetFileFieldIconImage;
                finder.popup();
            }
            function SetFileFieldIconImage(fileUrl) {
                document.getElementById('icon_image').value = fileUrl;
            }
        </script>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="sort_priority">Priority</label>
    <div class="controls">
        <select name="sort_priority" id="sort_priority">
            <?php for ($i = 1; $i <= $max_priority; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php echo ($icon_id == 0 || $dat->sort_priority != $i) ? '' : 'selected="selected"'; ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php echo anchor('structure/icon/', 'Back', array('title' => 'Icon List', 'class' => 'btn btn-inverse')); ?>
    </div>
</div>
<?php echo form_close(); ?>