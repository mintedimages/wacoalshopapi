<div class="container">
    <div class="row">
        <div class="span3">
            <h4>Structure</h4>
            <div class="sidebar">
                <ul class="col-nav span3">
                    <li><?php echo anchor('structure/content/', 'Structure', array('title' => 'Manage Structure')); ?></li>
                    <li><?php echo anchor('structure/icon/', 'Icon', array('title' => 'Manage Icon')); ?></li>
                    <li><?php echo anchor('structure/shop/', 'Shop', array('title' => 'Manage Shop')); ?></li>
                    <li><?php echo anchor('structure/lang/', 'Language', array('title' => 'Manage Language')); ?></li>
                    <li><?php echo anchor('structure/config_group/', 'Config', array('title' => 'Manage Config')); ?></li>
                    <li><?php echo anchor('welcome/updateTable/', 'Migration', array('title' => 'Update Version Backend')); ?></li>
                </ul>
            </div>
        </div>
        <div class="span9">
            <?php echo $content; ?>
        </div>
    </div>
</div>