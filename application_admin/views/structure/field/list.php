<ul class="breadcrumb">
    <li>
        <?php echo  anchor('structure/content/', 'Content'); ?><span class="divider">/</span>
    </li>
    <li>
        <?php echo  anchor('structure/content/form/' . $table_id, $this->StructureModel->getTableName($table_id)); ?><span class="divider">/</span>
    </li>
    <li class="active">
        Field
    </li>
</ul>
<div class="clearfix">
    <?php echo  anchor('structure/field/form/0/' . $table_id, '<i class="icon icon-plus"></i> New Field', array('title' => 'New Field', 'class' => 'btn pull-right')); ?>
    <h4 class="header">Field</h4>
</div>
<?php if ($query->num_rows() > 0) { ?>
    <table class="table table-striped sortable">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Name</th>
                <th style="width:120px">Code</th>
                <th style="width:100px">Type</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() AS $row) {
                ?>
                <tr>
                    <td class="hidden-phone"><?php echo  $row->column_id; ?></td>
                    <td><?php echo  anchor('structure/field/form/' . $row->column_id . '/' . $row->table_id, ($row->column_name != '') ? $row->column_name : '-', array('title' => 'Field Detail')); ?></td>
                    <td><?php echo  anchor('structure/field/form/' . $row->column_id . '/' . $row->table_id, ($row->column_code != '') ? $row->column_code : '-', array('title' => 'Field Detail')); ?></td>
                    <td><?php echo  anchor('structure/field/form/' . $row->column_id . '/' . $row->table_id, ($row->column_field_type != '') ? $row->column_field_type : '-', array('title' => 'Field Detail')); ?></td>
                    <td>
                        <div class="btn-group">
                            <?php echo  anchor('structure/field/form/' . $row->column_id . '/' . $row->table_id, 'Edit', array('title' => 'Field Detail', 'class' => 'btn')); ?>
                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php echo  anchor('structure/field/moveup/' . $row->column_id . '/' . $row->table_id, 'Move Up', array('title' => 'Move Up')); ?>
                                    <?php echo  anchor('structure/field/movedown/' . $row->column_id . '/' . $row->table_id, 'Move Down', array('title' => 'Move Down')); ?>
                                    <?php echo  anchor('structure/field/delete/' . $row->column_id . '/' . $row->table_id, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <script type="text/javascript">
        $(function(){
            $('.delete').click(function(){
                return (confirm('Do you want to delete this field ?') == true) ? true : false;
            });
        });
    </script>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>