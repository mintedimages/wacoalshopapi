<ul class="breadcrumb">
    <li>
        <?php echo anchor('structure/content/', 'Content'); ?><span class="divider">/</span>
    </li>
    <li>
        <?php echo anchor('structure/content/form/' . $table_id, $this->StructureModel->getTableName($table_id)); ?><span class="divider">/</span>
    </li>
    <li>
        <?php echo anchor('structure/field/index/' . $table_id, 'Field'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo $title; ?>
    </li>
</ul>
<div class="clearfix">
    <?php echo anchor('structure/field/index/' . $table_id, 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
    <h4 class="header"><?php echo $title; ?></h4>
</div>
<?php echo form_open_multipart('structure/field/form_post/' . $column_id . '/' . $table_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="column_name">Name</label>
    <div class="controls">
        <input class="input-xxlarge required" type="text" name="column_name" id="column_name" <?php echo ($column_id != 0) ? 'value="' . $dat->column_name . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="column_code">Code</label>
    <div class="controls">
        <input class="input-xxlarge required" type="text" name="column_code" id="column_code" <?php echo ($column_id != 0) ? 'value="' . $dat->column_code . '"' : ''; ?> />
    </div>
</div>
<div class="control-group <?php echo ( ($max_priority == 1) || ($column_id != 0 && $dat->column_main == 'true') ) ? 'hide' : ''; ?>">
    <label class="control-label" for="column_main">Main Content</label>
    <div class="controls">
        <input type="checkbox" name="column_main" id="column_main" <?php echo (($max_priority == 1) || (($column_id != 0) && ($dat->column_main == 'true'))) ? 'checked' : ''; ?> value="true"/>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="column_field_type">Type</label>
    <div class="controls">
        <select name="column_field_type" id="column_field_type" class="input-large">
            <?php
            $fieldType = $this->FieldTypeModel->getFieldType('fieldTypeModel');
            for ($i = 0; $i < count($fieldType); $i++) {
                ?>
                <option value="<?php echo $fieldType[$i]['field_type_code']; ?>" <?php echo ($column_id != 0 && $dat->column_field_type == $fieldType[$i]['field_type_code']) ? 'selected="selected"' : ''; ?>><?php echo $fieldType[$i]['field_type_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group hide" id="relation_table">
    <label class="control-label" for="column_relation_table">Relation Table</label>
    <div class="controls">
        <select name="column_relation_table" id="column_relation_table" class="input-large">
            <option value="0" <?php echo ($column_id != 0 && $dat->column_relation_table == 0) ? 'selected="selected"' : ''; ?>>No Select</option>
            <?php
            $loadRelateTable = $this->TableModel->loadRelateTable();
            foreach ($loadRelateTable->result_array() as $row) {
                ?>
                <option value="<?php echo $row['table_id']; ?>" <?php echo ($column_id != 0 && $dat->column_relation_table == $row['table_id']) ? 'selected="selected"' : ''; ?>><?php echo $row['table_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group" id="language_group">
    <label class="control-label" for="column_lang">Language</label>
    <div class="controls">
        <select name="column_lang" id="column_lang" class="input-large">
            <option value="0" <?php echo ($column_id != 0 && $dat->column_lang == 0) ? 'selected="selected"' : ''; ?>>No</option>
            <option value="1" <?php echo ($column_id != 0 && $dat->column_lang == 1) ? 'selected="selected"' : ''; ?>>Yes</option>
        </select>
    </div>
</div>
<div class="control-group <?php echo ( ($max_priority == 1) || ($column_id != 0 && $dat->column_main == 'true') ) ? 'hide' : ''; ?>">
    <label class="control-label" for="column_show_list">Show List Page</label>
    <div class="controls">
        <select name="column_show_list" id="column_show_list" class="input-large">
            <option value="0" <?php echo ($column_id != 0 && $dat->column_show_list == 0) ? 'selected="selected"' : ''; ?>>No</option>
            <option value="1" <?php echo (($max_priority == 1) || ($column_id != 0 && $dat->column_show_list == 1)) ? 'selected="selected"' : ''; ?>>Yes</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="searchable">Search</label>
    <div class="controls">
        <select name="searchable" id="searchable" class="input-large">
            <option value="Disable" <?php echo ($column_id != 0 && $dat->searchable == 'Disable') ? 'selected="selected"' : ''; ?>>Disable</option>
            <option value="Normal Search" <?php echo ($column_id != 0 && $dat->searchable == 'Normal Search') ? 'selected="selected"' : ''; ?>>Normal Search</option>
            <option value="Advance Search" <?php echo ($column_id != 0 && $dat->searchable == 'Advance Search') ? 'selected="selected"' : ''; ?>>Advance Search</option>
        </select>
    </div>
</div>
<input class="input-xxlarge" type="hidden" name="column_option" id="column_option" <?php echo ($column_id != 0) ? 'value="' . $dat->column_option . '"' : ''; ?> />
<div class="control-group displayImageCrop">
    <label class="control-label" for="imagecrop_width">Image Width</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="imagecrop_width" id="imagecrop_width" value=""/>
    </div>
</div>
<div class="control-group displayImageCrop">
    <label class="control-label" for="imagecrop_height">Image Height</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="imagecrop_height" id="imagecrop_height" value=""/>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="column_remark">Remark</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="column_remark" id="column_remark" <?php echo ($column_id != 0) ? 'value="' . $dat->column_remark . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="sort_priority">Priority</label>
    <div class="controls">
        <select name="sort_priority" id="sort_priority">
            <?php for ($i = 1; $i <= $max_priority; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php echo ($column_id == 0 || $dat->sort_priority != $i) ? '' : 'selected="selected"'; ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php echo anchor('structure/field/index/' . $table_id, 'Back', array('title' => 'Field List', 'class' => 'btn btn-inverse')); ?>
    </div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
    var $language_type = ['text', 'textarea', 'richtext', 'image', 'imagecrop', 'file'];
    var $relatiion_type = ['dropdown', 'radio', 'checkbox'];
    $(function() {
        $columnOptionVal = {}
        if ($('#column_option').val() !== '') {
            eval("$columnOptionVal = " + $('#column_option').val() + ";");
        }
        $('#imagecrop_width').val($columnOptionVal['imagecropWidth']);
        $('#imagecrop_height').val($columnOptionVal['imagecropHeight']);
        $('#column_field_type option[selected="selected"]').each(function() {
            $('#language_group').hide();
            $('#relation_table').hide();
            $('.displayImageCrop').hide();
            if ($.inArray($(this).val(), $language_type) !== -1) {
                $('#language_group').show();
            }
            if ($.inArray($(this).val(), $relatiion_type) !== -1) {
                $('#relation_table').show();
            }
            if ($(this).val() === 'imagecrop') {
                $('.displayImageCrop').show();
            }
        });

        $('#column_main').on('change', function() {
            ($(this).is(':checked')) ? $('#column_show_list option[value="1"]').prop('selected', true) : $('#column_show_list option[value="0"]').prop('selected', true);
        });

        $('#imagecrop_width, #imagecrop_height').on('change', function() {
            var imageCropConfig = {
                imagecropWidth: $('#imagecrop_width').val(),
                imagecropHeight: $('#imagecrop_height').val()
            };
            $('#column_option').val(JSON.stringify(imageCropConfig).replace(/"/g, "'"));
        });

        $('#column_field_type').on('change', function() {
            ($.inArray($(this).val(), $language_type) !== -1) ? $('#language_group').slideDown() : $('#language_group').slideUp();
            ($.inArray($(this).val(), $relatiion_type) !== -1) ? $('#relation_table').slideDown() : $('#relation_table').slideUp();
            ($(this).val() === 'imagecrop') ? $('.displayImageCrop').slideDown() : $('.displayImageCrop').slideUp();
        });

        $('#form').submit(function() {
            $return_val = false;
            if ($('.required').val() === '') {
                $('.required').after('<div class="alert alert-error" style="margin-top: 6px; margin-bottom: 0px;"><button type="button" data-dismiss="alert" class="close">×</button><strong>Can\'t Add / Update ! </strong>Please enter in that field required.</div>');
                setTimeout(function() {
                    $('.alert').fadeOut('slow', function() {
                        $(this).remove();
                        $('.required').eq(0).focus();
                    });
                }, 2400);
            }
            else {
                $return_val = true;
            }
            return $return_val;
        });
    });

<?php
if ($status === 'success') {
    ?>
        (function() {
            toastr.options = {
                positionClass: 'toast-top-right'
            };
            toastr.success('Save data successfully.', 'Success');
        }).call(this);
    <?php
}
?>
</script>