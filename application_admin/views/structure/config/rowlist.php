<tr>
    <td class="hidden-phone"><?php echo $row->config_id; ?></td>
    <td><?php echo anchor('structure/config/form/' . $row->config_group_id . '/' . $row->config_id, ($row->config_name != '') ? $row->config_name : '-', array('title' => 'Config Detail')); ?></td>
    <td><?php echo anchor('structure/config/form/' . $row->config_group_id . '/' . $row->config_id, ($row->config_code != '') ? $row->config_code : '-', array('title' => 'Config Detail')); ?></td>
    <td><?php echo anchor('structure/config/form/' . $row->config_group_id . '/' . $row->config_id, ($row->config_type != '') ? $row->config_type : '-', array('title' => 'Config Detail')); ?></td>
    <td>
        <div class="btn-group">
            <?php echo anchor('structure/config/form/' . $row->config_group_id . '/' . $row->config_id, 'Edit', array('title' => 'Content Detail', 'class' => 'btn')); ?>
            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li>
                    <?php echo anchor('structure/config/moveup/' . $row->config_id . '/' . $row->config_group_id, 'Move Up', array('title' => 'Move Up')); ?>
                    <?php echo anchor('structure/config/movedown/' . $row->config_id . '/' . $row->config_group_id, 'Move Down', array('title' => 'Move Down')); ?>
                    <?php if ($row->config_type == 'content') { ?>
                        <?php echo anchor('structure/config/delete/' . $row->config_id . '/' . $row->config_group_id, 'Delete', array('title' => 'Delete')); ?>
                    <?php } ?>
                </li>
            </ul>
        </div>
    </td>
</tr>