<tr>
    <td class="hidden-phone"><?php echo $row->config_group_id; ?></td>
    <td><?php echo anchor('structure/config_group/form/' . $row->config_group_id, ($row->config_group_name != '') ? $row->config_group_name : '-', array('title' => 'Config Group Detail')); ?></td>
    <td><?php echo anchor('structure/config/index/' . $row->config_group_id, 'Set Config', array('title' => 'Set Config')); ?></td>
    <td>
        <div class="btn-group">
            <?php echo anchor('structure/config_group/form/' . $row->config_group_id, 'Edit', array('title' => 'Content Group Detail', 'class' => 'btn')); ?>
            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li>
                    <?php echo anchor('structure/config_group/moveup/' . $row->config_group_id, 'Move Up', array('title' => 'Move Up')); ?>
                    <?php echo anchor('structure/config_group/movedown/' . $row->config_group_id, 'Move Down', array('title' => 'Move Down')); ?>
                    <?php echo anchor('structure/config_group/delete/' . $row->config_group_id, 'Delete', array('title' => 'Delete')); ?>
                    <?php echo anchor('structure/config/index/' . $row->config_group_id, 'Set Config', array('title' => 'Set Config')); ?>
                </li>
            </ul>
        </div>
    </td>
</tr>