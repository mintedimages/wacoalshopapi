<?php echo anchor('structure/config/form/' . $config_group_id, '<i class="icon icon-plus"></i> Create Config', array('title' => 'Create Config', 'class' => 'btn pull-right')); ?>
<ul class="breadcrumb">
    <li>
        <?php echo anchor('structure/config_group/', 'Config Group'); ?><span class="divider">/</span>
    </li>
    <li>
        <?php echo anchor('structure/config_group/form/' . $config_group_id, $this->ConfigGroupModel->getConfigGroupName($config_group_id)); ?><span class="divider">/</span>
    </li>
    <li class="active">
        Config
    </li>
</ul>
<?php if ($total_content > 0) { ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Config Name</th>
                <th style="width:120px">Code</th>
                <th style="width:100px">Type</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $this->ConfigModel->getRowList($config_group_id);
            ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>