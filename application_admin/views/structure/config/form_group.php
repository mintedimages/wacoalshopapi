<ul class="breadcrumb">
    <li>
        <?php echo anchor('structure/config_group/', 'Config Group'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo $title; ?>
    </li>
</ul>
<div class="clearfix">
    <?php echo anchor('structure/config_group/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
    <h4 class="header">Content</h4>
</div>
<?php echo form_open_multipart('structure/config_group/form_post/' . $config_group_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="config_group_name">Name</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="config_group_name" id="config_group_name" <?php echo ($config_group_id != 0) ? 'value="' . $dat->config_group_name . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="sort_priority">Priority</label>
    <div class="controls">
        <select name="sort_priority" id="sort_priority">
            <?php for ($i = 1; $i <= $max_priority; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php echo ($config_group_id == 0 || $dat->sort_priority != $i) ? '' : 'selected="selected"'; ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php if ($config_group_id > 0) { ?>
            <?php echo anchor('structure/config/index/' . $config_group_id, 'Set Config', array('title' => 'Set Config', 'class' => 'btn btn-info')); ?>
        <?php } ?>
        <?php echo anchor('structure/config_group/', 'Back', array('title' => 'Config Group List', 'class' => 'btn btn-inverse')); ?>
    </div> 
</div>
<?php echo form_close(); ?>