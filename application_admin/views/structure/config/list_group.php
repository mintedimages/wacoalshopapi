<?php echo  anchor('structure/config_group/form/', '<i class="icon icon-plus"></i> Create Config Group', array('title' => 'Create Config Group', 'class' => 'btn pull-right')); ?>
<h4 class="header">Config Group</h4>
<?php if ($total_content > 0) { ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Config Group Name</th>
                <th style="width:100px;">Set Config</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $this->ConfigGroupModel->getRowList();
            ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>