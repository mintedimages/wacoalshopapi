<ul class="breadcrumb">
    <li>
        <?php echo anchor('structure/config_group/', 'Config Group'); ?><span class="divider">/</span>
    </li>
    <li>
        <?php echo anchor('structure/config_group/form/' . $config_group_id, $this->ConfigGroupModel->getConfigGroupName($config_group_id)); ?><span class="divider">/</span>
    </li>
    <li>
        <?php echo anchor('structure/config/index/' . $config_group_id, 'Config'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo $title; ?>
    </li>
</ul>
<div class="clearfix">
    <?php echo anchor('structure/config_group/' . $config_group_id, 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
    <h4 class="header">Content</h4>
</div>
<?php echo form_open_multipart('structure/config/form_post/' . $config_group_id . '/' . $config_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="config_name">Name</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="config_name" id="config_name" <?php echo ($config_id != 0) ? 'value="' . $dat->config_name . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="config_code">Code</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="config_code" id="config_code" <?php echo ($config_id != 0) ? 'value="' . $dat->config_code . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="config_field_type">Field Type</label>
    <div class="controls">
        <select name="config_field_type" id="config_field_type" class="input-large">
            <?php
            $fieldType = $this->FieldTypeModel->getFieldType('fieldTypeModel');
            for ($i = 0; $i < count($fieldType); $i++) {
                ?>
                <option value="<?php echo $fieldType[$i]['field_type_code']; ?>" <?php echo ($config_id != 0 && $dat->config_field_type == $fieldType[$i]['field_type_code']) ? 'selected="selected"' : ''; ?>><?php echo $fieldType[$i]['field_type_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="sort_priority">Priority</label>
    <div class="controls">
        <select name="sort_priority" id="sort_priority">
            <?php for ($i = 1; $i <= $max_priority; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php echo ($config_id == 0 || $dat->sort_priority != $i) ? '' : 'selected="selected"'; ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php echo anchor('structure/config/index/' . $config_group_id, 'Back', array('title' => 'Config List', 'class' => 'btn btn-inverse')); ?>
    </div>
</div>
<?php echo form_close(); ?>