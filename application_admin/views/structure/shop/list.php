<?php echo  anchor('structure/shop/form/', '<i class="icon icon-plus"></i> New Shop', array('title' => 'New Shop', 'class' => 'btn pull-right')); ?>
<h4 class="header">Shop</h4>
<?php if ($query->num_rows() > 0) { ?>
    <table class="table table-striped sortable">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Shop</th>
                <th>Slug</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() AS $row) {
                ?>
                <tr>
                    <td class="hidden-phone"><?php echo  $row->shop_id; ?></td>
                    <td><?php echo  anchor('structure/shop/form/' . $row->shop_id, ($row->shop_name != '') ? $row->shop_name : '-', array('title' => 'Shop Detail')); ?></td>
                    <td><?php echo  anchor('structure/shop/form/' . $row->shop_id, ($row->shop_code != '') ? $row->shop_code : '-', array('title' => 'Shop Detail')); ?></td>
                    <td>
                        <div class="btn-group">
                            <?php echo  anchor('structure/shop/form/' . $row->shop_id, 'Edit', array('title' => 'Shop Detail', 'class' => 'btn')); ?>
                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php echo  anchor('structure/shop/moveup/' . $row->shop_id, 'Move Up', array('title' => 'Move Up')); ?>
                                    <?php echo  anchor('structure/shop/movedown/' . $row->shop_id, 'Move Down', array('title' => 'Move Down')); ?>
                                    <?php echo  anchor('structure/shop/delete/' . $row->shop_id, 'Delete', array('title' => 'Delete')); ?>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>