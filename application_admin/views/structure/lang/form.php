<ul class="breadcrumb">
    <li>
        <?php echo  anchor('structure/lang/', 'Language'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo  $title; ?>
    </li>
</ul>
<div class="clearfix">
    <?php echo  anchor('structure/lang/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
    <h4 class="header"><?php echo  $title; ?></h4>
</div>
<?php echo  form_open_multipart('structure/lang/form_post/' . $lang_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="lang_name">Language</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="lang_name" id="lang_name" <?php echo  ($lang_id != 0) ? 'value="' . $dat->lang_name . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="lang_code">Slug</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="lang_code" id="lang_code" <?php echo  ($lang_id != 0) ? 'value="' . $dat->lang_code . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="sort_priority">Priority</label>
    <div class="controls">
        <select name="sort_priority" id="sort_priority">
            <?php for ($i = 1; $i <= $max_priority; $i++) { ?>
                <option value="<?php echo  $i; ?>" <?php echo  ($lang_id == 0 || $dat->sort_priority != $i) ? '' : 'selected="selected"'; ?>><?php echo  $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php echo  anchor('structure/lang/', 'Back', array('title' => 'Language List', 'class' => 'btn btn-inverse')); ?>
    </div>
</div>
<?php echo  form_close(); ?>