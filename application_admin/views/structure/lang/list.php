<?php echo  anchor('structure/lang/form/', '<i class="icon icon-plus"></i> New Language', array('title' => 'New Language', 'class' => 'btn pull-right')); ?>
<h4 class="header">Language</h4>
<?php if ($query->num_rows() > 0) { ?>
    <table class="table table-striped sortable">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Language</th>
                <th>Slug</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() AS $row) {
                ?>
                <tr>
                    <td class="hidden-phone"><?php echo  $row->lang_id; ?></td>
                    <td><?php echo  anchor('structure/lang/form/' . $row->lang_id, ($row->lang_name != '') ? $row->lang_name : '-', array('title' => 'Language Detail')); ?></td>
                    <td><?php echo  anchor('structure/lang/form/' . $row->lang_id, ($row->lang_code != '') ? $row->lang_code : '-', array('title' => 'Language Detail')); ?></td>
                    <td>
                        <div class="btn-group">
                            <?php echo  anchor('structure/lang/form/' . $row->lang_id, 'Edit', array('title' => 'Language Detail', 'class' => 'btn')); ?>
                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php echo  anchor('structure/lang/moveup/' . $row->lang_id, 'Move Up', array('title' => 'Move Up')); ?>
                                    <?php echo  anchor('structure/lang/movedown/' . $row->lang_id, 'Move Down', array('title' => 'Move Down')); ?>
                                    <?php echo  anchor('structure/lang/delete/' . $row->lang_id, 'Delete', array('title' => 'Delete')); ?>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>