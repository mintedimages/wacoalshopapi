<div class="btn-group pull-right">
    <?php echo anchor('structure/content/form/', '<i class="icon icon-plus"></i> Create Content', array('title' => 'Create Root Content', 'class' => 'btn')); ?>
    <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li>
            <?php echo anchor('structure/content/exportstructure/', '<i class="icon icon-download"></i> Export Structure', array('title' => 'Create Root Content', 'class' => '')); ?>
            <a href="#importModal" role="button" class="" data-toggle="modal"><i class="icon icon-upload"></i> Import Structure</a>
        </li>
    </ul>
</div>
<h4 class="header">Content</h4>
<?php if ($total_content > 0) { ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th style="width:40px;" class="hidden-phone">#</th>
                <th>Content Name</th>
                <th style="width:120px;" class="hidden-phone">Parent</th>
                <th style="width:120px">Code</th>
                <th style="width:100px">Type</th>
                <th style="width:80px">Fields</th>
                <th style="width:100px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $this->StructureModel->getRowList(0);
            ?>
        </tbody>
    </table>
    <script type="text/javascript">
        $(function() {
            $('.delete').click(function() {
                return (confirm('Do you want to delete this content ?') === true) ? true : false;
            });
        });
    </script>
<?php } else { ?>
    <div class="alert"><i class="icon icon-warning-sign"></i> ไม่มีข้อมูล</div>
<?php } ?>
<div id="importModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <?php echo form_open_multipart('structure/content/importstructure', array('style' => 'margin:0;')); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="importModalLabel">Import Structure</h3>
    </div>
    <div class="modal-body">
        <input type="file" name="file_structure" id="file_structure" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="submit" class="btn btn-primary">Import Structure</button>
    </div>
    <?php echo form_close(); ?>
</div>
