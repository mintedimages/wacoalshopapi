<ul class="breadcrumb">
    <li>
        <?php echo anchor('structure/content/', 'Content'); ?><span class="divider">/</span>
    </li>
    <li class="active">
        <?php echo $title; ?>
    </li>
</ul>
<?php echo anchor('structure/content/', 'Back', array('title' => 'Back', 'class' => 'btn pull-right')); ?>
<h4 class="header">Content</h4>
<?php echo form_open_multipart('structure/content/form_post/' . $table_id, array('id' => 'form', 'class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label">Parent</label>
    <div class="controls">
        <input type="hidden" name="parent_table_id" id="parent_table_id" value="<?php echo $parent_table_id; ?>" />
        <select id="show_parent_table_id" class="input-large">
            <option value="0" <?php echo ($parent_table_id == 0) ? 'selected="selected"' : ''; ?> data-marpriority="<?php echo $max_priority; ?>">root</option>
            <?php $this->StructureModel->getDropdownList(0, 0, $table_id); ?>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="table_name">Name</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="table_name" id="table_name" <?php echo ($table_id != 0) ? 'value="' . $dat->table_name . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="table_code">Code</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="table_code" id="table_code" <?php echo ($table_id != 0) ? 'value="' . $dat->table_code . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="table_type">Type</label>
    <div class="controls">
        <select name="table_type" id="table_type" class="input-large">
            <option value="static" <?php echo ($table_id != 0 && $dat->table_type == 'static') ? 'selected="selected"' : ''; ?>>Static</option>
            <option value="dynamic" <?php echo ($table_id != 0 && $dat->table_type == 'dynamic') ? 'selected="selected"' : ''; ?>>Dynamic</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="table_order">Order</label>
    <div class="controls">
        <select name="table_order" id="table_order" class="input-large">
            <option value="asc" <?php echo ($table_id != 0 && $dat->table_order == 'asc') ? 'selected="selected"' : ''; ?>>Ascending</option>
            <option value="desc" <?php echo ($table_id != 0 && $dat->table_order == 'desc') ? 'selected="selected"' : ''; ?>>Descending</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="table_newcontent">New Content</label>
    <div class="controls">
        <select name="table_newcontent" id="table_newcontent" class="input-large">
            <option value="true" <?php echo ($table_id != 0 && $dat->table_newcontent == 'true') ? 'selected="selected"' : ''; ?>>True</option>
            <option value="false" <?php echo ($table_id != 0 && $dat->table_newcontent == 'false') ? 'selected="selected"' : ''; ?>>False</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="table_recursive">Recursive</label>
    <div class="controls">
        <select name="table_recursive" id="table_recursive" class="input-large">
            <option value="false" <?php echo ($table_id != 0 && $dat->table_recursive == 'false') ? 'selected="selected"' : ''; ?>>False</option>
            <option value="true" <?php echo ($table_id != 0 && $dat->table_recursive == 'true') ? 'selected="selected"' : ''; ?>>True</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="table_preview">Preview URL</label>
    <div class="controls">
        <input class="input-xxlarge" type="text" name="table_preview" id="table_preview" <?php echo ($table_id != 0) ? 'value="' . $dat->table_preview . '"' : ''; ?> />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="icon_id">Icon</label>
    <div class="controls">
        <select name="icon_id" id="icon_id">
            <?php foreach ($queryIcon->result() AS $icon) { ?>
                <option value="<?php echo $icon->icon_id; ?>" <?php echo ($table_id == 0 || $dat->icon_id != $icon->icon_id) ? '' : 'selected="selected"'; ?>><?php echo $icon->icon_name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="sort_priority">Priority</label>
    <div class="controls">
        <select name="sort_priority" id="sort_priority">
            <?php for ($i = 1; $i <= $max_priority; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php echo ($table_id == 0 || $dat->sort_priority != $i) ? '' : 'selected="selected"'; ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn btn-primary">
            Save Changes
        </button>
        <?php if ($table_id > 0) { ?>
            <?php echo anchor('structure/field/index/' . $table_id, 'Manage Field', array('title' => 'Field List', 'class' => 'btn btn-info')); ?>
        <?php } ?>
        <?php echo anchor('structure/content/', 'Back', array('title' => 'Content List', 'class' => 'btn btn-inverse')); ?>
    </div>
</div>
<?php echo form_close(); ?>
<?php
if ($status == 'success') {
    ?>
    <script type="text/javascript">
        (function() {
            toastr.options = {
                positionClass: 'toast-top-right'
            };

            toastr.success('Save data successfully.', 'Success');
        }).call(this);
    </script>
    <?php
} else if ($status == 'error') {
    ?>
    <script type="text/javascript">
        (function() {
            toastr.options = {
                positionClass: 'toast-top-right'
            };

            toastr.error('Please contact to your administrator to fix this problem.', 'Error !!!');
        }).call(this);
    </script>
    <?php
}
?>
<script type="text/javascript">
    $(function(){
        $('#show_parent_table_id').on('change', function(){
            var new_val = $(this).val();
            $('#parent_table_id').val(new_val);
            
            $.get('<?php echo base_url() . 'admin.php/structure/content/getMaxpriority/' . $table_id . '/'; ?>' + new_val, function( data ){
                var select = $('#sort_priority');
                if(select.prop) {
                    var options = select.prop('options');
                }
                else {
                    var options = select.attr('options');
                }
                $('option', select).remove();
                
                for(var i=1; i<=data; i++){
                    options[options.length] = new Option(i, i);
                }
            });
        });
    });
</script>