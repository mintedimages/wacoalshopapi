{
"table_id": <?php echo $data->table_id; ?>,
"parent_table_id": <?php echo $data->parent_table_id; ?>,
"table_name": "<?php echo $data->table_name; ?>",
"table_code": "<?php echo $data->table_code; ?>",
"table_type": "<?php echo $data->table_type; ?>",
"icon_id": <?php echo $data->icon_id; ?>,
"table_order": "<?php echo $data->table_order; ?>",
"table_newcontent": "<?php echo $data->table_newcontent; ?>",
"table_preview": "<?php echo $data->table_preview; ?>",
"table_recursive": "<?php echo $data->table_recursive; ?>",
"mother_column": [
<?php
$start_row_column = 0;
foreach ($query_column->result() as $row_column) {
    if ($start_row_column > 0) {
        echo ",";
    }
    $start_row_column++;
    ?>
    {
    "table_id": <?php echo $row_column->table_id; ?>,
    "column_name": "<?php echo $row_column->column_name; ?>",
    "column_code": "<?php echo $row_column->column_code; ?>",
    "column_main": "<?php echo $row_column->column_main; ?>",
    "column_field_type": "<?php echo $row_column->column_field_type; ?>",
    "column_lang": <?php echo $row_column->column_lang; ?>,
    "column_show_list": <?php echo $row_column->column_show_list; ?>,
    "column_option": "<?php echo $row_column->column_option; ?>",
    "column_relation_table": <?php echo $row_column->column_relation_table; ?>,
    "sort_priority": <?php echo $row_column->sort_priority; ?>,
    "searchable": "<?php echo $row_column->searchable; ?>",
    "column_remark": "<?php echo $row_column->column_remark; ?>"
    }
<?php } ?>
],
"child_table":[
<?php
$start_row_table = 0;
foreach ($query_child->result() as $row) {
    if ($start_row_table > 0) {
        echo ",";
    }
    $start_row_table++;
    $this->ExportStructureModel->printTable($row->table_id);
}
?>
]
}