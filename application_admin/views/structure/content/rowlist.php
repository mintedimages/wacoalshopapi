<tr>
    <td class="hidden-phone"><?php echo $row->table_id; ?></td>
    <td><?php for ($i = 0; $i < $deep; $i++) { ?>
        &nbsp;&nbsp;&nbsp;
        <?php } ?>
        <?php echo anchor('structure/content/form/' . $row->table_id, ($row->table_name != '') ? $row->table_name : '-', array('title' => 'Content Detail')); ?></td>
    <td><?php echo anchor('structure/content/form/' . $row->table_id, ($row->parent_table_id != 0) ? $this->StructureModel->getTableName($row->parent_table_id) : 'Root', array('title' => 'Content Detail')); ?></td>
    <td><?php echo anchor('structure/content/form/' . $row->table_id, ($row->table_code != '') ? $row->table_code : '-', array('title' => 'Content Detail')); ?></td>
    <td><?php echo anchor('structure/content/form/' . $row->table_id, ($row->table_type != '') ? $row->table_type : '-', array('title' => 'Content Detail')); ?></td>
    <td><?php echo anchor('structure/field/index/' . $row->table_id, $row->field_count, array('title' => 'Content Detail')); ?></td>
    <td>
        <div class="btn-group">
            <?php echo anchor('structure/content/form/' . $row->table_id, 'Edit', array('title' => 'Content Detail', 'class' => 'btn')); ?>
            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li>
                    <?php echo anchor('structure/content/form/0/' . $row->table_id, 'Create Sub Content', array('title' => 'Create Sub Content')); ?>
                    <?php echo anchor('structure/content/exportstructure/' . $row->table_id, 'Export Structure', array('title' => 'Export Structure')); ?>
                    <?php echo anchor('structure/content/moveup/' . $row->table_id, 'Move Up', array('title' => 'Move Up')); ?>
                    <?php echo anchor('structure/content/movedown/' . $row->table_id, 'Move Down', array('title' => 'Move Down')); ?>
                    <?php echo anchor('structure/content/delete/' . $row->table_id, 'Delete', array('title' => 'Delete', 'class' => 'delete')); ?>
                </li>
            </ul>
        </div>
    </td>
</tr>