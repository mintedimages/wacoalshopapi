<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        $this->load->model(array('BackendModel', 'FieldTypeModel', 'StructureModel', 'LangModel'));
    }

    public function index() {
        $data = array();
        $this->db->where('parent_table_id', 0);
        $this->db->order_by('sort_priority', 'asc');
        $query = $this->db->get('mother_table');
        $dataContent['query'] = $query;
        $data['content'] = $this->load->view('dashboard', $dataContent, true);
        $data['sel_menu'] = 'dashboard';
        $this->load->view('masterpage', $data);
    }

}