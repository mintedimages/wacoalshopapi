<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class UserGroup extends CI_Controller {

    var $sel_menu = 'user';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('g', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('UserGroupModel', 'StructureModel', 'PermissionModel', 'ShopModel'));
    }

    public function index() {
        $this->db->select('mother_user_group.*');
        $this->db->select('COUNT(mother_user.user_id) AS count_user');
        $this->db->join('mother_user', 'mother_user.user_group_id = mother_user_group.user_group_id', 'left');
        $this->db->group_by('mother_user_group.user_group_id');
        $query = $this->db->get('mother_user_group');
        $total_content = $query->num_rows();
        $dataContent['total_content'] = $total_content;
        $dataContent['query'] = $query;
        $dataNavigate['content'] = $this->load->view('usergroup/list', $dataContent, true);
        $data['content'] = $this->load->view('user/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($user_group_id = 0, $status = '') {
        $dataContent['user_group_id'] = $user_group_id;
        $dataContent['permission'] = '';
        if ($user_group_id <= 0) {
            $dataContent['title'] = 'Create User Group';
        } else {
            $this->db->select('mother_user_group.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_user_group.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_user_group.update_by', 'left');
            $this->db->where('mother_user_group.user_group_id', $user_group_id);
            $query = $this->db->get('mother_user_group');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->user_group_name;

                //get permission from permission table
                $dataContent['permission'] = $this->PermissionModel->getPermissionToString($user_group_id);
            }
        }
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('usergroup/form', $dataContent, true);
        $data['content'] = $this->load->view('user/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($user_group_id) {
        $permission = $_POST['permission'];
        $userGroup = array(
            'user_group_name' => $_POST['user_group_name']
                /* ,'user_group_permission' => $permission */
        );
        $this->UserGroupModel->deletePermission($user_group_id);
        if ($user_group_id <= 0) {
            $user_group_id = $this->UserGroupModel->insert($userGroup);
        } else {
            $this->UserGroupModel->update($user_group_id, $userGroup);
        }
        $shop_id = 0;
        $query_mother_table = $this->db->get('mother_table');
        $query_shop = $this->ShopModel->queryShopList();
        foreach ($query_shop->result_array() as $rowShop) {
            $shop_id = $rowShop['shop_id'];
            if (isset($_POST['permission_' . $shop_id])) {
                $permission_shop = $_POST['permission_' . $shop_id];
                foreach ($query_mother_table->result_array() as $row) {
                    $table_id = $row['table_id'];
                    $view = 'false';
                    $edit = 'false';
                    //print_r($permission_shop);
                    foreach ($permission_shop as $key => $value) {
                        if ($table_id == $value) {
                            $view = 'true';
                        }
                        if ($table_id == -$value) {
                            $edit = 'true';
                        }
                    }
                    $this->UserGroupModel->insertPermission($user_group_id, $shop_id, $table_id, $view, $edit);
                }
            }
        }
        $otherPermissionArray = array('s', 'u', 'g', 'c', 'l');
        foreach ($otherPermissionArray as $k => $v) {
            $view = 'false';
            $edit = 'false';
            foreach ($permission as $key => $value) {
                if ($v == $value) {
                    $view = 'true';
                    $edit = 'true';
                }
            }
            $this->UserGroupModel->insertPermission($user_group_id, 0, $v, $view, $edit);
        }
        redirect('user/usergroup/form/' . $user_group_id . '/success');
    }

    public function delete($user_group_id) {
        $this->UserGroupModel->delete($user_group_id);
        redirect('user/usergroup/index/');
    }

}