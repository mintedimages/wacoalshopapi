<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    var $sel_menu = 'user';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('u', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('UserModel', 'StructureModel'));
    }

    public function index() {
        $this->db->join('mother_user_group', 'mother_user_group.user_group_id = mother_user.user_group_id', 'left');
        if ($this->session->userdata('user_group_id') != 0) {
            $this->db->where('mother_user.user_group_id !=', 0);
        }
        $query = $this->db->get('mother_user');
        $total_content = $query->num_rows();
        $dataContent['total_content'] = $total_content;
        $dataContent['query'] = $query;
        $dataNavigate['content'] = $this->load->view('user/list', $dataContent, true);
        $data['content'] = $this->load->view('user/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($user_id = 0, $status = '') {
        $dataContent['user_id'] = $user_id;
        if ($user_id <= 0) {
            $dataContent['title'] = 'Create User';
        } else {
            $this->db->select('mother_user.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_user.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_user.update_by', 'left');
            $this->db->where('mother_user.user_id', $user_id);
            $query = $this->db->get('mother_user');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->user_login_name;
            }
        }
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('user/form', $dataContent, true);
        $data['content'] = $this->load->view('user/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($user_id) {
        $user = array(
            'user_group_id' => $_POST['user_group_id'],
            'user_login_name' => $_POST['user_login_name'],
            'enable_status' => $_POST['enable_status']
        );
        if ($user_id <= 0) {
            $user['user_login_password'] = md5($_POST['user_login_password']);
            $user_id = $this->UserModel->insert($user);
        } else {
            $this->UserModel->update($user_id, $user);
        }
        redirect('user/user/form/' . $user_id . '/success');
    }

    public function changepassword($user_id, $status = '') {
        $dataContent['user_id'] = $user_id;
        $this->db->select('mother_user.*');
        $this->db->select('mother_user_group.user_group_name');
        $this->db->join('mother_user_group', 'mother_user_group.user_group_id = mother_user.user_group_id', 'left');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_user.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_user.update_by', 'left');
        $this->db->where('mother_user.user_id', $user_id);
        $query = $this->db->get('mother_user');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $dataContent['dat'] = $dat;
            $dataContent['title'] = $dat->user_login_name;
        }
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('user/changepassword', $dataContent, true);
        $data['content'] = $this->load->view('user/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function changepassword_post($user_id) {
        $user = array(
            'user_login_password' => md5($_POST['user_login_password'])
        );
        $this->UserModel->update($user_id, $user);
        redirect('user/user/form/' . $user_id . '/success');
    }

    public function delete($user_id) {
        $this->UserModel->delete($user_id);
        redirect('user/user/index/');
    }

}