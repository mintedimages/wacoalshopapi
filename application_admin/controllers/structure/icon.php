<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Icon extends CI_Controller {

    var $sel_menu = 'structure';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('s',  0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('IconModel', 'StructureModel'));
    }

    public function index() {
        $this->db->select('icon_id');
        $query = $this->db->get('mother_icon');
        $total_content = $query->num_rows();
        $this->db->select('mother_icon.*');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = mother_icon.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = mother_icon.update_by', 'left');
        $this->db->group_by('mother_icon.icon_id');
        $this->db->order_by('mother_icon.sort_priority', 'asc');
        $dataContent['query'] = $this->db->get('mother_icon');
        $this->db->flush_cache();
        $dataContent['total_content'] = $total_content;
        $dataNavigate['content'] = $this->load->view('structure/icon/list', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($icon_id = 0, $status = '') {
        $dataContent['icon_id'] = $icon_id;
        $max_priority = $this->IconModel->getMaxPriority();
        if ($icon_id <= 0) {
            $dataContent['title'] = 'Create Icon';
            $max_priority++;
        } else {
            $this->db->select('mother_icon.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_icon.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_icon.update_by', 'left');
            $this->db->where('mother_icon.icon_id', $icon_id);
            $query = $this->db->get('mother_icon');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->icon_name;
            }
        }
        $dataContent['max_priority'] = $max_priority;
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('structure/icon/form', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($icon_id = 0) {
        $icon = array(
            'icon_name' => $_POST['icon_name'],
            'icon_image' => $_POST['icon_image'],
            'sort_priority' => $_POST['sort_priority']
        );
        if ($icon_id <= 0) {
            $icon_id = $this->IconModel->insert($icon);
        } else {
            $this->IconModel->update($icon_id, $icon);
        }
        redirect('structure/icon/form/' . $icon_id . '/success');
    }

    public function moveup($icon_id) {
        $this->IconModel->moveUp($icon_id);
        redirect('structure/icon/index/');
    }

    public function movedown($icon_id) {
        $this->IconModel->moveDown($icon_id);
        redirect('structure/icon/index/');
    }

    public function delete($icon_id) {
        $this->IconModel->delete($icon_id);
        redirect('structure/icon/index/');
    }

}