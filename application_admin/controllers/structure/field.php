<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Field extends CI_Controller {

    var $sel_menu = 'structure';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('s', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('StructureModel', 'FieldTypeModel'));
    }

    public function index($table_id) {
        $dataContent['table_id'] = $table_id;
        $this->db->select('mother_column.column_id');
        $this->db->where('mother_column.table_id', $table_id);
        $query = $this->db->get('mother_column');
        $total_content = $query->num_rows();
        $this->db->select('mother_column.*');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = mother_column.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = mother_column.update_by', 'left');
        $this->db->where('mother_column.table_id', $table_id);
        $this->db->order_by('mother_column.sort_priority', 'asc');
        $dataContent['query'] = $this->db->get('mother_column');
        $this->db->flush_cache();
        $dataContent['total_content'] = $total_content;
        $dataNavigate['content'] = $this->load->view('structure/field/list', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($column_id = 0, $table_id = 0, $status = '') {
        $dataContent['table_id'] = $table_id;
        $dataContent['column_id'] = $column_id;
        $max_priority = $this->StructureModel->getMaxColumnPriority($table_id);
        if ($max_priority <= 0) {
            $max_priority = 0;
        }
        if ($column_id <= 0) {
            $max_priority++;
            $dataContent['title'] = 'Create Field';
        } else {
            $this->db->select('mother_column.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_column.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_column.update_by', 'left');
            $this->db->where('mother_column.column_id', $column_id);
            $query = $this->db->get('mother_column');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->column_name;
            }
        }
        $dataContent['max_priority'] = $max_priority;
        $dataContent['status'] = $status;
        $dataNavigate['content'] = $this->load->view('structure/field/form', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['status'] = $status;
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($column_id = 0, $table_id = 0) {
        $column_main = 'false';
        if (isset($_POST['column_main'])) {
            $column_main = $_POST['column_main'];
            if ($column_main == 'true') {
                $this->StructureModel->resetMainColumn($table_id);
            }
        }
        $column = array(
            'column_name' => $_POST['column_name'],
            'column_code' => $_POST['column_code'],
            'column_main' => $column_main,
            'column_field_type' => $_POST['column_field_type'],
            'column_relation_table' => $_POST['column_relation_table'],
            'column_lang' => $_POST['column_lang'],
            'column_show_list' => $_POST['column_show_list'],
            'searchable' => $_POST['searchable'],
            'column_option' => $_POST['column_option'],
            'column_remark' => $_POST['column_remark'],
            'sort_priority' => $_POST['sort_priority']
        );
        if ($column_id <= 0) {
            $column['table_id'] = $table_id;
            $column_id = $this->StructureModel->insertColumn($column);
        } else {
            $this->StructureModel->updateColumn($column_id, $column);
        }

        redirect('structure/field/form/' . $column_id . '/' . $table_id . '/success');
    }

    public function moveup($column_id, $table_id) {
        $this->StructureModel->moveUpColumn($column_id);
        redirect('structure/field/index/' . $table_id);
    }

    public function movedown($column_id, $table_id) {
        $this->StructureModel->moveDownColumn($column_id);
        redirect('structure/field/index/' . $table_id);
    }

    public function delete($column_id, $table_id) {
        $this->StructureModel->deleteColumn($column_id);
        redirect('structure/field/index/' . $table_id);
    }

}