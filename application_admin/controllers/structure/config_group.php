<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Config_group extends CI_Controller {

    var $sel_menu = 'structure';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('s', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('ConfigGroupModel'));
    }

    public function index() {
        $this->db->select('config_group_id');
        $query = $this->db->get('mother_config_group');
        $total_content = $query->num_rows();
        $this->db->flush_cache();
        $dataContent['total_content'] = $total_content;
        $dataNavigate['content'] = $this->load->view('structure/config/list_group', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($config_group_id = 0, $status = '') {
        $dataContent['config_group_id'] = $config_group_id;
        $max_priority = 0;
        if ($config_group_id <= 0) {
            $dataContent['title'] = 'Create Config Group';
            $max_priority = $this->ConfigGroupModel->getMaxConfigGroupPriority(0);
            $max_priority++;
        } else {
            $this->db->select('mother_config_group.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_config_group.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_config_group.update_by', 'left');
            $this->db->where('mother_config_group.config_group_id', $config_group_id);
            $query = $this->db->get('mother_config_group');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->config_group_name;
                $max_priority = $this->ConfigGroupModel->getMaxConfigGroupPriority(0);
            }
        }
        $dataContent['max_priority'] = $max_priority;
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('structure/config/form_group', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($config_group_id = 0) {
        $config = array(
            'config_group_name' => $_POST['config_group_name'],
            'sort_priority' => $_POST['sort_priority']
        );
        if ($config_group_id <= 0) {
            $config_group_id = $this->ConfigGroupModel->insertConfigGroup($config);
        } else {
            $this->ConfigGroupModel->updateConfigGroup($config_group_id, $config);
        }
        redirect('structure/config_group/form/' . $config_group_id . '/success');
    }

    public function moveup($config_id) {
        $this->ConfigGroupModel->moveUpConfigGroup($config_id);
        redirect('structure/config_group/index/');
    }

    public function movedown($config_id) {
        $this->ConfigGroupModel->moveDownConfigGroup($config_id);
        redirect('structure/config_group/index/');
    }

    public function delete($config_id) {
        $this->ConfigGroupModel->deleteConfigGroup($config_id);
        redirect('structure/config_group/index/');
    }

}

?>
