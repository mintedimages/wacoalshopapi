<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop extends CI_Controller {

    var $sel_menu = 'structure';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('s', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('ShopModel', 'StructureModel'));
    }

    public function index() {
        $this->db->select('shop_id');
        $query = $this->db->get('mother_shop');
        $total_content = $query->num_rows();
        $this->db->select('mother_shop.*');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = mother_shop.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = mother_shop.update_by', 'left');
        $this->db->group_by('mother_shop.shop_id');
        $this->db->order_by('mother_shop.sort_priority', 'asc');
        $dataContent['query'] = $this->db->get('mother_shop');
        $this->db->flush_cache();
        $dataContent['total_content'] = $total_content;
        $dataNavigate['content'] = $this->load->view('structure/shop/list', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($shop_id = 0, $status = '') {
        $dataContent['shop_id'] = $shop_id;
        $max_priority = $this->ShopModel->getMaxPriority();
        if ($shop_id <= 0) {
            $dataContent['title'] = 'Create Shop';
            $max_priority++;
        } else {
            $this->db->select('mother_shop.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_shop.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_shop.update_by', 'left');
            $this->db->where('mother_shop.shop_id', $shop_id);
            $query = $this->db->get('mother_shop');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->shop_name;
            }
        }
        $dataContent['max_priority'] = $max_priority;
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('structure/shop/form', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($shop_id = 0) {
        $shop = array(
            'shop_name' => $_POST['shop_name'],
            'shop_code' => $_POST['shop_code'],
            'sort_priority' => $_POST['sort_priority']
        );
        if ($shop_id <= 0) {
            $shop_id = $this->ShopModel->insert($shop);
        } else {
            $this->ShopModel->update($shop_id, $shop);
        }
        redirect('structure/shop/form/' . $shop_id . '/success');
    }

    public function moveup($shop_id) {
        $this->ShopModel->moveUp($shop_id);
        redirect('structure/shop/index/');
    }

    public function movedown($shop_id) {
        $this->ShopModel->moveDown($shop_id);
        redirect('structure/shop/index/');
    }

    public function delete($shop_id) {
        $this->ShopModel->delete($shop_id);
        redirect('structure/shop/index/');
    }

}