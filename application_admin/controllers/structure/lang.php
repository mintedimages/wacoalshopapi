<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lang extends CI_Controller {
    var $sel_menu = 'structure';
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('s', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('LangModel', 'StructureModel'));
    }

    public function index() {
        $this->db->select('lang_id');
        $query = $this->db->get('mother_lang');
        $total_content = $query->num_rows();
        $this->db->select('mother_lang.*');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = mother_lang.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = mother_lang.update_by', 'left');
        $this->db->group_by('mother_lang.lang_id');
        $this->db->order_by('mother_lang.sort_priority', 'asc');
        $dataContent['query'] = $this->db->get('mother_lang');
        $this->db->flush_cache();
        $dataContent['total_content'] = $total_content;
        $dataNavigate['content'] = $this->load->view('structure/lang/list', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($lang_id = 0, $status = '') {
        $dataContent['lang_id'] = $lang_id;
        $max_priority = $this->LangModel->getMaxPriority();
        if ($lang_id <= 0) {
            $dataContent['title'] = 'Create Language';
            $max_priority++;
        } else {
            $this->db->select('mother_lang.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_lang.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_lang.update_by', 'left');
            $this->db->where('mother_lang.lang_id', $lang_id);
            $query = $this->db->get('mother_lang');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->lang_name;
            }
        }
        $dataContent['max_priority'] = $max_priority;
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('structure/lang/form', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($lang_id = 0) {
        $lang = array(
            'lang_name' => $_POST['lang_name'],
            'lang_code' => $_POST['lang_code'],
            'sort_priority' => $_POST['sort_priority']
        );
        if ($lang_id <= 0) {
            $lang_id = $this->LangModel->insert($lang);
        } else {
            $this->LangModel->update($lang_id, $lang);
        }
        redirect('structure/lang/form/' . $lang_id . '/success');
    }
    public function moveup($lang_id) {
        $this->LangModel->moveUp($lang_id);
        redirect('structure/lang/index/');
    }
    public function movedown($lang_id) {
        $this->LangModel->moveDown($lang_id);
        redirect('structure/lang/index/');
    }
    public function delete($lang_id) {
        $this->LangModel->delete($lang_id);
        redirect('structure/lang/index/');
    }
}