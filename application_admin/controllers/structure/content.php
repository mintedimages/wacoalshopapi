<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends CI_Controller {

    var $sel_menu = 'structure';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('s', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('StructureModel'));
    }

    public function index() {
        $this->db->select('table_id');
        $query = $this->db->get('mother_table');
        $total_content = $query->num_rows();
        $this->db->flush_cache();
        $dataContent['total_content'] = $total_content;
        $dataNavigate['content'] = $this->load->view('structure/content/list', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($table_id = 0, $parent_table_id = 0, $status = '') {
        $dataContent['table_id'] = $table_id;
        $dataContent['parent_table_id'] = $parent_table_id;
        $max_priority = 0;
        if ($table_id <= 0) {
            $dataContent['title'] = 'Create Content';
            $max_priority = $this->StructureModel->getMaxTablePriority($parent_table_id);
            $max_priority++;
        } else {
            $this->db->select('mother_table.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_table.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_table.update_by', 'left');
            $this->db->where('mother_table.table_id', $table_id);
            $query = $this->db->get('mother_table');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->table_name;
                $dataContent['parent_table_id'] = $dat->parent_table_id;
                $max_priority = $this->StructureModel->getMaxTablePriority($dat->parent_table_id);
            }
        }
        $this->db->order_by('mother_icon.sort_priority', 'asc');
        $dataContent['queryIcon'] = $this->db->get('mother_icon');
        $dataContent['max_priority'] = $max_priority;
        $dataContent['status'] = $status;
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('structure/content/form', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($table_id = 0) {
        $table = array(
            'parent_table_id' => $_POST['parent_table_id'],
            'table_name' => $_POST['table_name'],
            'table_code' => $_POST['table_code'],
            'table_type' => $_POST['table_type'],
            'table_order' => $_POST['table_order'],
            'table_newcontent' => $_POST['table_newcontent'],
            'table_recursive' => $_POST['table_recursive'],
            'table_preview' => $_POST['table_preview'],
            'icon_id' => $_POST['icon_id'],
            'sort_priority' => $_POST['sort_priority']
        );
        $this->db->where('table_code', $_POST['table_code']);
        $this->db->where('table_id !=', $table_id);
        $query = $this->db->get('mother_table');
        if ($query->num_rows() == 0) {
            if ($table_id <= 0) {
                $table_id = $this->StructureModel->insertTable($table);
            } else {
                $this->StructureModel->updateTable($table_id, $table);
            }
            redirect('structure/content/form/' . $table_id . '/' . $_POST['parent_table_id'] . '/success');
        } else {
            redirect('structure/content/form/' . $table_id . '/error/tablecode');
        }
    }

    public function moveup($table_id) {
        $this->StructureModel->moveUpTable($table_id);
        redirect('structure/content/index/');
    }

    public function movedown($table_id) {
        $this->StructureModel->moveDownTable($table_id);
        redirect('structure/content/index/');
    }

    public function delete($table_id) {
        $this->StructureModel->deleteTable($table_id);
        redirect('structure/content/index/');
    }

    public function exportstructure($table_id = 0) {
        $this->load->model('ExportStructureModel');
        $query = false;
        $file_name = 'structure';
        if ($table_id == 0) {
            $this->db->where('parent_table_id', 0);
            $this->db->order_by('sort_priority', 'asc');
            $query = $this->db->get('mother_table');
        } else {
            $this->db->where('table_id', $table_id);
            $query = $this->db->get('mother_table');
            $file_name = $query->row()->table_code;
        }
        $data['query_table'] = $query;
        header("Content-disposition: attachment; filename=" . $file_name . ".json");
        header('Content-Type: application/json');
        $this->load->view('structure/content/exportstructure/main', $data);
    }

    public function importstructure() {
        $this->load->model('ExportStructureModel');
        $status = 'success';
        $file_element_name = 'file_structure';
        if ($status != "error") {
            $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
                echo $msg;
            } else {
                $data = $this->upload->data();
                $json = file_get_contents($data['full_path']); // this WILL do an http request for you
                $datajson = json_decode($json);
                $table_length = count($datajson->mother_table);
                for ($i = 0; $i < $table_length; $i++) {
                    $this->ExportStructureModel->importTable($datajson->mother_table[$i], 0, $i);
                }
            }
            @unlink($_FILES[$file_element_name]);
            redirect('structure/content/index/');
        }
    }
    
    public function getMaxpriority($table_id = 0, $parent_table_id = 0) {
        $dataContent['table_id'] = $table_id;
        $dataContent['parent_table_id'] = $parent_table_id;
        $max_priority = 0;
        if ($table_id <= 0) {
            $max_priority = $this->StructureModel->getMaxTablePriority($parent_table_id);
        }
        else {
            $this->db->select('mother_table.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_table.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_table.update_by', 'left');
            $this->db->where('mother_table.parent_table_id', $parent_table_id);
            $query = $this->db->get('mother_table');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $max_priority = $this->StructureModel->getMaxTablePriority($dat->parent_table_id);
            }
        }
        $max_priority++;
        echo $max_priority;
    }

}