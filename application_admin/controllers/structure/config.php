<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Config extends CI_Controller {

    var $sel_menu = 'structure';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('s', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('FieldTypeModel', 'ConfigModel', 'ConfigGroupModel'));
    }

    public function index($config_group_id = 0) {
        $dataContent['config_group_id'] = $config_group_id;
        if (is_numeric($config_group_id) && $config_group_id > 0) {
            $this->db->where('config_group_id', $config_group_id);
        }
        $this->db->select('config_id');
        $query = $this->db->get('mother_config');
        $total_content = $query->num_rows();
        $this->db->flush_cache();
        $dataContent['total_content'] = $total_content;
        $dataNavigate['content'] = $this->load->view('structure/config/list', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($config_group_id = 0, $config_id = 0, $status = '') {
        $dataContent['config_group_id'] = $config_group_id;
        $dataContent['config_id'] = $config_id;
        $max_priority = 0;
        if ($config_id <= 0) {
            $dataContent['title'] = 'Create Config';
            $max_priority = $this->ConfigModel->getMaxConfigPriority($config_group_id);
            $max_priority++;
        } else {
            $this->db->select('mother_config.*');
            $this->db->select('tbl_create.user_login_name AS create_name');
            $this->db->join('mother_user AS tbl_create', 'tbl_create.user_id = mother_config.create_by', 'left');
            $this->db->select('tbl_update.user_login_name AS update_name');
            $this->db->join('mother_user AS tbl_update', 'tbl_update.user_id = mother_config.update_by', 'left');
            $this->db->where('mother_config.config_id', $config_id);
            $query = $this->db->get('mother_config');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                $dataContent['dat'] = $dat;
                $dataContent['title'] = $dat->config_name;
                $max_priority = $this->ConfigModel->getMaxConfigPriority($config_group_id);
            }
        }
        $dataContent['max_priority'] = $max_priority;
        $data['status'] = $status;
        $dataNavigate['content'] = $this->load->view('structure/config/form', $dataContent, true);
        $data['content'] = $this->load->view('structure/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($config_group_id = 0, $config_id = 0) {
        $config = array(
            'config_group_id' => $config_group_id,
            'config_name' => $_POST['config_name'],
            'config_code' => $_POST['config_code'],
            'config_field_type' => $_POST['config_field_type'],
            'sort_priority' => $_POST['sort_priority']
        );
        if ($config_id <= 0) {
            $config['config_type'] = 'content';
            $config_id = $this->ConfigModel->insertConfig($config);
        } else {
            $this->ConfigModel->updateConfig($config_id, $config);
        }
        redirect('structure/config/form/' . $config_group_id . '/' . $config_id . '/success');
    }

    public function moveup($config_id, $config_group_id = 0) {
        $this->ConfigModel->moveUpConfig($config_id, $config_group_id);
        redirect('structure/config/index/' . $config_group_id);
    }

    public function movedown($config_id, $config_group_id = 0) {
        $this->ConfigModel->moveDownConfig($config_id, $config_group_id);
        redirect('structure/config/index/' . $config_group_id);
    }

    public function delete($config_id, $config_group_id = 0) {
        $this->ConfigModel->deleteConfig($config_id, $config_group_id);
        redirect('structure/config/index/' . $config_group_id);
    }

}

?>
