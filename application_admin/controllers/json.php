<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Json extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('BackendModel', 'FieldTypeModel', 'StructureModel', 'LangModel'));
    }

    public function index($table_id, $parent_id = 0, $mother_shop_id = 0) {
        $query = $this->BackendModel->getContentList($table_id, $parent_id, 0, 1, array(), $mother_shop_id);
        $results = $this->BackendModel->getCountContentList($table_id, $parent_id, array(), $mother_shop_id);
        $arr = array();
        foreach ($query->result() as $obj) {
            $arr[] = $obj;
        }
        $this->db->flush_cache();
        header('Content-Type: application/json');
        echo '{"success":"true","results":' . $results . ',"rows":' . json_encode($arr) . '}';
    }
    public function getLastUpdate($table_code, $parent_id = 0, $mother_shop_id = 0, $last_update_date = ''){
        $this->db->where('parent_id', $parent_id);
        $this->db->where('mother_shop_id', $mother_shop_id);
        $this->db->where('update_date >', $last_update_date);
        $query = $this->db->get('tbl_'.$table_code);
    }

    public function backbone($table_id, $parent_id = 0) {
        $query = $this->BackendModel->getContentList($table_id, $parent_id);
        $results = $this->BackendModel->getCountContentList($table_id, $parent_id);
        $arr = array();
        foreach ($query->result() as $obj) {
            $arr[] = $obj;
        }
        $this->db->flush_cache();
        header('Content-Type: application/json');
        echo json_encode($arr);
    }

    public function backbone_detail($table_id, $content_id) {
        $dat = $this->BackendModel->getContentDetail($table_id, $content_id);
        $this->db->flush_cache();
        header('Content-Type: application/json');
        echo json_encode($dat);
    }

}
