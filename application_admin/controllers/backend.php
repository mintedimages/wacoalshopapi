<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Backend extends CI_Controller {

    function __construct() {
        parent::__construct();
        //fix for ckfinder
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        $this->load->model(array('BackendModel', 'FieldTypeModel', 'StructureModel', 'LangModel'));
    }

    public function index($table_id, $parent_id = 0, $query_id = 0, $page = 1, $recursive_id = 0, $status = '') {
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission($table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $query_array = array();

        $per_page = 20;
        if ($query_id > 0) {
// load query string form database
            $this->input->load_query($query_id);
            $rows = $this->db->get_where('ci_query', array('id' => $query_id))->result();
            parse_str($rows[0]->query_string, $query_array);

            if (array_key_exists('per_page', $query_array)) {
                $per_page = $query_array['per_page'];
            }
        }

        $data = array();

        if (!is_numeric($page) || $page <= 0) {
            $page = 1;
        }
        if (!is_numeric($query_id) || $query_id < 0) {
            $query_id = 0;
        }
        $dataContent['table_id'] = $table_id;
        $dataContent['parent_id'] = $parent_id;
        $dataContent['recursive_id'] = $recursive_id;
        $table = $this->StructureModel->getTable($table_id);
        $dataContent['table_name'] = $table->table_name;
        $dataContent['table_code'] = $table->table_code;
        $dataContent['table_type'] = $table->table_type;
        $dataContent['table_newcontent'] = $table->table_newcontent;
        $dataContent['table_recursive'] = $table_recursive = $table->table_recursive;

        $dataContent['del_status'] = '';
        if ($status != '') {
            $dataContent['del_status'] = 'delete';
        }

        if ($table->table_type === 'static') {
            $content_id = 0;
            $this->db->where('mother_shop_id', $this->session->userdata('mother_shop_id'));
            $query_content_table = $this->db->get('tbl_' . $table->table_code);
            if ($query_content_table->num_rows() > 0) {
                $content_table = $query_content_table->row_array();
                $content_id = $content_table[$table->table_code . '_id'];
            }
            redirect('backend/form/' . $table_id . '/' . $content_id . '/' . $parent_id . '/');
        } else {
            if ($table_recursive == 'true') {
                $dataContent['query'] = $this->BackendModel->getContentList($table_id, $parent_id, 0, 1, $query_array, 0);
                $dataContent['per_page'] = $per_page;
                $dataContent['page'] = $page;
                $dataContent['query_id'] = $query_id;
                $dataContent['query_array'] = $query_array;
                $data['content'] = $this->load->view('backend/recursive_list', $dataContent, true);
            } else {
                $dataContent['query'] = $this->BackendModel->getContentList($table_id, $parent_id, $per_page, $page, $query_array);
                $dataContent['total_content'] = $count_all = $this->BackendModel->getCountContentList($table_id, $parent_id, $query_array);
                if (ceil($count_all / $per_page)) {
                    $dataContent['count_page'] = ceil($count_all / $per_page);
                } else {
                    $dataContent['count_page'] = 1;
                }
                $dataContent['per_page'] = $per_page;
                $dataContent['page'] = $page;
                $dataContent['query_id'] = $query_id;
                $dataContent['query_array'] = $query_array;
                $data['content'] = $this->load->view('backend/list', $dataContent, true);
            }
        }
        $data['sel_menu'] = $table_id;
        $this->load->view('masterpage', $data);
    }

//- sort priority on list and grid view sorting
    public function update_sort_priority($table_id, $content_id = 0, $parent_id = 0) {
        if ($parent_id != 0) {
            $content['parent_id'] = $parent_id;
        }
        $content['sort_priority'] = $_POST['sort_priority'];
        $this->BackendModel->update($table_id, $content_id, $content);
    }

//- query on click change view style on list | grid
    public function query_sort_priority($table_id, $parent_id = 0, $query_id = 0, $page = 1) {
        $query_array = array();

        if ($query_id > 0) {
// load query string form database
            $this->input->load_query($query_id);
            $rows = $this->db->get_where('ci_query', array('id' => $query_id))->result();
            parse_str($rows[0]->query_string, $query_array);
        }

        $per_page = 20;
        if (!is_numeric($page) || $page <= 0) {
            $page = 1;
        }
        if (!is_numeric($query_id) || $query_id < 0) {
            $query_id = 0;
        }
        $dataContent['table_id'] = $table_id;
        $dataContent['parent_id'] = $parent_id;
        $table = $this->StructureModel->getTable($table_id);
        $dataContent['table_name'] = $table->table_name;
        $dataContent['table_code'] = $table->table_code;
        $dataContent['table_type'] = $table->table_type;

        if ($table->table_type != 'static') {
            $dataContent['query'] = $this->BackendModel->getContentList($table_id, $parent_id, $per_page, $page, $query_array);
            $dataContent['total_content'] = $count_all = $this->BackendModel->getCountContentList($table_id, $parent_id, $query_array);
            if (ceil($count_all / $per_page)) {
                $dataContent['count_page'] = ceil($count_all / $per_page);
            } else {
                $dataContent['count_page'] = 1;
            }
            $dataContent['per_page'] = $per_page;
            $dataContent['page'] = $page;
            $dataContent['query_id'] = $query_id;
            $dataContent['query_array'] = $query_array;
            $dataContent['condition_selection'] = $_POST['condition'];
            echo $this->load->view('backend/view', $dataContent, TRUE);
        }
    }

    public function form($table_id, $content_id = 0, $parent_id = 0, $recursive_id = 0, $status = '') {
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission($table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
//configure base path of ckeditor folder
        $this->ckeditor->basePath = base_url() . 'ckeditor/';
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['height'] = '400';
//configure ckfinder with ckeditor config
        $this->ckeditor->returnOutput = true;
        $this->ckfinder->BasePath = 'ckfinder/';
        $this->ckfinder->SetupCKEditor($this->ckeditor, base_url() . 'ckfinder/');

        $dataContent['content_id'] = $content_id;
        $dataContent['table_id'] = $table_id;
        $dataContent['parent_id'] = $parent_id;
        $dataContent['recursive_id'] = $recursive_id;
        $dataContent['status'] = $status;
        $table = $this->StructureModel->getTable($table_id);
        $dataContent['table_name'] = $table->table_name;
        $dataContent['table_code'] = $table->table_code;
        $dataContent['table_type'] = $table->table_type;
        $dataContent['table_order'] = $table->table_order;
        $dataContent['table_recursive'] = $table->table_recursive;
        $max_priority = $this->BackendModel->getMaxPriority($table_id, $parent_id, $recursive_id);
        if ($content_id <= 0) {
            $dataContent['title'] = 'Create ' . $table->table_name;
            $max_priority++;
        } else {
            if ($table->table_type == 'static') {
                $total_content = $this->BackendModel->getCountContentList($table_id, $parent_id);
                if ($total_content == 0) {
                    $content = array();
                    $content_id = $this->BackendModel->insert($table_id, 0, $content);
                }
            }
            $dat = $this->BackendModel->getContentDetail($table_id, $content_id);
            $dataContent['title'] = $table->table_name;
            $dataContent['dat'] = $dat;
            if ($table->table_type != 'static') {
                $dataContent['parent_id'] = $dat['parent_id'];
            }
        }
        if ($table->table_type != 'static') {
            $dataContent['max_priority'] = ($max_priority < 1) ? 1 : $max_priority;
        }
        $data['content'] = $this->load->view('backend/form', $dataContent, true);
        $data['sel_menu'] = $table_id;
        $this->load->view('masterpage', $data);
    }

    public function form_post($table_id, $content_id = 0, $parent_id = 0) {
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkEditPermissionSelect($table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
            redirect('backend/index/' . $table_id . '/' . $parent_id);
        }
        $table = $this->StructureModel->getTable($table_id);
        $content = array();
        $contentLang = array();
        $columns = $this->StructureModel->getColumnList($table_id);
        foreach ($columns->result() AS $column) {
            if ($parent_id != 0) {
                $content['parent_id'] = $parent_id;
            }
            if ($column->column_lang > 0) {
                $query = $this->LangModel->queryLangName();
                foreach ($query->result() AS $row) {
                    if ($column->column_field_type == 'imagecrop') {
                        /*
                          $fieldCode = $column->column_code . '_' . $row->lang_code;
                          if ($_POST[$fieldCode . '_master_image'] != '') {
                          $this->load->model('CropImageModel');
                          $file_name = '_' . $fieldCode;
                          if ($content_id > 0) {
                          $file_name = $content_id . $file_name;
                          } else {
                          $next_content_id = $this->BackendModel->getLastId($table_id) + 1;
                          $file_name = $next_content_id . $file_name;
                          }
                          $contentLang[$row->lang_id][$column->column_code] = $this->CropImageModel->saveCropImage($_POST[$fieldCode . '_master_image'], $file_name, $_POST[$fieldCode . '_crop_target_w'], $_POST[$fieldCode . '_crop_target_h'], $_POST[$fieldCode . '_crop_x'], $_POST[$fieldCode . '_crop_y'], $_POST[$fieldCode . '_crop_w'], $_POST[$fieldCode . '_crop_h']);
                          } else {
                          $contentLang[$row->lang_id][$column->column_code] = $_POST[$column->column_code . '_' . $row->lang_code];
                          }
                         * 
                         */
                        if ($_POST[$column->column_code . '_' . $row->lang_code] != "") {
                            //check where cancel crop image
                            $contentLang[$row->lang_id][$column->column_code] = $_POST[$column->column_code . '_' . $row->lang_code];
                        }
                    } else {
                        $contentLang[$row->lang_id][$column->column_code] = $_POST[$column->column_code . '_' . $row->lang_code];
                    }
                }
            } else {
                if ($column->column_field_type == 'password') {
                    $content[$column->column_code] = MD5($_POST[$column->column_code]);
                } else if ($column->column_field_type == 'checkbox') {
                    $array = $_POST[$column->column_code];
                    $value = '';
                    if (count($array) > 0) {
                        foreach ($array as $arr) {
                            if ($value != '') {
                                $value .= ',';
                            }
                            $value .= $arr;
                        }
                    }
                    $content[$column->column_code] = $value;
                } elseif ($column->column_field_type == 'imagecrop') {
                    /*
                      if ($_POST[$column->column_code . '_master_image'] != '') {
                      $this->load->model('CropImageModel');
                      $file_name = '_' . $column->column_code;
                      if ($content_id > 0) {
                      $file_name = $content_id . $file_name;
                      } else {
                      $next_content_id = $this->BackendModel->getLastId($table_id) + 1;
                      $file_name = $next_content_id . $file_name;
                      }
                      $content[$column->column_code] = $this->CropImageModel->saveCropImage($_POST[$column->column_code . '_master_image'], $file_name, $_POST[$column->column_code . '_crop_target_w'], $_POST[$column->column_code . '_crop_target_h'], $_POST[$column->column_code . '_crop_x'], $_POST[$column->column_code . '_crop_y'], $_POST[$column->column_code . '_crop_w'], $_POST[$column->column_code . '_crop_h']);
                      } else {
                      $content[$column->column_code] = $_POST[$column->column_code];
                      }
                     * 
                     */
                    if ($_POST[$column->column_code] != "") {
                        //check where cancel crop image
                        $content[$column->column_code] = $_POST[$column->column_code];
                    }
                } else {
                    $content[$column->column_code] = $_POST[$column->column_code];
                }
            }
        }
        $recursive_id = $content['recursive_id'] = 0;
        if ($table->table_type != 'static') {
            if ($table->table_recursive == 'true') {
                $recursive_id = $content['recursive_id'] = $_POST['recursive_id'];
            }
            $content['sort_priority'] = $_POST['sort_priority'];
            $content['enable_status'] = $_POST['enable_status'];
        }
        if ($content_id <= 0) {
            $content_id = $this->BackendModel->insert($table_id, $parent_id, $content);
        } else {
            $this->BackendModel->update($table_id, $content_id, $content, $recursive_id);
        }
        $this->BackendModel->updateLang($table_id, $content_id, $contentLang);
        redirect('backend/form/' . $table_id . '/' . $content_id . '/' . $parent_id . '/' . $recursive_id . '/success');
    }

    public function moveup($table_id, $content_id, $page = 1) {
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission(-$table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
            redirect('backend/index' . $table_id . '/' . $parent_id);
        }
        $this->BackendModel->moveUp($table_id, $content_id);
        $parent_id = $this->BackendModel->getParentId($table_id, $content_id);
        redirect('backend/index/' . $table_id . '/' . $parent_id . '/0/' . $page);
    }

    public function movedown($table_id, $content_id, $page = 1) {
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission(-$table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
            redirect('backend/index' . $table_id . '/' . $parent_id . '/0/' . $page);
        }
        $this->BackendModel->moveDown($table_id, $content_id);
        $parent_id = $this->BackendModel->getParentId($table_id, $content_id);
        redirect('backend/index/' . $table_id . '/' . $parent_id . '/0/' . $page);
    }

    public function delete($table_id, $content_id, $page = 1) {
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission(-$table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
            redirect('backend/index' . $table_id . '/' . $parent_id . '/0/' . $page);
        }
        $parent_id = $this->BackendModel->getParentId($table_id, $content_id);
        $this->BackendModel->delete($table_id, $content_id);
        redirect('backend/index/' . $table_id . '/' . $parent_id . '/0/0/delete');
    }

    public function deletecheck($table_id, $parent_id = 0, $page = 1) {
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission(-$table_id, $this->session->userdata('mother_shop_id'), $this->session->userdata('user_group_id'))) {
            redirect('backend/index' . $table_id . '/' . $parent_id . '/0/' . $page);
        }
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox); $i++) {
            $content_id = $checkbox[$i];
            $this->BackendModel->delete($table_id, $content_id);
        }
        redirect('backend/index/' . $table_id . '/' . $parent_id . '/0/' . $page);
    }

    public function json($table_id, $parent_id = 0) {
        $query = $this->BackendModel->getContentList($table_id, $parent_id);
        $results = $this->BackendModel->getCountContentList($table_id, $parent_id);
        $arr = array();
        foreach ($query->result() as $obj) {
            $arr[] = $obj;
        }
        $this->db->flush_cache();
        header('Content-Type: application/json');
        echo '{success:true,results:' . $results . ',rows:' . json_encode($arr) . '}';
    }

    public function exportdata($table_id, $parent_id = 0) {
        $this->load->model(array('ExportDataModel'));
        $table = $this->StructureModel->getTable($table_id);
        $data['table_id'] = $table_id;
        $data['parent_id'] = $parent_id;
        $file_name = 'data_' . $table->table_code;
        header("Content-disposition: attachment; filename=" . $file_name . ".json");
        header('Content-Type: application/json');
        $this->load->view('backend/exportdata/jsonmain', $data);
    }

    public function exportdataexcel($table_id, $parent_id = 0) {
        $table = $this->StructureModel->getTable($table_id);
        $file_name = 'data_' . $table->table_code;
        /** Error reporting */
        error_reporting(E_ALL);

        /** Include path * */
        ini_set('include_path', ini_get('include_path') . ';../Classes/');

        /** PHPExcel */
        include 'PHPExcel/Classes/PHPExcel.php';
        include 'PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
        include 'PHPExcel/Classes/PHPExcel/Writer/Excel5.php';

        $objPHPExcel = new PHPExcel();
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        header("Content-Disposition: attachment;filename=$file_name.xls");
        header("Content-Transfer-Encoding: binary ");
        $objPHPExcel->setActiveSheetIndex(0);
        $num_row = 1;
        $num_col = 65;
        //get lang list
        $query_lang = $this->LangModel->queryLangName();

        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $this->BackendModel->getTableCode($table_id) . '_id');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'mother_shop_id');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'parent_id');
        $columns = $this->StructureModel->getColumnList($table_id, 0);
        foreach ($columns->result() AS $column) {
            if ($column->column_lang == 0) {
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $column->column_code);
            } else {
                foreach ($query_lang->result() as $row_lang) {
                    $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $column->column_code . '_' . $row_lang->lang_code);
                }
            }
        }

        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'recursive_id');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'sort_priority');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'enable_status');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'create_date');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'create_by');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'update_date');
        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), 'update_by');

        $num_row++;
        $this->BackendModel->showRecursiveExcelList($table_id, $parent_id, 0, $objPHPExcel, $num_row);

        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        ob_end_clean();
        $objWriter->save('php://output');
    }

    public function importdata($table_id, $parent_id = 0) {
        $this->load->model('ExportDataModel');
        $table = $this->StructureModel->getTable($table_id);
        $content = array();
        $contentLang = array();
        $status = 'success';
        $file_element_name = 'file_data';
        if ($status != "error") {
            $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
                echo $msg;
            } else {
                $data = $this->upload->data();
                $json = file_get_contents($data['full_path']); // this WILL do an http request for you
                $datajson = json_decode($json, true);
                $this->ExportDataModel->importData($table_id, $parent_id, $datajson['tbl_' . $table->table_code]);
            }
            @unlink($_FILES[$file_element_name]);
            redirect('backend/index/' . $table_id . '/' . $parent_id);
        }
    }

    public function importdataexcel($table_id, $parent_id = 0) {
        $this->load->model('ExportDataModel');
        $status = 'success';
        $file_element_name = 'file_data';
        if ($status != "error") {
            $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
                echo $msg;
            } else {
                /** PHPExcel */
                require_once 'PHPExcel/Classes/PHPExcel.php';
                /** PHPExcel_IOFactory - Reader */
                include 'PHPExcel/Classes/PHPExcel/IOFactory.php';
                $data = $this->upload->data();
                $inputFileName = $data['full_path'];
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($inputFileName);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();

                $headingsArray = $objWorksheet->rangeToArray('A1:' . $highestColumn . '1', null, true, true, true);
                $headingsArray = $headingsArray[1];

                $r = -1;
                $namedDataArray = array();
                for ($row = 2; $row <= $highestRow; ++$row) {
                    $dataRow = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true, true);
                    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                        ++$r;
                        foreach ($headingsArray as $columnKey => $columnHeading) {
                            $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                        }
                    }
                }
                $this->ExportDataModel->importData($table_id, $parent_id, $namedDataArray);
            }
            @unlink($_FILES[$file_element_name]);
        }
        redirect('backend/index/' . $table_id . '/' . $parent_id);
    }

    public function export($table_id, $parent_id = 0) {
        $dataContent['table_id'] = $table_id;
        $dataContent['parent_id'] = $parent_id;
        $dataContent['table_name'] = $this->StructureModel->getTableName($table_id);
        $dataContent['table_code'] = $this->StructureModel->getTableCode($table_id);
        $dataContent['query'] = $this->BackendModel->getContentList($table_id, $parent_id);
        $dataContent['total_content'] = $this->BackendModel->getCountContentList($table_id, $parent_id);
        $dataContent['filename'] = 'Export_' . $dataContent['table_name'] . '_' . $this->MyUtilitiesModel->getNow();
        $this->load->view('backend/export', $dataContent);
    }

    public function search($table_id = 0, $parent_id = 0, $page = 1) {
        $query_array = array();
        $getSearchColumnList = $this->StructureModel->getSearchColumnList($table_id);
        if ($getSearchColumnList->num_rows() > 0) {
            foreach ($getSearchColumnList->result_array() AS $row) {
                if ($row['column_lang'] > 0) {
                    $query = $this->LangModel->queryLangName();
                    foreach ($query->result_array() AS $row2) {
                        $query_array = array_merge((array) $query_array, (array) $this->FieldTypeModel->displayFieldSearchPost($row, $row2, true));
                    }
                } else {
                    $query_array = array_merge((array) $query_array, (array) $this->FieldTypeModel->displayFieldSearchPost($row, null, true));
                }
            }
        }

        $query_array['per_page'] = $_POST['per_page'];
        $query_id = $this->input->save_query($query_array);
        redirect('backend/index/' . $table_id . '/' . $parent_id . '/' . $query_id . '/' . $page);
    }

    public function uploadImage() {
        $imagePath = '/uploads/';

        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);

        if (in_array($extension, $allowedExts)) {
            if ($_FILES["img"]["error"] > 0) {
                $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: ' . $_FILES["img"]["error"],
                );
                echo "Return Code: " . $_FILES["img"]["error"] . "<br>";
            } else {

                $filename = $_FILES["img"]["tmp_name"];
                list($width, $height) = getimagesize($filename);

                /*
                  echo $filename;
                  echo '</br>';
                  echo $imagePath . $_FILES["img"]["name"];
                  echo '</br>';
                  echo (is_dir($imagePath)) ? 'true' : 'false';

                 */
                move_uploaded_file($filename, $_SERVER["DOCUMENT_ROOT"] . $imagePath . $_FILES["img"]["name"]);


                $response = array(
                    "status" => 'success',
                    "url" => $imagePath . $_FILES["img"]["name"],
                    "width" => $width,
                    "height" => $height
                );
            }
        } else {
            $response = array(
                "status" => 'error',
                "message" => 'something went wrong',
            );
        }

        print json_encode($response);
    }

    public function uploadImageCrop() {
        $imgUrl = $_SERVER["DOCUMENT_ROOT"] . $_POST['imgUrl'];
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        $imgW = $_POST['imgW'];
        $imgH = $_POST['imgH'];
        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];
        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];

        $jpeg_quality = 100;

        $output_filename = '/uploads/croppedImg_' . rand();
        //$output_filename = './uploads/';

        $what = getimagesize($imgUrl);
        switch (strtolower($what['mime'])) {
            case 'image/png':
                $img_r = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                $type = '.jpeg';
                break;
            case 'image/gif':
                $img_r = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default: die('image type not supported');
        }

        $resizedImage = imagecreatetruecolor($imgW, $imgH);
        imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);


        $dest_image = imagecreatetruecolor($cropW, $cropH);
        imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);

        imagejpeg($dest_image, $_SERVER["DOCUMENT_ROOT"] . $output_filename . $type, $jpeg_quality);

        $response = array(
            "status" => 'success',
            "url" => $output_filename . $type
        );
        print json_encode($response);
    }

}
