<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index($err_id = 0, $user_login_name = '') {
        $this->load->library('migration');
        if (!$this->db->table_exists('migrations')) {
            redirect('welcome/updateTable');
        }
        if (!$this->migration->current()) {
            redirect('welcome/updateTable');
        }
        if ($this->session->userdata('logged_in') == TRUE) {
            redirect('dashboard');
        }
        $data['err_id'] = $err_id;
        $data['user_login_name'] = $user_login_name;
        $this->load->view('login', $data);
    }

    public function login() {
        if (!isset($_POST['user_login_name'])) {
            redirect('welcome/index');
        }
        $error_status = 1;
        // 1 = 'not found user'
        $user_login_name = trim($_POST['user_login_name']);
        $user_login_password = trim($_POST['user_login_password']);
        $logged_in = FALSE;
        if ($user_login_name != '') {
            $this->db->select('mother_user.*');
            $this->db->select('mother_user_group.user_group_permission');
            $this->db->join('mother_user_group', 'mother_user_group.user_group_id = mother_user.user_group_id', 'left');
            $this->db->where('user_login_name', $user_login_name);
            $this->db->where('user_login_password', md5($user_login_password));
            $this->db->where('mother_user.enable_status', 'enable');
            $query = $this->db->get('mother_user');
            if ($query->num_rows() > 0) {
                $dat = $query->row();
                
                $shop_id = $this->ShopModel->getDefaultShop()->shop_id;
                if(isset($_POST['shop_id'])){
                    $shop_id = $_POST['shop_id'];
                }
                $user_group_permission = $this->PermissionModel->getPermissionToString($dat->user_group_id);
                $data = array('user_id' => $dat->user_id, 'user_login_name' => $dat->user_login_name, 'user_group_id' => $dat->user_group_id, 'user_group_permission' => $user_group_permission, 'mother_shop_id' => $shop_id, 'logged_in' => TRUE);
                $this->session->set_userdata($data);
                $logged_in = TRUE;
            }
        }
        if ($logged_in) {
            redirect('dashboard');
        } else {
            redirect('welcome/index/' . $error_status . '/' . $user_login_name);
        }
    }

    public function updateTable() {
        $this->load->library('migration');
        if (!$this->migration->current()) {
            show_error($this->migration->error_string());
        } else {
            redirect('welcome');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('welcome/index');
    }

    public function changeShop() {
        $shop_id = $_POST['change_shop'];
        $this->session->set_userdata('mother_shop_id', $shop_id);
        redirect('welcome');
    }

}