<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log extends CI_Controller {

    var $sel_menu = 'log';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('l', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('LogActionModel', 'UserModel', 'UserGroupModel'));
    }

    public function index($page = 1) {
        if (!is_numeric($page) || $page <= 0) {
            $page = 1;
        }
        $query = $this->db->get('log_action');
        $total_content = $query->num_rows();
        $per_page = 20;
        $this->db->limit($per_page, ($page * $per_page) - $per_page);
        $dataContent['query'] = $this->db->get('log_action');
        $dataContent['total_content'] = $total_content;
        $dataContent['per_page'] = $per_page;
        $data['content'] = $this->load->view('log/list', $dataContent, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form($action_id = 0) {
        $dataContent['action_id'] = $action_id;
        $this->db->where('action_id', $action_id);
        $query = $this->db->get('log_action');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $dataContent['dat'] = $dat;
            $dataContent['title'] = 'Log Detail';
        }
        $data['content'] = $this->load->view('log/form', $dataContent, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

}

?>
