<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Config extends CI_Controller {

    var $sel_menu = 'setting';

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
        if ($this->session->userdata('user_group_id') != 0 && !$this->PermissionModel->checkNavigatePermission('c', 0, $this->session->userdata('user_group_id'))) {
            redirect('dashboard');
        }
        $this->load->model(array('FieldTypeModel', 'ConfigGroupModel', 'ConfigModel'));
    }

    public function index($config_group_id = 0) {
        $dataContent = array();
        if (!is_numeric($config_group_id) || $config_group_id <= 0) {
            $config_group_id = $this->ConfigGroupModel->getFirstConfigGroupId();
        }
        $dataContent['config_group_id'] = $config_group_id;
        $dataNavigate['content'] = $this->load->view('config/form', $dataContent, true);
        $data['content'] = $this->load->view('config/navigate', $dataNavigate, true);
        $data['sel_menu'] = $this->sel_menu;
        $this->load->view('masterpage', $data);
    }

    public function form_post($config_group_id) {
        $columns = $this->ConfigModel->getColumnList($config_group_id);
        foreach ($columns->result_array() AS $column) {
            $this->db->set('config_value', $_POST[$column['config_code']]);
            $this->db->where('config_code', $column['config_code']);
            $this->db->update('mother_config');
        }
        redirect('config/index/' . $config_group_id . '/success');
    }

}

?>
