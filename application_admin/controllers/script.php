<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Script extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('welcome/login');
        }
    }

    public function insertContents() {
        /*
        $this->db->where('unit_code', 'room801');
        $query = $this->db->get('tbl_unit');
        if ($query->num_rows() == 0) {
            $sort_priority = 1;
            for ($i = 2; $i <= 4; $i++) {
                for ($j = 14; $j <= 18; $j++) {
                    $this->insertContent($i, $j, $sort_priority);
                    $sort_priority++;
                }
            }
            $this->insertContent(5, 1, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 4, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 6, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 7, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 9, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 12, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 14, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 16, $sort_priority);
            $sort_priority++;
            $this->insertContent(5, 18, $sort_priority);
            $sort_priority++;
            for ($j = 12; $j <= 18; $j++) {
                if ($j != 17) {
                    $this->insertContent(6, $j, $sort_priority);
                    $sort_priority++;
                }
            }
            for ($i = 7; $i <= 25; $i++) {
                for ($j = 1; $j <= 18; $j++) {
                    if ($j != 17) {
                        $this->insertContent($i, $j, $sort_priority);
                        $sort_priority++;
                    }
                }
            }
            for ($j = 1; $j <= 4; $j++) {
                $this->insertContent(26, $j, $sort_priority);
                $sort_priority++;
            }
            for ($j = 7; $j <= 11; $j++) {
                $this->insertContent(26, $j, $sort_priority);
                $sort_priority++;
            }
            $this->insertContent(26, 16, $sort_priority);
            $sort_priority++;
            $this->insertContent(26, 18, $sort_priority);
            $sort_priority++;
            $this->insertContent(27, 1, $sort_priority);
            $sort_priority++;
            $this->insertContent(27, 7, $sort_priority);
            $sort_priority++;
            $this->insertContent(27, 10, $sort_priority);
            $sort_priority++;
            $this->insertContent(27, 16, $sort_priority);
            $sort_priority++;
            $this->insertContent(27, 18, $sort_priority);
            $sort_priority++;
            $this->insertContent(28, 1, $sort_priority);
            $sort_priority++;
            $this->insertContent(28, 4, $sort_priority);
            $sort_priority++;
            $this->insertContent(28, 7, $sort_priority);
            $sort_priority++;
            $this->insertContent(29, 1, $sort_priority);
            $sort_priority++;
            $this->insertContent(30, 1, $sort_priority);
            $sort_priority++;
        }
         * 
         */
        echo "Success";
    }

    function insertContent($floor_index, $unit_index, $sort_priority) {
        $unit_name = ($floor_index * 100) + $unit_index;
        $this->db->set('parent_id', 1);
        $this->db->set('sort_priority', $sort_priority);
        $this->db->set('enable_status', 'show');
        $this->db->set('unit_code', 'room' . $unit_name);
        $this->db->set('unit_name', $unit_name);
        $this->db->set('booking_status_id', 1);
        $this->db->set('roomplan_id', 1);
        $this->db->set('building_id', 1);
        $this->db->set('floor_index', $floor_index);
        $this->db->set('unit_index', $unit_index);
        $this->db->insert('tbl_unit');
    }

}