<?php

class LogActionModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insert($table_name, $content_id, $action_name, $action_detail, $action_query) {
        $create_by = $this->session->userdata('user_id');
        $create_ip = $_SERVER['REMOTE_ADDR'];
        $this->db->set('table_name', $table_name);
        $this->db->set('content_id', $content_id);
        $this->db->set('action_name', $action_name);
        $this->db->set('action_detail', $action_detail);
        $this->db->set('action_query', $action_query);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $create_by);
        $this->db->set('create_ip', $create_ip);
        $this->db->insert('log_action');
    }

}

?>