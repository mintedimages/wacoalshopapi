<?php

class LangModel extends CI_Model {

    private $modelName = 'LangModel';

    function __construct() {
        parent::__construct();
    }

    function insert($lang) {
        $action_name = 'create lang';
        $action_detail = 'model : ' . $this->modelName;
        $this->moveUpPriority($lang['sort_priority']);
        foreach ($lang as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_lang');
        $action_query = $this->db->last_query();
        $this->db->select_max('lang_id', 'max_id');
        $max_id = $this->db->get('mother_lang')->row()->max_id;
        $this->LogActionModel->insert('mother_lang', $max_id, $action_name, $action_detail, $action_query);

        // update old data to new language
        $query = $this->TableModel->queryTable();
        foreach ($query->result() as $row) {
            $this->db->select_max($row->table_code . '_id', 'max');
            $query2 = $this->db->get('tbl_' . $row->table_code);
            $max = $query2->row()->max;
            for ($i = 1; $i <= $max; $i++) {
                $data = array(
                    $row->table_code . '_id' => $i,
                    'lang_id' => $max_id
                );
                $this->db->insert('tbl_' . $row->table_code . '_lang', $data);
            }
        }

        return $max_id;
    }

    function update($lang_id, $lang) {
        $action_name = 'update lang';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('lang_id', $lang_id);
        $query = $this->db->get('mother_lang');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            //$this->moveDownPriority($dat['sort_priority']);
            //$this->moveUpPriority($lang['sort_priority']);
            $action_detail .= '<br /><b>Update</b>';
            foreach ($lang as $key => $value) {
                if ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('lang_id', $lang_id);
            $this->db->update('mother_lang');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found lang_id';
            $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveUp($lang_id) {
        $action_name = 'move up lang';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />lang_id : ' . $lang_id;
        $action_query = '';
        $this->db->where('lang_id', $lang_id);
        $query = $this->db->get('mother_lang');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $this->moveDownPriority($dat->sort_priority);
                $this->moveUpPriority($dat->sort_priority - 1);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('lang_id', $lang_id);
                $this->db->update('mother_lang');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found lang_id';
            $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDown($lang_id) {
        $action_name = 'move down lang';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />lang_id : ' . $lang_id;
        $action_query = '';
        $max_priority = $this->getMaxPriority();
        $this->db->where('lang_id', $lang_id);
        $query = $this->db->get('mother_lang');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority <= $max_priority) {
                $this->moveDownPriority($dat->sort_priority);
                $this->moveUpPriority($dat->sort_priority + 1);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('lang_id', $lang_id);
                $this->db->update('mother_lang');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found lang_id';
            $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
        }
    }

    function delete($lang_id) {
        $action_name = 'delete lang';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />lang_id : ' . $lang_id;
        $action_query = '';
        $this->db->where('lang_id', $lang_id);
        $query = $this->db->get('mother_lang');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->moveDownPriority($dat->sort_priority);
            $this->db->where('lang_id', $lang_id);
            $this->db->delete('mother_lang');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);

            // delete old data to remove language
            $query = $this->TableModel->queryTable();
            foreach ($query->result() as $row) {
                $this->db->where('lang_id', $lang_id);
                $this->db->delete('tbl_' . $row->table_code . '_lang');
            }
        } else {
            $action_detail .= '<br />status : not found lang_id';
            $this->LogActionModel->insert('mother_lang', $lang_id, $action_name, $action_detail, $action_query);
        }
    }

    function queryLangName() {
        return $this->db->get('mother_lang');
    }

    function getLangName($lang_id) {
        $this->db->where('lang_id', $lang_id);
        $query = $this->db->get('mother_lang');
        return $query->row()->lang_name;
    }

    function getLangCode($lang_id) {
        $this->db->where('lang_id', $lang_id);
        $query = $this->db->get('mother_lang');
        return $query->row()->lang_code;
    }

    function getLangId($lang_code) {
        $this->db->where('lang_code', $lang_code);
        $query = $this->db->get('mother_lang');
        return $query->row()->lang_id;
    }

    function getMaxPriority() {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $query = $this->db->get('mother_lang');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_lang');
    }

    function moveDownPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_lang');
    }

    function getDefaultLang() {
        $this->db->limit(1, 0);
        $query = $this->db->get('mother_lang');
        return $query->row();
    }

}

?>