<?php

class StructureModel extends CI_Model {

    private $modelName = 'StructureModel';

    function __construct() {
        parent::__construct();
        $this->load->model(array('TableModel', 'fieldTypeModel'));
    }

    function insertTable($table) {
        $action_name = 'create table';
        $action_detail = 'model : ' . $this->modelName;
        $this->moveUpTablePriority($table['parent_table_id'], $table['sort_priority']);
        foreach ($table as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_table');
        $action_query = $this->db->last_query();
        $this->db->select_max('table_id', 'max_id');
        $max_id = $this->db->get('mother_table')->row()->max_id;
        $this->LogActionModel->insert('mother_table', $max_id, $action_name, $action_detail, $action_query);
        $this->TableModel->createTable($table['table_code']);
        return $max_id;
    }

    function updateTable($table_id, $table) {
        $action_name = 'update table';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            if ($dat['sort_priority'] != $table['sort_priority']) {
                $this->moveDownTablePriority($dat['parent_table_id'], $dat['sort_priority']);
                $this->moveUpTablePriority($table['parent_table_id'], $table['sort_priority']);
            }
            $action_detail .= '<br /><b>Update</b>';
            if ($dat['table_code'] != $table['table_code']) {
                $this->TableModel->renameTable($dat['table_code'], $table['table_code']);
            }
            foreach ($table as $key => $value) {
                if ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('table_id', $table_id);
            $this->db->update('mother_table');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found table_id';
            $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveUpTable($table_id) {
        $action_name = 'move up table';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />table_id : ' . $table_id;
        $action_query = '';
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $this->moveDownTablePriority($dat->parent_table_id, $dat->sort_priority);
                $this->moveUpTablePriority($dat->parent_table_id, $dat->sort_priority - 1);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('table_id', $table_id);
                $this->db->update('mother_table');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found table_id';
            $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDownTable($table_id) {
        $action_name = 'move down table';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />table_id : ' . $table_id;
        $action_query = '';
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $max_priority = $this->getMaxTablePriority($dat->parent_table_id);
            if ($dat->sort_priority < $max_priority) {
                $this->moveDownTablePriority($dat->parent_table_id, $dat->sort_priority);
                $this->moveUpTablePriority($dat->parent_table_id, $dat->sort_priority + 1);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('table_id', $table_id);
                $this->db->update('mother_table');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found table_id';
            $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
        }
    }

    function deleteTable($table_id) {
        $action_name = 'delete table';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />table_id : ' . $table_id;
        $action_query = '';
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->moveDownTablePriority($dat->parent_table_id, $dat->sort_priority);
            $this->TableModel->dropTable($dat->table_code);
            $this->db->where('table_id', $table_id);
            $this->db->delete('mother_column');
            $this->db->where('table_id', $table_id);
            $this->db->delete('mother_table');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found table_id';
            $this->LogActionModel->insert('mother_table', $table_id, $action_name, $action_detail, $action_query);
        }
    }

    function getChildTableList($parent_table_id) {
        $this->db->where('parent_table_id', $parent_table_id);
        return $this->db->get('mother_table');
    }

    function getTable($table_id) {
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        return $query->row();
    }

    function getParentTableId($table_id) {
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        return $query->row()->parent_table_id;
    }

    function getTableName($table_id) {
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        return $query->row()->table_name;
    }

    function getTableCode($table_id) {
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        return $query->row()->table_code;
    }

    function getMaxTablePriority($parent_table_id = 0) {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $this->db->where('parent_table_id', $parent_table_id);
        $query = $this->db->get('mother_table');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpTablePriority($parent_table_id, $sort_priority) {
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('parent_table_id', $parent_table_id);
        $this->db->update('mother_table');
    }

    function moveDownTablePriority($parent_table_id, $sort_priority) {
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('parent_table_id', $parent_table_id);
        $this->db->update('mother_table');
    }

    public function getRowList($parent_table_id, $deep = 0) {
        $this->db->select('mother_table.*');
        $this->db->select('count(column_id) AS field_count');
        $this->db->join('mother_column', 'mother_column.table_id = mother_table.table_id', 'left');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = mother_table.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = mother_table.update_by', 'left');
        $this->db->where('mother_table.parent_table_id', $parent_table_id);
        $this->db->group_by('mother_table.table_id');
        $this->db->order_by('mother_table.sort_priority', 'asc');
        $query = $this->db->get('mother_table');
        foreach ($query->result() AS $row) {
            $dataContent['row'] = $row;
            $dataContent['deep'] = $deep;
            echo $this->load->view('structure/content/rowlist', $dataContent, true);
            $this->getRowList($row->table_id, $deep + 1);
        }
    }

    function getDropdownList($parent_table_id, $deep = 0, $current_table = 0) {
        $this->db->select('mother_table.*');
        $this->db->select('count(column_id) AS field_count');
        $this->db->join('mother_column', 'mother_column.table_id = mother_table.table_id', 'left');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = mother_table.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = mother_table.update_by', 'left');
        $this->db->where('mother_table.parent_table_id', $parent_table_id);
        $this->db->group_by('mother_table.table_id');
        $this->db->order_by('mother_table.sort_priority', 'asc');
        $query = $this->db->get('mother_table');
        $dataContent['current_table'] = $current_table;
        $dataContent['current_parent'] = $this->getParentTableId($current_table);
        foreach ($query->result() AS $row) {
            $dataContent['row'] = $row;
            $dataContent['deep'] = $deep;
            echo $this->load->view('structure/content/dropdownlist', $dataContent, true);
            $this->getDropdownList($row->table_id, $deep + 1, $current_table);
        }
    }

    function insertColumn($column) {
        $action_name = 'create column';
        $action_detail = 'model : ' . $this->modelName;
        $this->moveUpColumnPriority($column['table_id'], $column['sort_priority']);
        foreach ($column as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_column');
        $action_query = $this->db->last_query();
        $this->db->select_max('column_id', 'max_id');
        $max_id = $this->db->get('mother_column')->row()->max_id;
        $this->LogActionModel->insert('mother_column', $max_id, $action_name, $action_detail, $action_query);
        $this->db->where('table_id', $column['table_id']);
        $query = $this->db->get('mother_table');
        $table_code = $query->row()->table_code;
        if ($column['column_lang'] == '1') {
            $table_code = $table_code . '_lang';
        }
        $field_property = $this->fieldTypeModel->getFieldProperty($column);
        $this->TableModel->addColumn($table_code, $field_property);
        return $max_id;
    }

    function updateColumn($column_id, $column) {
        $action_name = 'delete column';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />column_id : ' . $column_id;
        $action_query = '';
        $this->db->select('mother_column.*');
        $this->db->select('mother_table.table_code');
        $this->db->join('mother_table', 'mother_table.table_id = mother_column.table_id', 'left');
        $this->db->where('column_id', $column_id);
        $query = $this->db->get('mother_column');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            if ($dat['sort_priority'] != $column['sort_priority']) {
                $this->moveDownColumnPriority($dat['table_id'], $dat['sort_priority']);
                $this->moveUpColumnPriority($dat['table_id'], $column['sort_priority']);
            }
            $table_code = $dat['table_code'];
            if ($dat['column_lang'] == $column['column_lang']) {
                if ($dat['column_lang'] == '1') {
                    $table_code .= '_lang';
                }
                $field_property = $this->fieldTypeModel->getFieldModifyProperty($dat['column_code'], $column);
                $this->TableModel->modifyColumn($table_code, $field_property);
            } else {
                $old_table_code = $dat['table_code'];
                if ($dat['column_lang'] == '1') {
                    $old_table_code .= '_lang';
                }
                $this->TableModel->dropColumn($old_table_code, $dat['column_code']);
                $new_table_code = $query->row()->table_code;
                if ($column['column_lang'] == '1') {
                    $new_table_code .= '_lang';
                }
                $field_property = $this->fieldTypeModel->getFieldProperty($column);
                $this->TableModel->addColumn($new_table_code, $field_property);
            }
            foreach ($column as $key => $value) {
                if ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('column_id', $column_id);
            $this->db->update('mother_column');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found column_id';
            $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
        }
    }

    function resetMainColumn($table_id = 0) {
        $this->db->set('column_main', 'false');
        $this->db->where('table_id', $table_id);
        $this->db->update('mother_column');
    }

    function moveUpColumn($column_id) {
        $action_name = 'move up column';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />column_id : ' . $column_id;
        $action_query = '';
        $this->db->where('column_id', $column_id);
        $query = $this->db->get('mother_column');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $this->moveDownColumnPriority($dat->table_id, $dat->sort_priority);
                $this->moveUpColumnPriority($dat->table_id, $dat->sort_priority - 1);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('table_id', $column_id);
                $this->db->update('mother_column');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found column_id';
            $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDownColumn($column_id) {
        $action_name = 'move down column';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />column_id : ' . $column_id;
        $action_query = '';
        $this->db->where('column_id', $column_id);
        $query = $this->db->get('mother_column');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $max_priority = $this->getMaxColumnPriority($dat->table_id);
            if ($dat->sort_priority < $max_priority) {
                $this->moveDownColumnPriority($dat->table_id, $dat->sort_priority);
                $this->moveUpColumnPriority($dat->table_id, $dat->sort_priority + 1);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('column_id', $column_id);
                $this->db->update('mother_column');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found column_id';
            $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
        }
    }

    function deleteColumn($column_id) {
        $action_name = 'delete column';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />column_id : ' . $column_id;
        $action_query = '';
        $this->db->select('mother_column.*');
        $this->db->select('mother_table.table_code');
        $this->db->join('mother_table', 'mother_table.table_id = mother_column.table_id', 'left');
        $this->db->where('column_id', $column_id);
        $query = $this->db->get('mother_column');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->moveDownColumnPriority($dat->table_id, $dat->sort_priority);
            $table_code = $dat->table_code;
            if ($dat->column_lang == '1') {
                $table_code .= '_lang';
            }
            $this->TableModel->dropColumn($table_code, $dat->column_code);
            $this->db->where('column_id', $column_id);
            $this->db->delete('mother_column');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found column_id';
            $this->LogActionModel->insert('mother_column', $column_id, $action_name, $action_detail, $action_query);
        }
    }

    function getColumnName($column_id) {
        $this->db->where('column_id', $column_id);
        $query = $this->db->get('mother_column');
        return $query->row()->column_name;
    }

    function getMaxColumnPriority($table_id) {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_column');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpColumnPriority($table_id, $sort_priority) {
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('table_id', $table_id);
        $this->db->update('mother_column');
    }

    function moveDownColumnPriority($table_id, $sort_priority) {
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('table_id', $table_id);
        $this->db->update('mother_column');
    }

    function getRootList() {
        $this->db->where('mother_table.parent_table_id', 0);
        $this->db->order_by('mother_table.sort_priority', 'asc');
        return $this->db->get('mother_table');
    }

    function getColumnList($table_id, $flag_list_page = 0) {
        $this->db->where('table_id', $table_id);
        if ($flag_list_page == 1) {
            $this->db->where('column_show_list', 1);
        }
        $this->db->order_by('sort_priority', 'asc');
        return $this->db->get('mother_column');
    }

    function getSearchColumnList($table_id = 0) {
        $this->db->where('mother_column.table_id', $table_id);
        $this->db->where('mother_column.searchable', 'Normal Search');
        $this->db->or_where('mother_column.searchable', 'Advance Search');
        return $this->db->get('mother_column');
    }

    function getMultilangColumn($table_id) {
        $this->db->where('column_lang', 1);
        $this->db->where('table_id', $table_id);
        return $this->db->get('mother_column');
    }

    function getPreviewUrl($table_id) {
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        return trim($query->row()->table_preview);
    }

    function isRecursive($table_id) {
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        return $query->row()->table_recursive;
    }

}

?>