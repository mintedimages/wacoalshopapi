<?php

class UserGroupModel extends CI_Model {

    private $modelName = 'UserGroupModel';

    function __construct() {
        parent::__construct();
    }

    function insert($userGroup) {
        $action_name = 'create user group';
        $action_detail = 'model : ' . $this->modelName;
        foreach ($userGroup as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_user_group');
        $action_query = $this->db->last_query();
        $this->db->select_max('user_group_id', 'max_id');
        $max_id = $this->db->get('mother_user_group')->row()->max_id;
        $this->LogActionModel->insert('mother_user_group', $max_id, $action_name, $action_detail, $action_query);
        return $max_id;
    }

    function update($user_group_id, $userGroup) {
        $action_name = 'update User Group';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('user_group_id', $user_group_id);
        $query = $this->db->get('mother_user_group');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            $action_detail .= '<br /><b>Update</b>';
            foreach ($userGroup as $key => $value) {
                if ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('user_group_id', $user_group_id);
            $this->db->update('mother_user_group');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_user_group', $user_group_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found user_group_id';
            $this->LogActionModel->insert('mother_user_group', $user_group_id, $action_name, $action_detail, $action_query);
        }
    }

    function delete($user_group_id) {
        $action_name = 'delete User Group';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />user_group_id : ' . $user_group_id;
        $action_query = '';
        $this->db->where('user_group_id', $user_group_id);
        $query = $this->db->get('mother_user_group');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->UserGroupModel->deletePermission($user_group_id);

            $this->db->where('user_group_id', $user_group_id);
            $this->db->delete('mother_user_group');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_user_group', $user_group_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found user_group_id';
            $this->LogActionModel->insert('mother_user_group', $user_group_id, $action_name, $action_detail, $action_query);
        }
    }

    function insertPermission($user_group_id, $shop_id, $table_id, $view, $edit) {
        $this->db->set('user_group_id', $user_group_id);
        $this->db->set('shop_id', $shop_id);
        $this->db->set('table_id', $table_id);
        $this->db->set('view', $view);
        $this->db->set('edit', $edit);
        $this->db->insert('mother_permission');
    }

    function deletePermission($user_group_id) {
        $this->db->where('user_group_id', $user_group_id);
        $this->db->delete('mother_permission');
    }

    function getUserGroupNameByGroupId($user_group_id = 0) {
        $this->db->where('user_group_id', $user_group_id);
        $query = $this->db->get('mother_user_group');
        $row = $query->row();
        return $row->user_group_name;
    }

}

?>