<?php

class TableModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    function createTable($table_name) {
        $fields = array(
            $table_name . '_id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'mother_shop_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'default' => 1
            ),
            'parent_id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'default' => 0
            ),
            'recursive_id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'default' => 0
            ),
            'sort_priority' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE
            ),
            'enable_status' => array(
                'type' => "ENUM",
                'constraint' => "'show','hide','delete'",
                'default' => 'show',
            ),
            'create_date' => array(
                'type' => 'DATETIME'
            ),
            'create_by' => array(
                'type' => 'INT'
            ),
            'update_date' => array(
                'type' => 'DATETIME'
            ),
            'update_by' => array(
                'type' => 'INT'
            )
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key($table_name . '_id', TRUE);
        $this->dbforge->create_table('tbl_' . $table_name, TRUE);
        $langFields = array(
            $table_name . '_lang_id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            $table_name . '_id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE
            ),
            'lang_id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'default' => 0
            )
        );
        $this->dbforge->add_field($langFields);
        $this->dbforge->add_key($table_name . '_lang_id', TRUE);
        $this->dbforge->create_table('tbl_' . $table_name . '_lang', TRUE);
    }

    function dropTable($table_name) {
        $this->dbforge->drop_table('tbl_' . $table_name);
        $this->dbforge->drop_table('tbl_' . $table_name . '_lang');
    }

    function renameTable($old_name, $new_name) {
        $this->dbforge->rename_table('tbl_' . $old_name, 'tbl_' . $new_name);
        $this->dbforge->rename_table('tbl_' . $old_name . '_lang', 'tbl_' . $new_name . '_lang');
        $this->dbforge->modify_column('tbl_' . $new_name, array($old_name . '_id' => array('name' => $new_name . '_id', 'type' => 'BIGINT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE)));
        $this->dbforge->modify_column('tbl_' . $new_name . '_lang', array($old_name . '_lang_id' => array('name' => $new_name . '_lang_id', 'type' => 'BIGINT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE)));
        $this->dbforge->modify_column('tbl_' . $new_name . '_lang', array($old_name . '_id' => array('name' => $new_name . '_id', 'type' => 'BIGINT', 'unsigned' => TRUE)));
    }

    function addColumn($table_name, $field_property) {
        $this->dbforge->add_column('tbl_' . $table_name, $field_property);
    }

    function dropColumn($table_name, $column_name) {
        $this->dbforge->drop_column('tbl_' . $table_name, $column_name);
    }

    public function modifyColumn($table_name, $field_property) {
        $this->dbforge->modify_column('tbl_' . $table_name, $field_property);
    }

    function loadRelateTable() {
        $this->db->where('table_type', 'dynamic');
        $this->db->order_by('sort_priority');
        return $this->db->get('mother_table');
    }

    function queryTable() {
        $this->db->order_by('sort_priority');
        return $this->db->get('mother_table');
    }

}

?>