<?php

class ShopModel extends CI_Model {

    private $modelName = 'ShopModel';

    function __construct() {
        parent::__construct();
    }

    function insert($shop) {
        $action_name = 'create shop';
        $action_detail = 'model : ' . $this->modelName;
        $this->moveUpPriority($shop['sort_priority']);
        foreach ($shop as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_shop');
        $action_query = $this->db->last_query();
        $this->db->select_max('shop_id', 'max_id');
        $max_id = $this->db->get('mother_shop')->row()->max_id;
        $this->LogActionModel->insert('mother_shop', $max_id, $action_name, $action_detail, $action_query);
        return $max_id;
    }

    function update($shop_id, $shop) {
        $action_name = 'update shop';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            //$this->moveDownPriority($dat['sort_priority']);
            //$this->moveUpPriority($shop['sort_priority']);
            $action_detail .= '<br /><b>Update</b>';
            foreach ($shop as $key => $value) {
                if ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('shop_id', $shop_id);
            $this->db->update('mother_shop');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found shop_id';
            $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveUp($shop_id) {
        $action_name = 'move up shop';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />shop_id : ' . $shop_id;
        $action_query = '';
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $this->moveDownPriority($dat->sort_priority);
                $this->moveUpPriority($dat->sort_priority - 1);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('shop_id', $shop_id);
                $this->db->update('mother_shop');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found shop_id';
            $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDown($shop_id) {
        $action_name = 'move down shop';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />shop_id : ' . $shop_id;
        $action_query = '';
        $max_priority = $this->getMaxPriority();
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority <= $max_priority) {
                $this->moveDownPriority($dat->sort_priority);
                $this->moveUpPriority($dat->sort_priority + 1);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('shop_id', $shop_id);
                $this->db->update('mother_shop');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found shop_id';
            $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
        }
    }

    function delete($shop_id) {
        $action_name = 'delete shop';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />shop_id : ' . $shop_id;
        $action_query = '';
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->moveDownPriority($dat->sort_priority);
            $this->db->where('shop_id', $shop_id);
            $this->db->delete('mother_shop');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found shop_id';
            $this->LogActionModel->insert('mother_shop', $shop_id, $action_name, $action_detail, $action_query);
        }
    }

    function queryShopList() {
        $this->db->order_by('mother_shop.sort_priority', 'asc');
        return $this->db->get('mother_shop');
    }

    function getShop($shop_id) {
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        return $query->row();
    }

    function getShopName($shop_id) {
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        return $query->row()->shop_name;
    }

    function getMaxPriority() {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $query = $this->db->get('mother_shop');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_shop');
    }

    function moveDownPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_shop');
    }

    function getDefaultShop() {
        $this->db->limit(1, 0);
        $this->db->order_by('sort_priority', 'ASC');
        $query = $this->db->get('mother_shop');
        return $query->row();
    }

}

?>