<?php

class ConfigGroupModel extends CI_Model {

    private $modelName = 'ConfigGroupModel';

    function __construct() {
        parent::__construct();
    }

    function insertConfigGroup($config) {
        $action_name = 'create config group0';
        $action_detail = 'model : ' . $this->modelName;
        $this->moveUpConfigPriority($config['sort_priority']);
        foreach ($config as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_config_group');
        $action_query = $this->db->last_query();
        $this->db->select_max('config_group_id', 'max_id');
        $max_id = $this->db->get('mother_config_group')->row()->max_id;
        $this->LogActionModel->insert('mother_config_group', $max_id, $action_name, $action_detail, $action_query);
        return $max_id;
    }

    function updateConfigGroup($config_id, $config) {
        $action_name = 'update table';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            $this->moveDownConfigPriority($dat['sort_priority']);
            $this->moveUpConfigPriority($config['sort_priority']);
            $action_detail .= '<br /><b>Update</b>';

            foreach ($config as $key => $value) {
                if ($config != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('config_group_id', $config_id);
            $this->db->update('mother_config_group');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveUpConfigGroup($config_id) {
        $action_name = 'move up config';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />config_id : ' . $config_id;
        $action_query = '';
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $this->moveDownConfigPriority($dat->sort_priority);
                $this->moveUpConfigPriority($dat->sort_priority - 1);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('config_id', $config_id);
                $this->db->update('mother_config');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDownConfigGroup($config_id) {
        $action_name = 'move down config';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />config_id : ' . $config_id;
        $action_query = '';
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $max_priority = $this->getMaxConfigPriority(0);
            if ($dat->sort_priority < $max_priority) {
                $this->moveDownConfigPriority($dat->sort_priority);
                $this->moveUpConfigPriority($dat->sort_priority + 1);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('config_id', $config_id);
                $this->db->update('mother_config');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function deleteConfigGroup($config_id) {
        $action_name = 'delete config';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />config_id : ' . $config_id;
        $action_query = '';
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->moveDownConfigPriority(0, $dat->sort_priority);
            $this->db->where('config_id', $config_id);
            $this->db->delete('mother_config');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function getMaxConfigPriority() {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpConfigPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_config');
    }

    function moveDownConfigPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_config');
    }

    function getMaxConfigGroupPriority() {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $query = $this->db->get('mother_config_group');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function getRowList() {
        $this->db->select('mother_config_group.*');
        $this->db->order_by('sort_priority');
        $query = $this->db->get('mother_config_group');
        foreach ($query->result() AS $row) {
            $dataContent['row'] = $row;
            echo $this->load->view('structure/config/rowlist_group', $dataContent, true);
        }
    }

    function getQuery() {
        $this->db->select('mother_config_group.*');
        $this->db->order_by('sort_priority');
        $query = $this->db->get('mother_config_group');
        return $query;
    }

    function getConfigGroupName($config_group_id) {
        $this->db->where('config_group_id', $config_group_id);
        $query = $this->db->get('mother_config_group');
        $row = $query->row_array();
        return $row['config_group_name'];
    }

    function getFirstConfigGroupId() {
        $this->db->order_by('sort_priority');
        $this->db->limit(1);
        $query = $this->db->get('mother_config_group');
        $row = $query->row_array();
        return $row['config_group_id'];
    }

}

?>
