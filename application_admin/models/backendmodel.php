<?php

class BackendModel extends CI_Model {

    private $modelName = 'BackendModel';

    function __construct() {
        parent::__construct();
        $this->load->model(array('StructureModel', 'fieldTypeModel'));
    }

    function getTableCode($table_id) {
        $this->db->where('mother_table.table_id', $table_id);
        $query = $this->db->get('mother_table');
        return $query->row()->table_code;
    }

    function getCountContentList($table_id = 0, $parent_id = 0, $query_array = array(), $mother_shop_id = 0) {
        $table_code = $this->getTableCode($table_id);
        //getter query_array
        $strWhere = "";
        if (count($query_array) > 0) {
            $getSearchColumnList = $this->StructureModel->getSearchColumnList($table_id);
            if ($getSearchColumnList->num_rows() > 0) {
                foreach ($getSearchColumnList->result_array() AS $row) {
                    if ($row['column_lang'] > 0) {
                        $queryLang = $this->LangModel->queryLangName();
                        foreach ($queryLang->result_array() AS $row2) {
                            $strWhere .= $this->FieldTypeModel->getFieldSearch($row, $row2, 'tbl_' . $table_code . '.');
                        }
                    } else {
                        $strWhere .= $this->FieldTypeModel->getFieldSearch($row, null, 'tbl_' . $table_code . '.');
                    }
                }
            }
        }

        $this->db->select('tbl_' . $table_code . '.' . $table_code . '_id');
        if ($parent_id != 0) {
            $this->db->where('parent_id', $parent_id);
        }

        if (count($query_array) > 0 && $strWhere != '') {
            $this->db->where(substr($strWhere, 4), NULL, FALSE);
        }
        if ($mother_shop_id == 0) {
            $mother_shop_id = $this->session->userdata('mother_shop_id');
        }
        $this->db->where('tbl_' . $table_code . '.mother_shop_id', $mother_shop_id);
        $this->db->where('tbl_' . $table_code . '.enable_status !=', 'delete');
        $query = $this->db->get('tbl_' . $table_code);
        return $query->num_rows();
    }

    function getContentList($table_id = 0, $parent_id = 0, $per_page = 0, $page = 1, $query_array = array(), $mother_shop_id = 0, $recursive_id = 0) {
        if ($mother_shop_id == 0) {
            $mother_shop_id = $this->session->userdata('mother_shop_id');
        }
        $table_code = $this->getTableCode($table_id);

        //getter query_array
        $strWhere = "";
        if (count($query_array) > 0) {
            $getSearchColumnList = $this->StructureModel->getSearchColumnList($table_id);
            if ($getSearchColumnList->num_rows() > 0) {
                foreach ($getSearchColumnList->result_array() AS $row) {
                    if ($row['column_lang'] > 0) {
                        $queryLang = $this->LangModel->queryLangName();
                        foreach ($queryLang->result_array() AS $row2) {
                            $strWhere .= $this->FieldTypeModel->getFieldSearch($row, $row2, 'tbl_' . $table_code . '.');
                        }
                    } else {
                        $strWhere .= $this->FieldTypeModel->getFieldSearch($row, null, 'tbl_' . $table_code . '.');
                    }
                }
            }
        }

        $defaultLang = $this->LangModel->getDefaultLang();
        $this->db->select('tbl_' . $table_code . '_lang.*');
        $this->db->select('tbl_' . $table_code . '.*');
        $this->db->join('tbl_' . $table_code . '_lang', 'tbl_' . $table_code . '_lang.' . $table_code . '_id = tbl_' . $table_code . '.' . $table_code . '_id AND lang_id = ' . $defaultLang->lang_id, 'left');
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = tbl_' . $table_code . '.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = tbl_' . $table_code . '.update_by', 'left');
        $this->db->where('tbl_' . $table_code . '.enable_status !=', 'delete');
        $this->db->where('tbl_' . $table_code . '.mother_shop_id', $mother_shop_id);
        $this->db->where('recursive_id', $recursive_id);
        if ($parent_id != 0) {
            $this->db->where('parent_id', $parent_id);
        }

        if ($strWhere != '') {
            $this->db->where(substr($strWhere, 4), NULL, FALSE);
        }

        $this->db->order_by('tbl_' . $table_code . '.sort_priority asc, tbl_' . $table_code . '.' . $table_code . '_id asc');
        if ($per_page > 0) {
            //check per page if $per_page == 0 is unlimit
            $this->db->limit($per_page, ($page * $per_page) - $per_page);
        }
        $query = $this->db->get('tbl_' . $table_code);
        return $query;
    }

    function showRecursiveList($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id, $level) {
        $query = $this->getContentList($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id);
        $table_code = $this->getTableCode($table_id);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() AS $row) {
                $dataQuery['row'] = $row;
                $dataQuery['level'] = $level;
                echo $this->load->view('backend/recursive_list_table', $dataQuery, true);
                $recursive_id = $row[$table_code . '_id'];
                $this->showRecursiveList($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id, $level + 1);
            }
        }
    }
    
    function showRecursiveDropdown($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id, $level) {
        $query = $this->getContentList($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id);
        $table_code = $this->getTableCode($table_id);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() AS $row) {
                $dataQuery['row'] = $row;
                $dataQuery['level'] = $level;
                echo $this->load->view('backend/recursive_dropdown', $dataQuery, true);
                $recursive_id = $row[$table_code . '_id'];
                $this->showRecursiveDropdown($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id, $level + 1);
            }
        }
    }
    
    function showRecursiveFieldtypeDropdown($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id, $level) {
        $query = $this->getContentList($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id);
        $table_code = $this->getTableCode($table_id);
        $relateTable = $this->StructureModel->getTable($table_id);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() AS $row) {
                $dataQuery['table_id'] = $table_id;
                $dataQuery['row'] = $row;
                $dataQuery['level'] = $level;
                $dataQuery['relateTable'] = $relateTable;
                //echo $row[$relateTable->table_code . '_id'].'</br>';
                echo $this->load->view('fieldtype/recursive_dropdown', $dataQuery, true);
                $recursive_id = $row[$table_code . '_id'];
                $this->showRecursiveFieldtypeDropdown($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id, $recursive_id, $level + 1);
            }
        }
    }

    function showRecursiveExcelList($table_id, $parent_id, $recursive_id, $objPHPExcel, $num_row) {
        $query = $this->getContentListAllLang($table_id, $parent_id, $recursive_id);
        $table_code = $this->getTableCode($table_id);
        $query_lang = $this->LangModel->queryLangName();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() AS $row) {
                // save parameter
                $id = $row[$this->BackendModel->getTableCode($table_id) . '_id'];
                $mother_shop_id = $row['mother_shop_id'];
                $recursive_id = $row['recursive_id'];
                $parent_id = $row['parent_id'];
                $sort_priority = $row['sort_priority'];
                $enable_status = $row['enable_status'];
                $create_date = $row['create_date'];
                $create_by = $row['create_by'];
                $update_date = $row['update_date'];
                $update_by = $row['update_by'];
                //run code excel  inset data
                $num_col = 65;
                //$num_row = $this->count_excel_row++;
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $id);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $mother_shop_id);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $parent_id);
                $columns = $this->StructureModel->getColumnList($table_id, 0);
                foreach ($columns->result() AS $column) {
                    $content = '-';
                    if ($column->column_lang == 0) {
                        if (isset($row[$column->column_code]) && $row[$column->column_code] != '') {
                            $content = $row[$column->column_code];
                        }
                        $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $content);
                    } else {
                        foreach ($query_lang->result() as $row_lang) {
                            if (isset($row[$column->column_code . '_' . $row_lang->lang_code]) && $row[$column->column_code . '_' . $row_lang->lang_code] != '') {
                                $content = $row[$column->column_code . '_' . $row_lang->lang_code];
                            }
                            $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $content);
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $recursive_id);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $sort_priority);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $enable_status);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $create_date);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $create_by);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $update_date);
                $objPHPExcel->getActiveSheet()->SetCellValue(chr($num_col++) . (string) ($num_row), $update_by);

                $recursive_id = $row[$table_code . '_id'];
                $num_row++;
                $num_row = $this->showRecursiveExcelList($table_id, $parent_id, $recursive_id, $objPHPExcel, $num_row);
            }
        }
        return $num_row;
    }

    /*
      function showContentList($data) {
      $table_id = $data['table_id'];
      $parent_id = $data['parent_id'];
      $per_page = $data['per_page'];
      $page = $data['page'];
      $query_array = $data['query_array'];
      $mother_shop_id = $data['mother_shop_id'];
      if ($query->num_rows() > 0) {
      foreach ($query->result_array() AS $row) {
      $dataContent['row'] = $row;
      echo $this->load->view('backend/rowlist', $dataContent, true);
      $this->getContentList($table_id, $parent_id, $per_page, $page, $query_array, $mother_shop_id);
      }
      }
      }
     */

      function getContentListAllLang($table_id, $parent_id, $recursive_id = 0) {
        $table_code = $this->getTableCode($table_id);
        $lang_list = $this->LangModel->queryLangName();
        $lang_column_list = $this->StructureModel->getMultilangColumn($table_id);
        $this->db->select('tbl_' . $table_code . '.*');
        foreach ($lang_list->result() as $lang) {
            foreach ($lang_column_list->result() as $lang_column) {
                $this->db->select('tbl_' . $lang->lang_code . '.' . $lang_column->column_code . ' AS ' . $lang_column->column_code . '_' . $lang->lang_code);
            }
            $this->db->join('tbl_' . $table_code . '_lang AS tbl_' . $lang->lang_code, 'tbl_' . $lang->lang_code . '.' . $table_code . '_id = tbl_' . $table_code . '.' . $table_code . '_id AND tbl_' . $lang->lang_code . '.lang_id = ' . $lang->lang_id, 'left');
        }
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = tbl_' . $table_code . '.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = tbl_' . $table_code . '.update_by', 'left');
        $this->db->where('tbl_' . $table_code . '.enable_status !=', 'delete');
        $this->db->where('tbl_' . $table_code . '.parent_id', $parent_id);
        if($recursive_id >= 0){
            $this->db->where('tbl_' . $table_code . '.recursive_id', $recursive_id);
        }
        $this->db->where('tbl_' . $table_code . '.mother_shop_id', $this->session->userdata('mother_shop_id'));
        $this->db->order_by('tbl_' . $table_code . '.sort_priority', 'asc');
        return $this->db->get('tbl_' . $table_code);
    }

    function getContentDetail($table_id, $content_id) {
        $table_code = $this->getTableCode($table_id);
        $lang_list = $this->LangModel->queryLangName();
        $lang_column_list = $this->StructureModel->getMultilangColumn($table_id);
        $this->db->select('tbl_' . $table_code . '.*');
        foreach ($lang_list->result() as $lang) {
            foreach ($lang_column_list->result() as $lang_column) {
                $this->db->select('tbl_' . $lang->lang_code . '.' . $lang_column->column_code . ' AS ' . $lang_column->column_code . '_' . $lang->lang_code);
            }
            $this->db->join('tbl_' . $table_code . '_lang AS tbl_' . $lang->lang_code, 'tbl_' . $lang->lang_code . '.' . $table_code . '_id = tbl_' . $table_code . '.' . $table_code . '_id AND tbl_' . $lang->lang_code . '.lang_id = ' . $lang->lang_id, 'left');
        }
        $this->db->select('tbl_create.user_login_name AS create_name');
        $this->db->join('mother_user as tbl_create', 'tbl_create.user_id = tbl_' . $table_code . '.create_by', 'left');
        $this->db->select('tbl_update.user_login_name AS update_name');
        $this->db->join('mother_user as tbl_update', 'tbl_update.user_id = tbl_' . $table_code . '.update_by', 'left');
        $this->db->where('tbl_' . $table_code . '.enable_status !=', 'delete');
        $this->db->where('tbl_' . $table_code . '.' . $table_code . '_id', $content_id);
        $this->db->where('tbl_' . $table_code . '.mother_shop_id', $this->session->userdata('mother_shop_id'));
        return $this->db->get('tbl_' . $table_code)->row_array();
    }

    function insert($table_id, $parent_id, $content) {
        $table = $this->StructureModel->getTable($table_id);
        $action_name = 'insert ' . $table->table_name;
        $action_detail = 'model : ' . $this->modelName;
        if ($table->table_type != 'static') {
            $this->moveUpPriority($table_id, $parent_id, $content['sort_priority'], $content['recursive_id']);
        }
        foreach ($content as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->set('mother_shop_id', $this->session->userdata('mother_shop_id'));
        $this->db->insert('tbl_' . $table->table_code);
        $action_query = $this->db->last_query();
        $max_id = $this->getLastId($table_id);
        
        $this->LogActionModel->insert('tbl_' . $table->table_code, $max_id, $action_name, $action_detail, $action_query);
        return $max_id;
    }

    function updateLang($table_id, $content_id, $contentLang) {
        $table = $this->StructureModel->getTable($table_id);
        $action_name = 'insert ' . $table->table_name . '_lang';
        $action_detail = 'model : ' . $this->modelName;
        $queryLang = $this->LangModel->queryLangName();
        foreach ($queryLang->result() AS $row) {
            $content_lang_id = 0;
            $this->db->where($table->table_code . '_id', $content_id);
            $this->db->where('lang_id', $row->lang_id);
            $queryContentLang = $this->db->get('tbl_' . $table->table_code . '_lang');
            if ($queryContentLang->num_rows() > 0) {
                $oldContentLang = $queryContentLang->row_array();
                $content_lang_id = $oldContentLang[$table->table_code . '_lang_id'];
            }
            if(count($contentLang) > 0){
                foreach ($contentLang[$row->lang_id] as $key => $value) {
                    $action_detail .= '<br />' . $key . ' : ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set($table->table_code . '_id', $content_id);
            $this->db->set('lang_id', $row->lang_id);
            if ($content_lang_id == 0) {
                $this->db->insert('tbl_' . $table->table_code . '_lang');
                $action_query = $this->db->last_query();
                $this->db->select_max($table->table_code . '_id', 'max_id');
                $content_lang_id = $this->db->get('tbl_' . $table->table_code)->row()->max_id;
            } else {
                $this->db->where($table->table_code . '_lang_id', $content_lang_id);
                $this->db->update('tbl_' . $table->table_code . '_lang');
                $action_query = $this->db->last_query();
            }
            $this->LogActionModel->insert('tbl_' . $table->table_code . '_lang', $content_lang_id, $action_name, $action_detail, $action_query);
        }
        return true;
    }

    function update($table_id, $content_id, $content, $recursive_id) {
        $action_query = '';
        $table = $this->StructureModel->getTable($table_id);
        $action_name = 'update ' . $table->table_name;
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where($table->table_code . '_id', $content_id);
        $query = $this->db->get('tbl_' . $table->table_code);
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            $parent_id = 0;
            if ($table->parent_table_id != 0) {
                $parent_id = $dat['parent_id'];
            }
            if ($table->table_type != 'static') {
                $this->moveDownPriority($table_id, $parent_id, $dat['sort_priority'], $recursive_id);
                $this->moveUpPriority($table_id, $parent_id, $content['sort_priority'], $recursive_id);
            }
            $action_detail .= '<br /><b>Update</b>';
            foreach ($content as $key => $value) {
                if ($key == 'sort_priority') {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                } elseif ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where($table->table_code . '_id', $content_id);
            $this->db->update('tbl_' . $table->table_code);
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found ' . $table->table_code . '_id';
            $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveUp($table_id, $content_id) {
        $table = $this->StructureModel->getTable($table_id);
        $action_name = 'move up ' . $table->table_name;
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />' . $table->table_code . '_id : ' . $content_id;
        $action_query = '';
        $this->db->where($table->table_code . '_id', $content_id);
        $query = $this->db->get('tbl_' . $table->table_code);
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $parent_id = 0;
                if ($table->parent_table_id != 0) {
                    $parent_id = $dat->parent_id;
                }
                $recursive_id = $dat->recursive_id;
                $this->moveDownPriority($table_id, $parent_id, $dat->sort_priority, $recursive_id);
                $this->moveUpPriority($table_id, $parent_id, $dat->sort_priority - 1, $recursive_id);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where($table->table_code . '_id', $content_id);
                $this->db->where('mother_shop_id', $this->session->userdata('mother_shop_id'));
                $this->db->where('recursive_id', $recursive_id);
                $this->db->update('tbl_' . $table->table_code);
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found ' . $table->table_code . '_id';
            $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDown($table_id, $content_id) {
        $table = $this->StructureModel->getTable($table_id);
        $action_name = 'move down ' . $table->table_name;
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />' . $table->table_code . '_id : ' . $content_id;
        $action_query = '';
        $this->db->where($table->table_code . '_id', $content_id);
        $query = $this->db->get('tbl_' . $table->table_code);
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $parent_id = 0;
            if ($table->parent_table_id != 0) {
                $parent_id = $dat->parent_id;
            }
            $recursive_id = $dat->recursive_id;
            $max_priority = $this->getMaxPriority($table_id, $parent_id, $recursive_id);
            if ($dat->sort_priority < $max_priority) {
                $this->moveDownPriority($table_id, $parent_id, $dat->sort_priority, $recursive_id);
                $this->moveUpPriority($table_id, $parent_id, $dat->sort_priority + 1, $recursive_id);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where($table->table_code . '_id', $content_id);
                $this->db->where('mother_shop_id', $this->session->userdata('mother_shop_id'));
                $this->db->where('recursive_id', $recursive_id);
                $this->db->update('tbl_' . $table->table_code);
                $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found ' . $table->table_code . '_id';
            $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
        }
    }

    function delete($table_id, $content_id) {
        $table = $this->StructureModel->getTable($table_id);
        $action_name = 'delete ' . $table->table_name;
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />' . $table->table_code . '_id : ' . $content_id;
        $action_query = '';
        $this->db->where($table->table_code . '_id', $content_id);
        $query = $this->db->get('tbl_' . $table->table_code);
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $parent_id = 0;
            if ($table->parent_table_id != 0) {
                $parent_id = $dat->parent_id;
            }
            $recursive_id = $dat->recursive_id;
            $this->moveDownPriority($table_id, $parent_id, $dat->sort_priority, $recursive_id);
            $this->db->set('enable_status', 'delete');
            $this->db->where($table->table_code . '_id', $content_id);
            $this->db->update('tbl_' . $table->table_code);
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found ' . $table->table_code . '_id';
            $this->LogActionModel->insert('tbl_' . $table->table_code, $content_id, $action_name, $action_detail, $action_query);
        }
    }

    function getMaxPriority($table_id, $parent_id, $recursive_id) {
        $table = $this->StructureModel->getTable($table_id);
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        if ($table->parent_table_id != 0) {
            $this->db->where('parent_id', $parent_id);
        }
        $this->db->where('recursive_id', $recursive_id);
        $this->db->where('mother_shop_id', $this->session->userdata('mother_shop_id'));
        $query = $this->db->get('tbl_' . $table->table_code);
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpPriority($table_id, $parent_id, $sort_priority, $recursive_id) {
        $table = $this->StructureModel->getTable($table_id);
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('recursive_id', $recursive_id);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('mother_shop_id', $this->session->userdata('mother_shop_id'));
        if ($table->parent_table_id != 0) {
            $this->db->where('parent_id', $parent_id);
        }
        $this->db->update('tbl_' . $table->table_code);
    }

    function moveDownPriority($table_id, $parent_id, $sort_priority, $recursive_id) {
        $table = $this->StructureModel->getTable($table_id);
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('recursive_id', $recursive_id);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('sort_priority >', 0);
        $this->db->where('mother_shop_id', $this->session->userdata('mother_shop_id'));
        if ($table->parent_table_id != 0) {
            $this->db->where('parent_id', $parent_id);
        }
        $this->db->update('tbl_' . $table->table_code);
    }

    function getParentId($table_id, $content_id) {
        $table = $this->StructureModel->getTable($table_id);
        $parent_id = 0;
        if ($table->parent_table_id != 0) {
            $this->db->select('parent_id');
            $this->db->where($table->table_code . '_id', $content_id);
            $query = $this->db->get('tbl_' . $table->table_code);
            $dat = $query->row();
            $parent_id = $dat->parent_id;
        }
        return $parent_id;
    }

    function getFirstField($table_id = 0, $content_id = 0) {
        $table_code = $this->getTableCode($table_id);
        $defaultLang = $this->LangModel->getDefaultLang();
        $this->db->where('column_main', 'true');
        $this->db->where('table_id', $table_id);
        $this->db->order_by('sort_priority');
        $column = $this->db->get('mother_column')->row();
        if ($column->column_lang == 1) {
            $this->db->select('tbl_' . $table_code . '_lang.*');
            $this->db->where('tbl_' . $table_code . '_lang.lang_id', $defaultLang->lang_id);
            $this->db->where('tbl_' . $table_code . '_lang.' . $table_code . '_id', $content_id);
            $row = $this->db->get('tbl_' . $table_code . '_lang')->row_array();
            return $row[$column->column_code];
        } else {
            $this->db->select('tbl_' . $table_code . '.*');
            $this->db->where('tbl_' . $table_code . '.enable_status !=', 'delete');
            $this->db->where('tbl_' . $table_code . '.' . $table_code . '_id', $content_id);
            $row = $this->db->get('tbl_' . $table_code)->row_array();
            if (isset($row[$column->column_code])) {
                return $row[$column->column_code];
            }
            else {
                return '';
            }
        }
    }

    function createBreadCrumb($table_id = 0, $content_id = 0, $parent_id = 0, $isDetail = 0, $breadCrumbStr = '') {
        $table = $this->StructureModel->getTable($table_id);
        $table_name = $table->table_name;
        $parent_table_id = $table->parent_table_id;
        if ($isDetail == 1 && $content_id != 0) {
            $parent_id = $this->getParentId($table_id, $content_id);
        }
        $newBreadCrumbStr = '';
        if ($table->table_type == 'static') {
            $newBreadCrumbStr .= '<li class=""> ';
            $newBreadCrumbStr .= anchor('backend/form/' . $table_id . '/' . $content_id . '/', $table_name);
            $newBreadCrumbStr .= ' </li>';
            if ($breadCrumbStr != '') {
                $newBreadCrumbStr .= ' <span class="divider"> / </span> ';
            }
        } else {
            $newBreadCrumbStr .= '<li class=""> ';
            $newBreadCrumbStr .= anchor('backend/index/' . $table_id . '/' . $parent_id . '/', $table_name);
            $newBreadCrumbStr .= ' </li>';
            if ($isDetail == 1) {
                $newBreadCrumbStr .= ' <span class="divider"> / </span> ';
                $title_name = ($content_id != 0) ? $this->getFirstField($table_id, $content_id) : 'Create ' . $table_name;
                $newBreadCrumbStr .= '<li class=""> ';
                $newBreadCrumbStr .= anchor('backend/form/' . $table_id . '/' . $content_id . '/', $title_name);
                $newBreadCrumbStr .= ' </li>';
                if ($breadCrumbStr != '') {
                    $newBreadCrumbStr .= ' <span class="divider"> / </span> ';
                }
            }
        }
        $breadCrumbStr = $newBreadCrumbStr . $breadCrumbStr;
        if ($parent_table_id > 0) {
            $breadCrumbStr = $this->BackendModel->createBreadCrumb($parent_table_id, $parent_id, 0, 1, $breadCrumbStr);
        }
        return $breadCrumbStr;
    }

    function getLastId($table_id) {
        $table = $this->StructureModel->getTable($table_id);
        $this->db->select_max($table->table_code . '_id', 'max_id');
        $query = $this->db->get('tbl_' . $table->table_code);
        if ($query->num_rows() > 0) {
            return $query->row()->max_id;
        } else {
            return 0;
        }
    }

}

?>