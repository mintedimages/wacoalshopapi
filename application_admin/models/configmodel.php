<?php

class ConfigModel extends CI_Model {

    private $modelName = 'ConfigModel';

    function __construct() {
        parent::__construct();
        $this->load->model(array('fieldTypeModel'));
    }

    function insertConfig($config) {
        $action_name = 'create config';
        $action_detail = 'model : ' . $this->modelName;
        $this->moveUpConfigPriority($config['sort_priority'], $config['config_group_id']);
        foreach ($config as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_config');
        $action_query = $this->db->last_query();
        $this->db->select_max('config_id', 'max_id');
        $max_id = $this->db->get('mother_config')->row()->max_id;
        $this->LogActionModel->insert('mother_config', $max_id, $action_name, $action_detail, $action_query);
        return $max_id;
    }

    function updateConfig($config_id, $config) {
        $action_name = 'update table';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            $this->moveDownConfigPriority($dat['sort_priority'], $config['config_group_id']);
            $this->moveUpConfigPriority($config['sort_priority'], $config['config_group_id']);
            $action_detail .= '<br /><b>Update</b>';

            foreach ($config as $key => $value) {
                if ($config != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('config_id', $config_id);
            $this->db->update('mother_config');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveUpConfig($config_id, $config_group_id) {
        $action_name = 'move up config';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />config_id : ' . $config_id;
        $action_query = '';
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $this->moveDownConfigPriority($dat->sort_priority, $config_group_id);
                $this->moveUpConfigPriority($dat->sort_priority - 1, $config_group_id);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('config_id', $config_id);
                $this->db->update('mother_config');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDownConfig($config_id, $config_group_id) {
        $action_name = 'move down config';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />config_id : ' . $config_id;
        $action_query = '';
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $max_priority = $this->getMaxConfigPriority($config_group_id);
            if ($dat->sort_priority < $max_priority) {
                $this->moveDownConfigPriority($dat->sort_priority, $config_group_id);
                $this->moveUpConfigPriority($dat->sort_priority + 1, $config_group_id);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('config_id', $config_id);
                $this->db->update('mother_config');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function deleteConfig($config_id, $config_group_id) {
        $action_name = 'delete config';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />config_id : ' . $config_id;
        $action_query = '';
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->moveDownConfigPriority(0, $dat->sort_priority, $config_group_id);
            $this->db->where('config_id', $config_id);
            $this->db->delete('mother_config');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found config_id';
            $this->LogActionModel->insert('mother_config', $config_id, $action_name, $action_detail, $action_query);
        }
    }

    function getMaxConfigPriority($config_group_id) {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $this->db->where('config_group_id', $config_group_id);
        $query = $this->db->get('mother_config');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpConfigPriority($sort_priority, $config_group_id) {
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('config_group_id', $config_group_id);
        $this->db->update('mother_config');
    }

    function moveDownConfigPriority($sort_priority, $config_group_id) {
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->where('config_group_id', $config_group_id);
        $this->db->update('mother_config');
    }

    function getRowList($config_group_id = 0) {
        if (is_numeric($config_group_id) && $config_group_id > 0) {
            $this->db->where('config_group_id', $config_group_id);
        }
        $this->db->select('mother_config.*');
        $this->db->order_by('sort_priority');
        $query = $this->db->get('mother_config');
        foreach ($query->result() AS $row) {
            $dataContent['row'] = $row;
            echo $this->load->view('structure/config/rowlist', $dataContent, true);
        }
    }

    function getColumnList($config_group_id = 0) {
        $this->db->order_by('sort_priority', 'asc');
        if (is_numeric($config_group_id) && $config_group_id > 0) {
            $this->db->where('config_group_id', $config_group_id);
        }
        return $this->db->get('mother_config');
    }

    function displayField($column, $lang, $value) {
        $dataView = array();
        if ($lang != null) {
            $dataView['name'] = $column['config_name'] . ' ' . $lang['lang_name'];
            $dataView['code'] = $column['config_code'] . '_' . $lang['lang_code'];
        } else {
            $dataView['name'] = $column['config_name'];
            $dataView['code'] = $column['config_code'];
        }
        $dataView['value'] = $value;

        if ($column['config_field_type'] == 'checkbox') {
            return $this->load->view('fieldtype/checkbox', $dataView, true);
        } elseif ($column['config_field_type'] == 'color') {
            return $this->load->view('fieldtype/color', $dataView, true);
        } elseif ($column['config_field_type'] == 'date') {
            return $this->load->view('fieldtype/date', $dataView, true);
        } elseif ($column['config_field_type'] == 'dropdown') {
            return $this->load->view('fieldtype/dropdown', $dataView, true);
        } elseif ($column['config_field_type'] == 'email') {
            return $this->load->view('fieldtype/email', $dataView, true);
        } elseif ($column['config_field_type'] == 'file') {
            return $this->load->view('fieldtype/file', $dataView, true);
        } elseif ($column['config_field_type'] == 'image') {
            return $this->load->view('fieldtype/image', $dataView, true);
        } elseif ($column['config_field_type'] == 'number') {
            return $this->load->view('fieldtype/number', $dataView, true);
        } elseif ($column['config_field_type'] == 'radio') {
            return $this->load->view('fieldtype/radio', $dataView, true);
        } elseif ($column['config_field_type'] == 'richtext') {
            return $this->load->view('fieldtype/richtext', $dataView, true);
        } elseif ($column['config_field_type'] == 'text') {
            return $this->load->view('fieldtype/text', $dataView, true);
        } elseif ($column['config_field_type'] == 'textarea') {
            return $this->load->view('fieldtype/textarea', $dataView, true);
        } elseif ($column['config_field_type'] == 'url') {
            return $this->load->view('fieldtype/url', $dataView, true);
        }
    }

    function getConfig($config_code = '') {
        $this->db->where('config_code', $config_code);
        return $this->db->get('mother_config')->row()->config_value;
    }

}
