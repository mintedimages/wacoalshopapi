<?php

class CropImageModel extends CI_Model {

    private $modelName = 'CropImageModel';
    private $jpeg_quality = 90;

    function __construct() {
        parent::__construct();
    }

    function saveCropImage($src, $file_name, $target_w, $target_h, $x, $y, $w, $h) {
        $savePath = '/images/';
        ob_start();
        $this->cropImage($_SERVER['DOCUMENT_ROOT'] . $src, $target_w, $target_h, $x, $y, $w, $h);
        $i = ob_get_clean();
        $saveFile = $savePath . $file_name . '.jpg';
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . $saveFile, 'w');
        fwrite($fp, $i);
        fclose($fp);
        return $saveFile;
    }

    function cropImage($src, $target_w, $target_h, $x, $y, $w, $h) {
        $size = getimagesize($src);
        $img_r = false;
        switch ($size["mime"]) {
            case "image/jpeg":
                $img_r = imagecreatefromjpeg($src); //jpeg file
                break;
            case "image/gif":
                $img_r = imagecreatefromgif($src); //gif file
                break;
            case "image/png":
                $img_r = imagecreatefrompng($src); //png file
                break;
            default:
                $img_r = false;
                break;
        }
        $dst_r = ImageCreateTrueColor($target_w, $target_h);

        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $target_w, $target_h, $w, $h);

        imagejpeg($dst_r, null, $this->jpeg_quality);
    }

}

?>