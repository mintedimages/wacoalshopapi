<?php

class PermissionModel extends CI_Model {

    private $modelName = 'PermissionModel';

    function __construct() {
        parent::__construct();
    }

    function listCategoryPermission($parent_id = 0, $level = 0, $user_group_id = 0, $shop_id = 0) {
        $dash = "";
        $end_dash = "";
        for ($i = 0; $i <= $level; $i++) {
            $dash = '<ul>';
            $end_dash = '</ul>';
        }
        echo $dash;
        if ($parent_id > 0) {
            echo '<li>';
            $checked = $this->PermissionModel->checkEditPermissionSelect($parent_id, $user_group_id, $shop_id);
            echo '<input type="checkbox" name="permission_' . $shop_id . '[]" value="-' . $parent_id . '" ' . $checked . '/> ';
            echo '<i class="icon-pencil"></i> ';
            echo 'Edit';
            echo '</li>';
        }
        $sql = "SELECT * FROM mother_table WHERE parent_table_id = " . (int) $parent_id . " ORDER BY sort_priority, table_id";
        $children = $this->db->query($sql);

        if (!empty($children)) {
            foreach ($children->result_array() as $row) {
                $checked = $this->PermissionModel->checkPermissionSelect($row['table_id'], $user_group_id, $shop_id);

                echo '<li>';
                echo '<input type="checkbox" name="permission_' . $shop_id . '[]" value="' . $row['table_id'] . '" ' . $checked . '/> ';
                if ($this->PermissionModel->countChild($row['table_id']) > 0 && $row['table_type'] == 'static') {
                    echo '<i class="icon-folder-close"></i> ';
                } else if ($this->PermissionModel->countChild($row['table_id']) == 0 && $row['table_type'] == 'static') {
                    echo '<i class="icon-file"></i> ';
                } else {
                    echo '<i class="icon-list"></i> ';
                }
                echo $row['table_name'];
                /*
                  if ($row['type'] == 'static') { */
                $this->PermissionModel->listCategoryPermission($row['table_id'], $level + 1, $user_group_id, $shop_id);
                /* } */
                echo '</li>';
            }
        }
        echo $end_dash;
    }

    function checkNavigatePermission($table_id = 0, $shop_id = 0, $user_group_id = 0) {
        $this->db->where('user_group_id', $user_group_id);
        $this->db->where('shop_id', $shop_id);
        $this->db->where('table_id', $table_id);
        $this->db->where('view', 'true');
        $query = $this->db->get('mother_permission');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function checkEditPermission($table_id = 0, $shop_id = 0, $user_group_id = 0) {
        if ($user_group_id > 0) {
            $this->db->where('user_group_id', $user_group_id);
            $this->db->where('shop_id', $shop_id);
            $this->db->where('table_id', $table_id);
            $this->db->where('edit', 'true');
            $query = $this->db->get('mother_permission');
            if ($query->num_rows() > 0) {
                return 'enabled';
            } else {
                return 'disabled';
            }
        }else{
                return 'enabled';
        }
    }

    function checkPermission($table_id, $user_group_id, $shop_id) {
        /*
          $permission = explode(",", $permission);
          return in_array($id, $permission);
         * 
         */
    }

    function checkPermissionSelect($table_id, $user_group_id, $shop_id) {
        if ($this->checkNavigatePermission($table_id, $shop_id, $user_group_id)) {
            return ' checked="checked"';
        } else {
            return '';
        }
    }

    function checkEditPermissionSelect($table_id, $user_group_id, $shop_id) {
        //return $this->checkPermissionSelect(-$id, $permission);
        if ($this->checkEditPermission($table_id, $shop_id, $user_group_id) == 'enabled') {
            return ' checked="checked"';
        } else {
            return '';
        }
    }

    function checkShopSelect($shop_id = 0, $user_group_id = 0) {
        $this->db->where('shop_id', $shop_id);
        $this->db->where('user_group_id', $user_group_id);
        $this->db->where('view', 'true');
        $query = $this->db->get('mother_permission');
        if ($query->num_rows() > 0) {
            return ' checked="checked"';
        } else {
            return '';
        }
    }

    function countChild($parent_id = 0) {
        $sql = "SELECT * FROM mother_table WHERE parent_table_id = " . (int) $parent_id;
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function getPermissionToString($user_group_id = 0) {
        $this->db->where('user_group_id', $user_group_id);
        $query = $this->db->get('mother_permission');
        $permissionStr = "";
        foreach ($query->result_array() as $row) {
            if ($row['view'] == 'true') {
                $permissionStr .= $row['table_id'] . ',';
            }
            if ($row['edit'] == 'true') {
                $permissionStr .= '-' . $row['table_id'] . ',';
            }
        }
        if ($permissionStr != '') {
            $permissionStr = substr($permissionStr, 0, -1);
        }
        return $permissionStr;
    }

}

?>
