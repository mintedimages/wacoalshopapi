<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of subnavigatemodel
 *
 * @author tomozard
 */
class Subnavigatemodel extends CI_Model {

    function createSubNavigate($content_id = 0, $table_id = 0) {
        $str = '';
        if ($content_id != 0) {
            $children_table = $this->StructureModel->getChildTableList($table_id);
            if ($children_table->num_rows() > 0) {
                $str .= '<ul class="col-nav">';
                foreach ($children_table->result() AS $child_table) {

                    $str .= '<li>';
                    $str .= anchor('backend/index/' . $child_table->table_id . '/' . $content_id, $child_table->table_name . '<span class="label label-inverse pull-right">' . $this->BackendModel->getCountContentList($child_table->table_id, $content_id) . '</span>', array('title' => $child_table->table_name . ' List'));
                    if ($child_table->table_type == 'static') {
                        $str .= $this->createSubNavigate($content_id, $child_table->table_id);
                    }
                    $str .= '</li>';
                }
                $str .= '</ul>';
            }
        }

        return $str;
    }

}
?>
