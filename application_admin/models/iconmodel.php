<?php

class IconModel extends CI_Model {

    private $modelName = 'IconModel';

    function __construct() {
        parent::__construct();
    }

    function insert($icon) {
        $action_name = 'create icon';
        $action_detail = 'model : ' . $this->modelName;
        $this->moveUpPriority($icon['sort_priority']);
        foreach ($icon as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_icon');
        $action_query = $this->db->last_query();
        $this->db->select_max('icon_id', 'max_id');
        $max_id = $this->db->get('mother_icon')->row()->max_id;
        $this->LogActionModel->insert('mother_icon', $max_id, $action_name, $action_detail, $action_query);
        return $max_id;
    }

    function update($icon_id, $icon) {
        $action_name = 'update icon';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('icon_id', $icon_id);
        $query = $this->db->get('mother_icon');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            $this->moveDownPriority($dat['sort_priority']);
            $this->moveUpPriority($icon['sort_priority']);
            $action_detail .= '<br /><b>Update</b>';
            foreach ($icon as $key => $value) {
                if ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('icon_id', $icon_id);
            $this->db->update('mother_icon');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found icon_id';
            $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveUp($icon_id) {
        $action_name = 'move up icon';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />icon_id : ' . $icon_id;
        $action_query = '';
        $this->db->where('icon_id', $icon_id);
        $query = $this->db->get('mother_icon');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority > 1) {
                $this->moveDownPriority($dat->sort_priority);
                $this->moveUpPriority($dat->sort_priority - 1);
                $action_detail .= '<br /><b>Move Up Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority - 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('icon_id', $icon_id);
                $this->db->update('mother_icon');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is lowest';
                $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found icon_id';
            $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
        }
    }

    function moveDown($icon_id) {
        $action_name = 'move down icon';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />icon_id : ' . $icon_id;
        $action_query = '';
        $max_priority = $this->getMaxPriority();
        $this->db->where('icon_id', $icon_id);
        $query = $this->db->get('mother_icon');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            if ($dat->sort_priority <= $max_priority) {
                $this->moveDownPriority($dat->sort_priority);
                $this->moveUpPriority($dat->sort_priority + 1);
                $action_detail .= '<br /><b>Move Down Priority</b>';
                $this->db->set('sort_priority', $dat->sort_priority + 1);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_by', $this->session->userdata('user_id'));
                $this->db->where('icon_id', $icon_id);
                $this->db->update('mother_icon');
                $action_query = $this->db->last_query();
                $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
            } else {
                $action_detail .= '<br />status : priority is highest';
                $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
            }
        } else {
            $action_detail .= '<br />status : not found icon_id';
            $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
        }
    }

    function delete($icon_id) {
        $action_name = 'delete icon';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />icon_id : ' . $icon_id;
        $action_query = '';
        $this->db->where('icon_id', $icon_id);
        $query = $this->db->get('mother_icon');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->moveDownPriority($dat->sort_priority);
            $this->db->where('icon_id', $icon_id);
            $this->db->delete('mother_icon');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found icon_id';
            $this->LogActionModel->insert('mother_icon', $icon_id, $action_name, $action_detail, $action_query);
        }
    }

    function queryIconList() {
        return $this->db->get('mother_icon');
    }

    function getIcon($icon_id) {
        $this->db->where('icon_id', $icon_id);
        $query = $this->db->get('mother_icon');
        return $query->row();
    }

    function getMaxPriority() {
        $retVal = 0;
        $this->db->select('max(sort_priority) AS max_priority');
        $query = $this->db->get('mother_icon');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $retVal = $dat->max_priority;
        }
        return $retVal;
    }

    function moveUpPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority+1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_icon');
    }

    function moveDownPriority($sort_priority) {
        $this->db->set('sort_priority', 'sort_priority-1', false);
        $this->db->where('sort_priority >=', $sort_priority);
        $this->db->update('mother_icon');
    }

    function getDefaultIcon() {
        $this->db->limit(1, 0);
        $query = $this->db->get('mother_icon');
        return $query->row();
    }

}

?>