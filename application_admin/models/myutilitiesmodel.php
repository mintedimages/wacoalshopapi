<?php

class MyUtilitiesModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getNow() {
        $timestamp = now();
        $timezone = 'UM1';
        $daylight_saving = TRUE;
        $timeTh = gmt_to_local($timestamp, $timezone, $daylight_saving);
        return unix_to_human($timeTh, TRUE, 'eu');
    }

    function thisDate() {
        return substr($this->MyUtilitiesModel->getNow(), 0, 10);
    }

    function thisYear() {
        return substr($this->MyUtilitiesModel->getNow(), 0, 4);
    }

    function thisMonth() {
        return substr($this->MyUtilitiesModel->getNow(), 5, 2);
    }

    function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function pagelist($targetpage, $total_page, $page) {
        $pagination = '';
        if ($page <= 0) {
            $page = 1;
        }
        $adjacents = 3;
        $prev = $page - 1;
        //previous page is page - 1
        $next = $page + 1;
        //next page is page + 1
        $lastpage = $total_page;
        //ceil($total_pages / $limit);  //lastpage is = total pages / items per page, rounded up.
        $lpm1 = $lastpage - 1;
        if ($lastpage > 1) {
            $pagination .= '<ul>';
            $pagination .= '<li><a>Page : </a></li>';
            //previous button
            if ($page > 1)
                $pagination .= '<li>' . anchor($targetpage . '/' . $prev, 'Prev') . '</li>';
            //pages
            if ($lastpage < 7 + ($adjacents * 2)) {//not enough pages to bother breaking it up
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination .= '<li class="active">';
                    else
                        $pagination .= '<li>';
                    $pagination .= anchor($targetpage . '/' . $counter, $counter) . '</li>';
                }
            } elseif ($lastpage > 5 + ($adjacents * 2)) {//enough pages to hide some
                //close to beginning; only hide later pages
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $pagination .= '<li class="active">';
                        else
                            $pagination .= '<li>';
                        $pagination .= anchor($targetpage . '/' . $counter, $counter) . '</li>';
                    }
                    $pagination .= '<li>' . anchor('#', '...') . '</li>';
                    $pagination .= '<li>' . anchor($targetpage . '/' . $lpm1, $lpm1) . '</li>';
                    $pagination .= '<li>' . anchor($targetpage . '/' . $lastpage, $lastpage) . '</li>';
                }
                //in middle; hide some front and some back
                elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $pagination .= '<li>' . anchor($targetpage . '/1', '1') . '</li>';
                    $pagination .= '<li>' . anchor($targetpage . '/2', '2') . '</li>';
                    $pagination .= '<li>' . anchor('#', '...') . '</li>';
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $pagination .= '<li class="active">';
                        else
                            $pagination .= '<li>';
                        $pagination .= anchor($targetpage . '/' . $counter, $counter) . '</li>';
                    }
                    $pagination .= '<li>' . anchor('#', '...') . '</li>';
                    $pagination .= '<li>' . anchor($targetpage . '/' . $lpm1, $lpm1) . '</li>';
                    $pagination .= '<li>' . anchor($targetpage . '/' . $lastpage, $lastpage) . '</li>';
                }
                //close to end; only hide early pages
                else {
                    $pagination .= '<li>' . anchor($targetpage . '/1', '1') . '</li>';
                    $pagination .= '<li>' . anchor($targetpage . '/2', '2') . '</li>';
                    $pagination .= '<li>' . anchor('#', '...') . '</li>';
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination .= '<li class="active">';
                        else
                            $pagination .= '<li>';
                        $pagination .= anchor($targetpage . '/' . $counter, $counter) . '</li>';
                    }
                }
            }
            //next button
            if ($page < $counter - 1)
                $pagination .= '<li>' . anchor($targetpage . '/' . $next, 'Next') . '</li>';
            $pagination .= '</ul>';
        }
        return $pagination;
    }

    function create_pagination($base_url = '', $total_rows = 0, $per_page = 10, $uri_segment = 3, $num_links = 2) {
        $config['base_url'] = $base_url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['uri_segment'] = $uri_segment;
        $config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

}

?>
