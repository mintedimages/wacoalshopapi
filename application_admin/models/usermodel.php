<?php

class UserModel extends CI_Model {

    private $modelName = 'UserModel';

    function __construct() {
        parent::__construct();
    }

    function insert($user) {
        $action_name = 'create user';
        $action_detail = 'model : ' . $this->modelName;
        foreach ($user as $key => $value) {
            $action_detail .= '<br />' . $key . ' : ' . $value;
            $this->db->set($key, $value);
        }
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('create_by', $this->session->userdata('user_id'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->set('update_by', $this->session->userdata('user_id'));
        $this->db->insert('mother_user');
        $action_query = $this->db->last_query();
        $this->db->select_max('user_id', 'max_id');
        $max_id = $this->db->get('mother_user')->row()->max_id;
        $this->LogActionModel->insert('mother_user', $max_id, $action_name, $action_detail, $action_query);
        return $max_id;
    }

    function update($user_id, $user) {
        $action_name = 'update User';
        $action_detail = 'model : ' . $this->modelName;
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('mother_user');
        if ($query->num_rows() > 0) {
            $dat = $query->row_array();
            $action_detail .= '<br /><b>Update</b>';
            foreach ($user as $key => $value) {
                if ($value != $dat[$key]) {
                    $action_detail .= '<br />' . $key . ' : ' . $dat[$key] . ' => ' . $value;
                    $this->db->set($key, $value);
                }
            }
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', $this->session->userdata('user_id'));
            $this->db->where('user_id', $user_id);
            $this->db->update('mother_user');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_user', $user_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found user_id';
            $this->LogActionModel->insert('mother_user', $user_id, $action_name, $action_detail, $action_query);
        }
    }

    function delete($user_id) {
        $action_name = 'delete User';
        $action_detail = 'model : ' . $this->modelName;
        $action_detail .= '<br />user_id : ' . $user_id;
        $action_query = '';
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('mother_user');
        if ($query->num_rows() > 0) {
            $dat = $query->row();
            $this->db->where('user_id', $user_id);
            $this->db->delete('mother_user');
            $action_query = $this->db->last_query();
            $this->LogActionModel->insert('mother_user', $user_id, $action_name, $action_detail, $action_query);
        } else {
            $action_detail .= '<br />status : not found user_id';
            $this->LogActionModel->insert('mother_user', $user_id, $action_name, $action_detail, $action_query);
        }
    }

    function getUserNameById($user_id = 0) {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('mother_user');
        $row = $query->row();
        return $row->user_login_name;
    }

    function getUserGroupIdById($user_id = 0) {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('mother_user');
        $row = $query->row();
        return $row->user_group_id;
    }
}

?>