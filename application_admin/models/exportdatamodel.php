<?php

class ExportDataModel extends CI_Model {

    private $modelName = 'ExportDataModel';

    function __construct() {
        parent::__construct();
    }

    function printData($table_id, $parent_id) {
        $data['table_id'] = $table_id;
        $data['parent_id'] = $parent_id;
        $table = $this->StructureModel->getTable($table_id);
        $data['table'] = $table;
        //get column list
        $this->db->where('mother_column.table_id', $table_id);
        $this->db->order_by('sort_priority', 'asc');
        $data['query_column'] = $this->db->get('mother_column');
        //get data list
        $data['query_data'] = $this->BackendModel->getContentListAllLang($table_id, $parent_id, -1);
        //get lang list
        $data['query_lang'] = $this->LangModel->queryLangName();
        $this->load->view('backend/exportdata/jsonrow', $data);
    }

    function importData($table_id, $parent_id, $dataList) {
        $table = $this->StructureModel->getTable($table_id);
        $countData = count($dataList);
        $recursive_id = 0;
        $arrId = array();
        for ($i = $countData - 1; $i > -1; $i--) {
            $content_id = $this->insertData($table_id, $parent_id, $dataList[$i]);
            $arrId[$dataList[$i][$table->table_code . '_id']] = $content_id;
        }
        for ($i = $countData - 1; $i > -1; $i--) {
            if ($dataList[$i]['recursive_id'] != 0) {
                //updateRecursive;
                $this->updateRecursive($table_id, $arrId[$dataList[$i][$table->table_code . '_id']], $arrId[$dataList[$i]['recursive_id']]);
            }
        }
    }

    function insertData($table_id, $parent_id, $data) {
        $table = $this->StructureModel->getTable($table_id);
        $content = array();
        $contentLang = array();
        $columns = $this->StructureModel->getColumnList($table_id);
        foreach ($columns->result() AS $column) {
            if ($parent_id != 0) {
                $content['parent_id'] = $parent_id;
            }
            if ($column->column_lang > 0) {
                $query = $this->LangModel->queryLangName();
                foreach ($query->result() AS $row) {
                    $contentLang[$row->lang_id][$column->column_code] = $data[$column->column_code . '_' . $row->lang_code];
                }
            } else {
                $content[$column->column_code] = $data[$column->column_code];
            }
        }
        if ($table->table_type != 'static') {
            $content['sort_priority'] = 1;
            $content['enable_status'] = $data['enable_status'];
        }
        $content['recursive_id'] = $data['recursive_id'];
        //if ($content_id <= 0) {
        $content_id = $this->BackendModel->insert($table_id, $parent_id, $content);
        //} else {
        //$this->BackendModel->update($table_id, $content_id, $content);
        //}
        $this->BackendModel->updateLang($table_id, $content_id, $contentLang);
        //insert other sub table
        $this->db->where('parent_table_id', $table_id);
        $this->db->order_by('sort_priority', 'asc');
        $queryTable = $this->db->get('mother_table');
        foreach ($queryTable->result() as $rowChildTable) {
            $this->importData($rowChildTable->table_id, $content_id, $data['tbl_' . $rowChildTable->table_code]);
        }
        return $content_id;
    }

    function updateRecursive($table_id, $content_id, $newRecursive) {
        $table = $this->StructureModel->getTable($table_id);
        $this->db->set('recursive_id', $newRecursive);
        $this->db->where($table->table_code . '_id', $content_id);
        $this->db->update('tbl_' . $table->table_code);
    }

}

?>