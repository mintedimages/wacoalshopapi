<?php

class UploadHelperModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('upload');
    }

    function upload_image($upload_name, $pre_name) {
        $retVal = "";
        $error = 0;
        if ($_FILES[$upload_name]['name'] != '') {
            $_FILES['userfile']['name'] = $_FILES[$upload_name]['name'];
            $_FILES['userfile']['type'] = $_FILES[$upload_name]['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES[$upload_name]['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES[$upload_name]['error'];
            $_FILES['userfile']['size'] = $_FILES[$upload_name]['size'];

            $config['file_name'] = $pre_name . '_' . 0 . '_' . time();
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|';
            $config['max_size'] = '0';
            $config['overwrite'] = FALSE;

            $this->upload->initialize($config);

            if ($this->upload->do_upload()) {
                $error += 0;
                $data = $this->upload->data();
                $retVal = $data['file_name'];
            } else {
                $error += 1;
                echo $this->upload->display_errors();
                echo "error upload image<br />";
            }
        }
        return $retVal;
    }

    function upload_multi_image($upload_name, $pre_name) {
        $retVal = array();
        $error = 0;
        for ($i = 0; $i < count($_FILES[$upload_name]['name']); $i++) {
            $_FILES['userfile']['name'] = $_FILES[$upload_name]['name'][$i];
            $_FILES['userfile']['type'] = $_FILES[$upload_name]['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES[$upload_name]['tmp_name'][$i];
            $_FILES['userfile']['error'] = $_FILES[$upload_name]['error'][$i];
            $_FILES['userfile']['size'] = $_FILES[$upload_name]['size'][$i];

            $config['file_name'] = $pre_name . '_' . $i . '_' . time();
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|';
            $config['max_size'] = '0';
            $config['overwrite'] = FALSE;

            $this->upload->initialize($config);

            if ($this->upload->do_upload()) {
                $error += 0;
                $data = $this->upload->data();
                array_push($retVal, $data['file_name']);
            } else {
                $error += 1;
            }
        }
        return $retVal;
    }

}

?>
