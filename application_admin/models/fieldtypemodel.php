<?php

class FieldTypeModel extends CI_Model {

    var $field_type = array();

    function __construct() {
        parent::__construct();
        array_push($this->field_type, $this->getFieldTypeArray('text', 'Text', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('textarea', 'Text Area', 'text', ''));
        array_push($this->field_type, $this->getFieldTypeArray('richtext', 'Rich Text', 'text', ''));
        array_push($this->field_type, $this->getFieldTypeArray('image', 'Image', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('imagecrop', 'Image Crop', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('file', 'File', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('date', 'Date', 'date', ''));
        array_push($this->field_type, $this->getFieldTypeArray('color', 'Color', 'varchar', '7'));
        array_push($this->field_type, $this->getFieldTypeArray('number', 'Number', 'float', ''));
        array_push($this->field_type, $this->getFieldTypeArray('url', 'Url', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('email', 'Email', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('dropdown', 'Dropdown', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('radio', 'Radio', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('checkbox', 'Checkbox', 'varchar', '255'));
        array_push($this->field_type, $this->getFieldTypeArray('password', 'Password', 'varchar', '100'));
    }

    function getFieldTypeArray($fieldTypeCode, $fieldTypeName, $fieldTypeValue, $fieldTypeContraint) {
        return array('field_type_code' => $fieldTypeCode, 'field_type_name' => $fieldTypeName, 'field_type_value' => $fieldTypeValue, 'field_type_contraint' => $fieldTypeContraint);
    }

    function getFieldType() {
        return $this->field_type;
    }

    function getFieldProperty($column) {
        $fieldType = $this->getFieldTypeObj($column['column_field_type']);
        $property = array();
        $property['type'] = $fieldType['field_type_value'];
        if ($fieldType['field_type_contraint'] != '') {
            $property['constraint'] = $fieldType['field_type_contraint'];
        }
        return array($column['column_code'] => $property);
    }

    function getFieldModifyProperty($old_column_code, $column) {
        $fieldType = $this->getFieldTypeObj($column['column_field_type']);
        $property = array();
        $property['name'] = $column['column_code'];
        $property['type'] = $fieldType['field_type_value'];
        if ($fieldType['field_type_contraint'] != '') {
            $property['constraint'] = $fieldType['field_type_contraint'];
        }
        return array($old_column_code => $property);
    }

    function getFieldTypeObj($fieldTypeName) {
        for ($i = 0; $i < count($this->field_type); $i++) {
            if ($this->field_type[$i]['field_type_code'] == $fieldTypeName) {
                return $this->field_type[$i];
            }
        }
    }

    function displayField($column, $lang, $value, $isSreach = false) {
        $dataView = array();
        if ($lang != null) {
            $dataView['name'] = $column['column_name'] . ' ' . $lang['lang_name'];
            $dataView['code'] = $column['column_code'] . '_' . $lang['lang_code'];
        } else {
            $dataView['name'] = $column['column_name'];
            $dataView['code'] = $column['column_code'];
        }
        $dataView['remark'] = $column['column_remark'];
        $dataView['value'] = $value;
        if ($column['column_field_type'] == 'checkbox') {
            $dataView['relation_table'] = $column['column_relation_table'];
            return $this->load->view('fieldtype/checkbox', $dataView, true);
        } elseif ($column['column_field_type'] == 'color') {
            return $this->load->view('fieldtype/color', $dataView, true);
        } elseif ($column['column_field_type'] == 'date') {
            if ($isSreach) {
                return $this->load->view('fieldtype/date_search', $dataView, true);
            } else {
                return $this->load->view('fieldtype/date', $dataView, true);
            }
        } elseif ($column['column_field_type'] == 'dropdown') {
            $dataView['relation_table'] = $column['column_relation_table'];
            return $this->load->view('fieldtype/dropdown', $dataView, true);
        } elseif ($column['column_field_type'] == 'email') {
            return $this->load->view('fieldtype/email', $dataView, true);
        } elseif ($column['column_field_type'] == 'file') {
            if (!$isSreach) {
                return $this->load->view('fieldtype/file', $dataView, true);
            }
        } elseif ($column['column_field_type'] == 'image') {
            if (!$isSreach) {
                return $this->load->view('fieldtype/image', $dataView, true);
            }
        } elseif ($column['column_field_type'] == 'imagecrop') {
            if (!$isSreach) {
                $dataView['column_option'] = $column['column_option'];
                return $this->load->view('fieldtype/imagecrop', $dataView, true);
            }
        } elseif ($column['column_field_type'] == 'number') {
            if ($isSreach) {
                return $this->load->view('fieldtype/number_search', $dataView, true);
            } else {
                return $this->load->view('fieldtype/number', $dataView, true);
            }
        } elseif ($column['column_field_type'] == 'radio') {
            $dataView['relation_table'] = $column['column_relation_table'];
            return $this->load->view('fieldtype/radio', $dataView, true);
        } elseif ($column['column_field_type'] == 'password') {
            if ($isSreach) {
                
            } else {
                return $this->load->view('fieldtype/password', $dataView, true);
            }
        } elseif ($column['column_field_type'] == 'richtext') {
            return $this->load->view('fieldtype/richtext', $dataView, true);
        } elseif ($column['column_field_type'] == 'text') {
            if ($isSreach) {
                return $this->load->view('fieldtype/text_search', $dataView, true);
            } else {
                return $this->load->view('fieldtype/text', $dataView, true);
            }
        } elseif ($column['column_field_type'] == 'textarea') {
            return $this->load->view('fieldtype/textarea', $dataView, true);
        } elseif ($column['column_field_type'] == 'url') {
            return $this->load->view('fieldtype/url', $dataView, true);
        }
    }

    function displayFieldSearchPost($column, $lang) {
        $dataView = array();
        if ($lang != null) {
            $code = $column['column_code'] . '_' . $lang['lang_code'];
        } else {
            $code = $column['column_code'];
        }
        $query_array = array();
        if ($column['column_field_type'] == 'checkbox') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'color') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'date') {
            $code1 = $code . '_start';
            $code2 = $code . '_end';
            if (isset($_POST['' . $code1 . '']) || $_POST['' . $code1 . ''] != '') {
                $query_array['' . $code1 . ''] = $_POST['' . $code1 . ''];
            }
            if (isset($_POST['' . $code2 . '']) || $_POST['' . $code2 . ''] != '') {
                $query_array['' . $code2 . ''] = $_POST['' . $code2 . ''];
            }
        } elseif ($column['column_field_type'] == 'dropdown') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'email') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'file') {
            
        } elseif ($column['column_field_type'] == 'image') {
            
        } elseif ($column['column_field_type'] == 'number') {
            $code1 = $code . '_start';
            $code2 = $code . '_end';
            if (isset($_POST['' . $code1 . '']) || $_POST['' . $code1 . ''] != '') {
                $query_array['' . $code1 . ''] = $_POST['' . $code1 . ''];
            }
            if (isset($_POST['' . $code2 . '']) || $_POST['' . $code2 . ''] != '') {
                $query_array['' . $code2 . ''] = $_POST['' . $code2 . ''];
            }
        } elseif ($column['column_field_type'] == 'radio') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'password') {
            
        } elseif ($column['column_field_type'] == 'richtext') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'text') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'textarea') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        } elseif ($column['column_field_type'] == 'url') {
            if (isset($_POST['' . $code . '']) || $_POST['' . $code . ''] != '') {
                $query_array['' . $code . ''] = $_POST['' . $code . ''];
            }
        }
        return $query_array;
    }

    function getFieldSearch($column, $lang, $table_code = '') {
        $dataView = array();
        if ($lang != null) {
            $code = $column['column_code'] . '_' . $lang['lang_code'];
        } else {
            $code = $column['column_code'];
        }
        $strWhere = '';
        if ($column['column_field_type'] == 'checkbox') {
            if ($this->input->get($code) != '') {
                
            }
        } elseif ($column['column_field_type'] == 'color') {
            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' = ' . '\'' . $value . '\'';
            }
        } elseif ($column['column_field_type'] == 'date') {
            $code1 = $code . '_start';
            $code2 = $code . '_end';
            if ($this->input->get($code1) != '') {
                $value = $this->input->get('' . $code1 . '');
                $strWhere .= ' AND ' . $table_code . $code . ' >= ' . '\'' . $value . '\'';
            }
            if ($this->input->get($code2) != '') {
                $value = $this->input->get('' . $code2 . '');
                $strWhere .= ' AND ' . $table_code . $code . ' <= ' . '\'' . $value . '\'';
            }
        } elseif ($column['column_field_type'] == 'dropdown') {
            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' = ' . '\'' . $value . '\'';
            }
        } elseif ($column['column_field_type'] == 'email') {

            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' LIKE ' . '\'%' . $value . '%\'';
            }
        } elseif ($column['column_field_type'] == 'file') {
            
        } elseif ($column['column_field_type'] == 'image') {
            
        } elseif ($column['column_field_type'] == 'number') {
            $code1 = $code . '_start';
            $code2 = $code . '_end';

            if ($this->input->get($code1) != '') {
                $value = $this->input->get('' . $code1 . '');
                $strWhere .= ' AND ' . $table_code . $code . ' >= ' . '\'' . $value . '\'';
            }
            if ($this->input->get($code2) != '') {
                $value = $this->input->get('' . $code2 . '');
                $strWhere .= ' AND ' . $table_code . $code . ' <= ' . '\'' . $value . '\'';
            }
        } elseif ($column['column_field_type'] == 'radio') {
            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' = ' . '\'' . $value . '\'';
            }
        } elseif ($column['column_field_type'] == 'password') {
            
        } elseif ($column['column_field_type'] == 'richtext') {
            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' LIKE ' . '\'%' . $value . '%\'';
            }
        } elseif ($column['column_field_type'] == 'text') {
            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' LIKE ' . '\'%' . $value . '%\'';
            }
        } elseif ($column['column_field_type'] == 'textarea') {
            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' LIKE ' . '\'%' . $value . '%\'';
            }
        } elseif ($column['column_field_type'] == 'url') {
            if ($this->input->get($code) != '') {
                $value = $this->input->get('' . $code . '');
                $strWhere .= ' AND ' . $table_code . $code . ' LIKE ' . '\'%' . $value . '%\'';
            }
        }
        return $strWhere;
    }

}

?>