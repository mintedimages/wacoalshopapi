<?php

class ExportStructureModel extends CI_Model {

    private $modelName = 'ExportStructureModel';

    function __construct() {
        parent::__construct();
    }

    function printTable($table_id) {
        $this->db->where('table_id', $table_id);
        $this->db->order_by('sort_priority', 'asc');
        $data['data'] = $this->db->get('mother_table')->row();
        $this->db->where('mother_column.table_id', $table_id);
        $this->db->order_by('sort_priority', 'asc');
        $data['query_column'] = $this->db->get('mother_column');
        $this->db->where('parent_table_id', $table_id);
        $this->db->order_by('sort_priority', 'asc');
        $data['query_child'] = $this->db->get('mother_table');
        $this->load->view('structure/content/exportstructure/table', $data);
    }

    function importTable($table, $parent_id, $sort_priority) {
        $this->load->model(array('StructureModel', 'FieldTypeModel'));
        $table_data = array(
            'parent_table_id' => $parent_id,
            'table_name' => $table->table_name,
            'table_code' => $table->table_code,
            'table_type' => $table->table_type,
            'icon_id' => $table->icon_id,
            'table_order' => $table->table_order,
            'table_newcontent' => $table->table_newcontent,
            'table_preview' => $table->table_preview,
            'table_recursive' => $table->table_recursive,
            'sort_priority' => $sort_priority
        );
        $this->db->where('table_code', $table->table_code);
        $query = $this->db->get('mother_table');
        if ($query->num_rows() == 0) {
            $table_id = $this->StructureModel->insertTable($table_data);
            //insert field
            $column_length = count($table->mother_column);
            for ($i = 0; $i < $column_length; $i++) {
                $column = array(
                    'table_id' => $table_id,
                    'column_name' => $table->mother_column[$i]->column_name,
                    'column_code' => $table->mother_column[$i]->column_code,
                    'column_main' => $table->mother_column[$i]->column_main,
                    'column_field_type' => $table->mother_column[$i]->column_field_type,
                    'column_relation_table' => $table->mother_column[$i]->column_relation_table,
                    'column_lang' => $table->mother_column[$i]->column_lang,
                    'column_show_list' => $table->mother_column[$i]->column_show_list,
                    'searchable' => $table->mother_column[$i]->searchable,
                    'column_option' => $table->mother_column[$i]->column_option,
                    'column_remark' => $table->mother_column[$i]->column_remark,
                    'sort_priority' => $table->mother_column[$i]->sort_priority
                );
                $this->StructureModel->insertColumn($column);
            }
            //insert child table
            $child_length = count($table->child_table);
            for ($i = 0; $i < $child_length; $i++) {
                $this->importTable($table->child_table[$i], $table_id, $i + 1);
            }
        }
    }

}

?>