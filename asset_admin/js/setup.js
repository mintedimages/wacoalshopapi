$(function(){
    $(".dial").knob();
  
    for (var a=[],i=0;i<20;++i) a[i]=i;

    // http://stackoverflow.com/questions/962802#962890
    function shuffle(array) {
        var tmp, current, top = array.length;
        if(top) while(--top) {
            current = Math.floor(Math.random() * (top + 1));
            tmp = array[current];
            array[current] = array[top];
            array[top] = tmp;
        }
        return array;
    }

    $(".sparklines").each(function(){
        $(this).sparkline(shuffle(a), {
            type: 'line',
            width: '150',
            lineColor: '#333',
            spotRadius: 2,
            spotColor: "#000",
            minSpotColor: "#000",
            maxSpotColor: "#000",
            highlightSpotColor: '#EA494A',
            highlightLineColor: '#EA494A',
            fillColor: '#FFF'
        });
    });
  
    $(".sortable").tablesorter();
  
    $(".pbar").peity("bar", {
        colours: ["#EA494A"],
        strokeWidth: 4,
        height: 32,
        max: null,
        min: 0,
        spacing: 4,
        width: 58
    });
});

$(document).ready(function() {

    // page is now ready, initialize the calendar...
    $('.datepicker').datepicker();
    
    $('.timepicker').timepicker({
        template : 'dropdown',
        showSeconds: false,
        minuteStep : 5,
        showInputs: true,
        showMeridian: false
    });
    
    $('.colorpicker').colorpicker();
    
    $('#calendar').fullCalendar({
        events: 'http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic',
			
        eventClick: function(event) {
            // opens events in a popup window
            window.open(event.url, 'gcalevent', 'width=700,height=600');
            return false;
        },
			
        loading: function(bool) {
            if (bool) {
                $('#loading').show();
            }else{
                $('#loading').hide();
            }
        }
    });
    
    $("#column_name").keyup(function() {
        var slug = $.slug($(this).val());
        $("#column_code").val(slug);
    });
    $("#lang_name").keyup(function() {
        var slug = $.slug($(this).val());
        $("#lang_code").val(slug);
    });
    
    $("#checkall").click(function(){
        if($(this).prop("checked")){
            $(".checkbox").prop('checked',true)
        }else{
            $(".checkbox").prop('checked',false);
        }
    });
    
    $(".del").click(function(){
        $this = $(this);
        $form = $('#frm-showlist');
        if (confirm("Are you sure you want to delete")) {
            $checked  = 0;
            $checkbox = $form.children().find('input[name="checkbox[]"]');
            $.each($checkbox, function() {
                if($(this).is(':checked')) {
                    $checked++;
                }
            });

            if($checked > 0) {
                if($this.attr('id') == 'del-top') {
                    $form.submit();
                }
                else {
                    return true;
                }
            }
            else {
            	alert('Please, select one or many checkbox of your selection to delete !');
                return false;
            }
        }
        else{
            return false;
        }
    });
    
    $(".disabled").click(function(){
        return false;
    });
});